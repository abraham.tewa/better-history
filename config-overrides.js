const path = require('path');

function webpack(config, env) {
    const newConfig = {
        ...config,
        entry: {
            "main": [
                path.resolve('src/index.tsx'),
            ],
            "background": path.resolve('src/background/index.ts'),
        },
        output: {
            ...config.output,
            filename: 'views/[name]/index.js',
        },
        cache: false,
        plugins : config
            .plugins
            .map((plugin) => {
                const pluginName = plugin.constructor.name;
                if (pluginName !== 'HtmlWebpackPlugin') {
                    return plugin;
                }

                plugin.userOptions = {
                    ...plugin.userOptions,
                    filename: 'better-history',
                    inject: false,
                };

                return plugin;
            }),
        devServer: {
            open: false,
        },
    };

    delete newConfig.output.chunkFilename;
    delete newConfig.output.assetModuleFilename;

    return newConfig;
}

function devServer(config) {
    const newConfig = {
        ...config,
        open: false,
        devMiddleware: {
            ...config.devMiddleware,
            writeToDisk: true,
        },
    };

    return newConfig;
}

module.exports = {
    webpack,
    devServer(configFunction) {
        return (proxy, allowedHost) => {
            const config = configFunction(proxy, allowedHost);

            return devServer(config);
        };
    },
};
