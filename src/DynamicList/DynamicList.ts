// ============================================================
// Class
class DynamicList<T extends DynamicListEventTarget> extends EventTarget {
    #itemUpdateStack : T[] = [];

    #list: T[];

    #nextEvents : {
        [key in EventType]: {
            timeout: NodeJS.Timeout | undefined,
            items: Set<T>,
        }
    };

    constructor(list: T[]) {
        super();
        this.#list = [...list];

        this.#list.forEach((item) => item.addEventListener('update', () => this.#onItemUpdate(item)));

        this.#nextEvents = {
            [EventType.itemUpdated]: { timeout: undefined, items: new Set() },
            [EventType.itemAdded]: { timeout: undefined, items: new Set() },
            [EventType.itemRemoved]: { timeout: undefined, items: new Set() },
            [EventType.orderChanged]: { timeout: undefined, items: new Set() },
        };
    }

    get length() : number {
        return this.#list.length;
    }

    addEventListener(
        type: EventType.itemAdded,
        callback: (event: ItemsEventObject<T, EventType.itemAdded>) => void,
    ): void;
    addEventListener(
        type: EventType.itemRemoved,
        callback: (event: ItemsEventObject<T, EventType.itemRemoved>) => void,
    ): void;
    addEventListener(
        type: EventType.itemUpdated,
        callback: (event: ItemsEventObject<T, EventType.itemUpdated>) => void,
    ): void;
    addEventListener(
        type: EventType.orderChanged,
        callback: (event: NotItemEvent<EventType.orderChanged>) => void,
    ): void;
    addEventListener(
        type: string,
        callback: EventListenerOrEventListenerObject | null,
        options?: boolean | AddEventListenerOptions,
    ): void {
        super.addEventListener(type, callback, options);
    }

    at(position: number): T | undefined {
        return this.#list.at(position);
    }

    concat(...items: T[]): DynamicList<T> {
        const list = this.#list.concat(items);
        return new DynamicList(list);
    }

    indexOf(searchElement: T, fromIndex?: number): number {
        return this.#list.indexOf(searchElement, fromIndex);
    }

    lastIndexOf(searchElement: T, fromIndex?: number): number {
        return this.#list.lastIndexOf(searchElement, fromIndex);
    }

    includes(item: T, fromIndex?: number): boolean {
        return this.#list.includes(item, fromIndex);
    }

    join(separator: string): string {
        return this.#list.join(separator);
    }

    pop() : T | undefined {
        const item = this.#list.pop();

        if (item) {
            this.#planEvent(EventType.itemRemoved, [item]);
        }

        return item;
    }

    #pushSafe(list: T[]) : void {
        this.#list.push(...list);
        this.#planEvent(EventType.itemUpdated, list);
    }

    push(...list: T[]) : this {
        if (!list.length) {
            return this;
        }

        const duplicates = list
            .filter((item) => this.#list.includes(item));

        if (duplicates.length) {
            throw new Error(`Pushing duplicate: ${duplicates}`);
        }

        this.#pushSafe(list);
        return this;
    }

    reverse(): this {
        this.#list.reverse();
        this.#planEvent(EventType.orderChanged);
        return this;
    }

    shift(): T | undefined {
        const item = this.#list.shift();

        if (item) {
            this.#planEvent(EventType.itemRemoved, [item]);
        }

        return item;
    }

    slice(start?: number, end?: number): DynamicList<T> {
        const items = this.#list.slice(start, end);

        return new DynamicList(items);
    }

    sort(compareFn?: (a: T, b: T) => number): this {
        this.#list.sort(compareFn);
        this.#planEvent(EventType.orderChanged);
        return this;
    }

    splice(start: number, deleteCount: number, ...items: T[]): DynamicList<T> {
        const list = this.#list.splice(start, deleteCount, ...items);

        if (list.length) {
            this.#planEvent(EventType.itemRemoved, list);
        }

        if (items.length) {
            this.#planEvent(EventType.itemAdded, items);
        }

        return new DynamicList(list);
    }

    unshift(...list: T[]): this {
        this.#list.unshift(...list);
        this.#planEvent(EventType.itemUpdated, list);
        return this;
    }

    every<S extends T>(predicate: (value: T, index: number, array: T[]) => value is S, thisArg?: any): this is S[];
    every(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): boolean {
        return this.#list.every.call(thisArg, predicate);
    }

    some(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): boolean {
        return this.#list.some.call(thisArg, predicate);
    }

    forEach(callbackfn: (value: T, index: number, array: T[]) => void, thisArg?: any): void {
        return this.#list.forEach.call(thisArg, callbackfn);
    }

    map<U>(callbackfn: (value: T, index: number, array: T[]) => U, thisArg?: any): U[] {
        return this.#list.map.call(thisArg, callbackfn) as U[];
    }

    filter(
        predicate: (value: T, list: DynamicList<T>) => boolean,
        thisArg?: any,
    ): DynamicList<T> {
        const filter = (value: T) => predicate(value, this);

        const list = this.#list.filter.call(thisArg, filter);

        return new DynamicList(list);
    }

    reduce(
        callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T,
        initialValue?: T,
    ): T;
    reduce<U>(
        callbackfn: (previousValue: U, currentValue: T, currentIndex: number, array: T[]) => U,
        initialValue: U,
    ): U {
        return this.#list.reduce<U>(callbackfn, initialValue);
    }

    reduceRight(
        callbackfn: (previousValue: T, currentValue: T, currentIndex: number, array: T[]) => T,
        initialValue?: T,
    ): T;
    reduceRight<U>(
        callbackfn: (previousValue: U, currentValue: T, currentIndex: number, array: T[]) => U,
        initialValue: U,
    ): U {
        return this.#list.reduceRight<U>(callbackfn, initialValue);
    }

    #onItemUpdate(item: T) {
        this.#itemUpdateStack.push(item);
        this.#planEvent(EventType.itemUpdated, [item]);
    }

    #planEvent(type: EventType, list?: T[]) {
        const nextEvent = this.#nextEvents[type];

        list?.forEach((item) => nextEvent.items.add(item));

        // Planning next event if not already done
        if (!nextEvent.timeout) {
            nextEvent.timeout = setTimeout(
                () => {
                    const items = Array.from(nextEvent.items);

                    // Cleaning next event info
                    nextEvent.timeout = undefined;
                    nextEvent.items = new Set();

                    const detail = list
                        ? { items }
                        : {};

                    // Creating and dispatching event
                    const event = new CustomEvent(
                        EventType.itemUpdated,
                        {
                            detail,
                        },
                    );

                    this.dispatchEvent(event);
                },
                0,
            );
        }
    }
}

// ============================================================
// Types

enum EventType {
    itemAdded = 'itemAdded',
    orderChanged = 'orderChanged',
    itemRemoved = 'itemRemoved',
    itemUpdated = 'itemUpdated',
}

type NotItemEvent<U extends EventType> = {
    type: U,
    detail: {},
};

type ItemsEventObject<T, U extends EventType> = {
    type: U,
    detail : {
        items: T[],
    }
};

type ItemEvents = 'update';

interface DynamicListEventTarget extends EventTarget {
    addEventListener(
        type: ItemEvents,
        callback: EventListenerOrEventListenerObject | null, options?: AddEventListenerOptions | boolean
    ): void,

    removeEventListener(
        type: ItemEvents,
        callback: EventListenerOrEventListenerObject | null,
        options?: EventListenerOptions | boolean,
    ): void;
}

// ============================================================
// Exports
export default DynamicList;

export type {
    ItemEvents,
};
