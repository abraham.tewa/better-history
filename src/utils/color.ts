// ============================================================
// Import packages
import Color from 'color';

// ============================================================
// Module's constants and variables

// ============================================================
// Functions
function getGradient(
    base: string,
    nbColors: number,
    modifier: (color: Color, index: number) => Color,
) : string[] {
    const initialColor = Color(base);

    const colors = new Array(nbColors - 1)
        .fill(undefined)
        .reduce(
            (acc: Color[], val, index) => {
                const prevColor = acc.at(-1) as Color;
                acc.push(modifier(prevColor, index));
                return acc;
            },
            [initialColor],
        )
        .map((color) => color.hex());

    return colors;
}

// ============================================================
// Exports
export {
    getGradient,
};
