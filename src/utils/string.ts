function compareString(a: string | undefined, b: string | undefined) : number {
    if (a === undefined) {
        return -1;
    }

    if (b === undefined) {
        return 1;
    }

    return a.localeCompare(b);
}

export {
    compareString,
};
