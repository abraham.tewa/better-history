function ensureCount<T, U>(
    count: number,
    list: T[],
    getValue: GetValue<T, U>,
) : U[] {
    const returnList = [];
    let index = 0;

    while (returnList.length < count && index < list.length) {
        const value = getValue(
            list[index],
            [...returnList],
        );

        if (value !== undefined && value !== null) {
            returnList.push(value);
        }

        index += 1;
    }

    return returnList;
}

type GetValue<T, U> = (
    val: T,
    currentList: U[],
) => U | undefined | null;

export {
    ensureCount,
};
