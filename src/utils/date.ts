// ============================================================
// Import packages
import { Info, DateTime } from 'luxon';

// ============================================================
// Import modules
import { TimeRange, TimeUnit } from '../constants';

// ============================================================
// Module's constants and variables
const secondInMilliseconds = 1000;
const minuteInMilliseconds = 60 * secondInMilliseconds;
const hourInMilliseconds = 60 * minuteInMilliseconds;
const dayInMilliseconds = 24 * hourInMilliseconds;

const timeUnitQuantity : { [key in TimeUnit]?: number } = {
    [TimeUnit.seconds]: secondInMilliseconds,
    [TimeUnit.minute]: minuteInMilliseconds,
    [TimeUnit.hour]: hourInMilliseconds,
    [TimeUnit.amPm]: (dayInMilliseconds / 2),
    [TimeUnit.day]: dayInMilliseconds,
    [TimeUnit.week]: dayInMilliseconds * 7,
};

const timeQuantity : { [key in TimeRange]: GetTimeQuantity } = {
    // Seconds
    [TimeRange.seconds]: (time) => time - Math.floor(time / (secondInMilliseconds)) * secondInMilliseconds,

    // Minutes
    [TimeRange.minute]: (time) => time - Math.floor(time / (minuteInMilliseconds)) * minuteInMilliseconds,

    // Hours
    [TimeRange.hour]: (time) => time - Math.floor(time / (hourInMilliseconds)) * hourInMilliseconds,

    // Day
    [TimeRange.day]: (time) => time - Math.floor(time / (dayInMilliseconds)) * dayInMilliseconds,

    // AM / PM
    [TimeRange.amPm]: (time) => {
        let quantity = time - Math.floor(time / (dayInMilliseconds)) * dayInMilliseconds;

        const hours = Math.floor((time % dayInMilliseconds) / hourInMilliseconds);

        if (hours >= 12) {
            quantity -= 12 * hourInMilliseconds;
        }

        return quantity;
    },

    // Week
    [TimeRange.week]: (time) => {
        const date = new Date(time);
        const firstWeekDay = getFirstWeekDay(date);

        return time - firstWeekDay.getTime();
    },

    // Month
    [TimeRange.month]: (time) => {
        const date = new Date(time);
        const firstMonthDay = getFirstMonthDay(date);
        return time - firstMonthDay.getTime();
    },

    // Year
    [TimeRange.year]: (time) => {
        const date = new Date(time);
        const firstYearDay = getFirstYearDay(date);
        return time - firstYearDay.getTime();
    },

    // Always
    [TimeRange.always]: (time) => time,
};

// ============================================================
// Functions

function getWeekDayName(locale: string, day: number) {
    return Info.weekdays('long', { locale })[day];
}

/**
 * Return the first day of the week in which belongs the given date.
 *
 * For example, the 01/01/2022 was a saturday.
 *      const date = new Date('2022-01-01');    // saturday
 *      const firstDay = getFirstWeekDay(date); // 2021-27-12 (monday)
 */
function getFirstWeekDay(date: Date): Date {
    const firstWeekDay = new Date(date);
    firstWeekDay.setDate(date.getDate() - date.getDay());
    firstWeekDay.setHours(0);
    firstWeekDay.setMinutes(0);
    firstWeekDay.setSeconds(0);
    firstWeekDay.setMilliseconds(0);

    return firstWeekDay;
}

/**
 * Return the first day of the month in which belongs the given date.
 *
 * Example:
 *      const date = new Date('2022-01-25');
 *      const firstDay = getFirstMonthDay(date); // 2022-01-01
 */
function getFirstMonthDay(date: Date): Date {
    const firstMonthDay = new Date(date.getFullYear(), date.getMonth());
    return firstMonthDay;
}

/**
 * Return the first day of the month in which belongs the given date.
 *
 * Example:
 *      const date = new Date('2022-03-25');
 *      const firstDay = getFirstMonthDay(date); // 2022-01-01
 */
function getFirstYearDay(date: Date): Date {
    const firstMonthDay = new Date(date.getFullYear().toString());
    return firstMonthDay;
}

function formatDate(
    range: TimeRange,
    timeStep: TimeUnit,
    formatterOptions: FormatterOptions,
    value: number,
) : string {
    switch (range) {
    case TimeRange.minute:
        return toSeconds(value);

    case TimeRange.hour:
        return toMinutes(value);

    case TimeRange.amPm:
    case TimeRange.day:
        return toHours(value, timeStep);

    case TimeRange.week:
        return toDays(value, timeStep, formatterOptions);

    case TimeRange.month:
        return toDays(value, timeStep);

    case TimeRange.year:
        return toDays(value, timeStep, formatterOptions);

    case TimeRange.always:
        return toDateString(value, timeStep, formatterOptions);

    default:
    }

    return value.toString();
}

function toSeconds(
    value: TimeInSeconds,
) : string {
    return value.toString().padStart(2, '0');
}

function toMinutes(
    value: number,
) : string {
    const [minutes, seconds] = mod(value, [60]);
    return `${minutes}:${seconds}`;
}

function toHours(
    value: TimeInSeconds | TimeInMinutes,
    from: TimeUnit,
) : string {
    switch (from) {
    case TimeUnit.seconds: {
        const [hours, minutes, seconds] = mod(value, [24, 60, 60]);
        return `${hours}:${minutes}:${seconds}`;
    }

    case TimeUnit.minute: {
        const [hours, minutes] = mod(value, [24, 60]);
        return `${hours}:${minutes}`;
    }
    default:
        throw new Error(`Invalid time unit: ${from}`);
    }
}

function toDays(
    value: TimeInSeconds | TimeInMinutes | TimeInHours,
    from: TimeUnit,
    options?: FormatterOptions,
) : string {
    switch (from) {
    case TimeUnit.seconds: {
        const [days, hours, minutes, seconds] = mod(value, [24, 60, 60]);

        if (options) {
            const dayLabel = toDayLong(days, options.locale);
            return `${dayLabel} ${hours}:${minutes}:${seconds}`;
        }
        return `${days} ${hours}:${minutes}:${seconds}`;
    }

    case TimeUnit.minute: {
        const [days, hours, minutes] = mod(value, [24, 60]);

        if (options) {
            const dayLabel = toDayLong(days, options.locale);
            return `${dayLabel} ${hours}:${minutes}`;
        }
        return `${days} ${hours}:${minutes}`;
    }

    case TimeUnit.hour: {
        const [days, hours] = mod(value, [24]);

        if (options) {
            const dayLabel = toDayLong(days, options.locale);
            return `${dayLabel} ${hours}:00`;
        }
        return `${days} ${hours}:00`;
    }

    case TimeUnit.amPm: {
        const [days, amPm] = mod(value, [2]);

        const label = amPm === 0 ? 'am' : 'pm';

        if (options) {
            const dayLabel = toDayLong(days, options.locale);
            return `${dayLabel} ${label}`;
        }

        return `${days}j ${label}`;
    }

    case TimeUnit.week: {
        const [weeks, days] = mod(value, [7]);

        return `${weeks}w, ${days}j`;
    }

    case TimeUnit.year: {
        return `${value}j`;
    }

    default:
        return `x${value}j`;
    }
}

/**
 * Count the number of time units in the given range.
 * The number will be the floor value.
 *
 * Examples:
 *   Range: Week
 *   Units: Day
 *   Values:
 *      2022-01-01 ==> 5 | meaning: 5 time units (days) has passed from the beginning of the range (week))
 *      2021-12-27 ==> 0 | meaning: 0 time units (days) has passed from the beginning of the range (week))
 *      2022-01-08 ==> 5 | meaning: 5 time units (days) has passed from the beginning of the range (week))
 *
 *
 *   Range: Week
 *   Units: hours
 *   Values:
 *     2022-01-01 01:25:30 ==> 126 | meaning: 126 time units (hours) has passed from the beginning of the range (week)
 *     2021-12-27 01:25:30 ==> 1   | meaning: 1 time unit (hours) has passed from the beginning of the range (week)
 *
 * There is a special case if the range is "Always".
 *  In this case, the value return will not be a time quantity but a Epoch.
 *
 */
function toUnit(
    time: number,
    range: TimeRange,
    timeStep: TimeUnit,
) : TimeUnitQuantity | Epoch {
    const quantityInMs = timeQuantity[range](time);
    const unit = timeUnitQuantity[timeStep];

    if (unit === undefined) {
        throw new Error('Invalid time step');
    }

    const quantity = Math.floor(quantityInMs / unit);

    return range === TimeRange.always
        ? quantity * unit
        : quantity;
}

function mod(value : number, divisors: number[]) : number[] {
    const divisor = divisors.reduce((acc, v) => acc * v, 1);

    const result = Math.floor(value / divisor);
    const remain = value % divisors[0];

    if (divisors.length === 1) {
        return [
            result,
            remain,
        ];
    }

    return [
        result,
        ...mod(remain, divisors.slice(1)),
    ];
}

function toDayLong(value: number, locale: string) : string {
    return getWeekDayName(locale, value);
}

function toDateString(
    value: number,
    unit: TimeUnit,
    options: FormatterOptions,
) : string {
    const dt = DateTime.fromMillis(value);
    dt.setLocale(options.locale);
    return dt.toLocaleString();
}

type GetTimeQuantity = (time: Epoch) => TimeQuantityInMs | Epoch;

// ============================================================
// Exports
export {
    getFirstMonthDay,
    getFirstWeekDay,
    getFirstYearDay,
    getWeekDayName,
    formatDate,
    toUnit,
};
