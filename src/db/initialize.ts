// ============================================================
// Import packages
import Dexie, { Table } from 'dexie';
import {
    DatabaseName,
    Tables,
} from '../constants';
import List from '../List';
import { VisitEntry } from '../VisitList';

// ============================================================
// Module's constants and variables

const currentList : List<VisitEntry> = new List();
let resolveInitializePromise : (value: Dexie) => void;

const initializedPromise = new Promise<Dexie>((resolve) => {
    resolveInitializePromise = resolve;
});

async function initialize() {
    const db = new Dexie(DatabaseName);
    db.version(1).stores({
        [Tables.visits]: 'visit.visitId, url, visit.visitTime',
    });

    resolveInitializePromise(db);
}

function getList() : List<VisitEntry> {
    return currentList;
}

async function getDb() : Promise<Dexie> {
    return initializedPromise;
}

async function getTable(): Promise<Table<DbVisitItem>> {
    const db = await getDb();
    return db.table(Tables.visits);
}

// ============================================================
// Exports
export default initialize;
export {
    getDb,
    getList,
    getTable,
};
