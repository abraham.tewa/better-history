const DatabaseName = 'better-history';
const ItemType = Symbol('ItemType');
const Undefined = Symbol('undefined');
const GroupedBy = Symbol('group by');
const Stats = Symbol('group stats');
const ListGroups = Symbol('List of groups');

enum LocalStorageKeys {
    IndexerStats = 'indexer.stats',
}

enum DialogMessageType {
    IndexerUpdate = 'indexerUpdate',
    Query = 'query',
    Ready = 'ready',
}

enum TimeUnit {
    seconds = 0,
    minute = 1,
    hour = 2,
    amPm = 3,
    day = 4,
    week = 5,
    month = 6,
    year = 7,
}

enum TimeRange {
    seconds = 0,
    minute = 1,
    hour = 2,
    amPm = 3,
    day = 4,
    week = 5,
    month = 6,
    year = 7,
    always = 9,
}

enum VisitItemType {
    file = 'file',
    url = 'url',
}

enum Tables {
    visits = 'visits',
}

export {
    DatabaseName,

    ItemType,
    GroupedBy,
    ListGroups,
    Stats,
    Undefined,

    DialogMessageType,
    LocalStorageKeys,
    TimeUnit,
    TimeRange,
    Tables,
    VisitItemType,
};
