// ============================================================
// Import packages
import type {
    Browser,
    History,
} from 'webextension-polyfill';

import {
    DialogMessageType,
    ItemType,
    TimeUnit,
    VisitItemType,
    Undefined,
    TimeRange,
    GroupedBy,
    Stats,
    ListGroups,
} from './constants';
import { VisitEntry } from './VisitList';

import {
    UrlParser,
} from './Realm';

// ============================================================
// Types
declare global {

    type GetOrCalc<T> = T | undefined | null;

    type IUrlParser = UrlParser;

    type IVisitEntry = VisitEntry;

    type VisitEntryId = string;

    type StringRegExp = string | RegExp;

    type IndexStats = {
        oldest: Date,
        newest: Date,
        indexed: number,
    };

    /**
     * Create a type NonNullable if the second generic parameter equal true.
     *
     * Example:
     *
     *      type SomeType = number | undefined;
     *
     *      type CanBeUndefined = ReqIf<SomeType, true>
     *      type CannotBeUndefined = ReqIf<SomeType, false>
     *
     */
    type RemoveUndefIf<T, R extends (boolean | undefined)> = R extends true
        ? NonNullable<T>
        : T;

    type PropertyType<
        Type,
        Property extends keyof Type,
    > = Type extends { [key in Property]: infer U } ? U : never;

    type Domain = string;

    type FormatterOptions = {
        locale: string,
    };

    // eslint-disable-next-line no-var, vars-on-top
    var browser : Browser;

    type Epoch = number;
    type TimeUnitQuantity = number;

    type TimeQuantityInMs = number;
    type TimeInSeconds = number;
    type TimeInMinutes = number;
    type TimeInHours = number;

    interface StorageContent {
        host: { [key: string]: number },
        type: { [key: string]: number },
    }

    interface Window {
        tabId?: number;
    }

    interface IDBVersionChangeEvent extends Event<{ result: IDBDatabase }> {
        readonly newVersion: number | null;
        readonly oldVersion: number;
    }

    interface Event<T> {
        target: Required<T>
    }

    interface IDbVisitItem {
        visit: History.VisitItem,
        type: VisitItemType,
        title?: string,
        url: string,
    }

    type DbVisitItem = DbVisitUrl | DbVisitFile;

    interface DbVisitUrl extends IDbVisitItem {
        visit: History.VisitItem,
        type: VisitItemType.url,
        domain?: string,
        hostname: string,
        tld?: string,
        path: string,
        protocol: string,
    }

    interface DbVisitFile extends IDbVisitItem {
        visit: History.VisitItem,
        type: VisitItemType.file,
        drive?: string,
        folder: string,
        path: string,
        filename: string,
        extension?: string,
    }

    namespace QueryVisits {
        type GroupMap = NumberGroupMap | StringGroupMap | EpochGroupMap | TimeNumberGroupMap;

        // Abstract types
        interface IGroupMap<
            T,
            G extends IGroup<T> = IGroup<T>,
            S extends StringGroupStats = StringGroupStats,
            GB extends GroupBy.Criteria | string | symbol | number = GroupBy.Criteria,
        > {
            [key: string]: G,
            [ItemType]: ValueEnumType[],
            [Undefined]?: G,
            [GroupedBy]: GB,
            [ListGroups]: G[],
            [Stats]: S,
        }

        interface IGroup<T, U = object> {
            count: number,
            value: T | undefined,
            list?: U[],
        }

        interface IGroupWithList<T, U = object> extends IGroup<T, U> {
            list: U[],
        }

        // ========================================
        // Time number groups
        interface INumberGroup extends IGroup<number | undefined> {
            min: number,
            max: number,
        }

        type NumberGroupStats<T extends number = number> = {
            groups: {
                // Number of groups (including Undefined)
                count: number,
                min: T | null,
                max: T | null,

                // Count of the smallest groups
                smallest: number | null,

                // Count of the biggest groups
                biggest: number | null,
            },
            items: {
                count: number,
                min?: T,
                max?: T,
            },
        };

        // Concrete types
        type NumberGroupMap = {
            [key: string]: INumberGroup,
            [ItemType]: VisitItemType[],
            [Undefined]?: INumberGroup,
            [GroupedBy]: QueryVisits.GroupBy.Criteria,

            // List of all groups, ordered by number
            // Undefined groups will be at the end
            [ListGroups]: INumberGroup[],
            [Stats]: NumberGroupStats<number>,
        };

        // ========================================
        // Time number groups
        interface ITimeNumberGroup extends IGroup<TimeUnitQuantity | undefined> {
            min: TimeUnitQuantity,
            max: TimeUnitQuantity,
        }
        type TimeNumberGroupMap = {
            [key: TimeUnitQuantity]: ITimeNumberGroup,
            [ItemType]: VisitItemType[],
            [Undefined]?: ITimeNumberGroup,
            [GroupedBy]: QueryVisits.GroupBy.VisitTime,

            // List of all groups, ordered by Epoch
            // Undefined groups will be at the end
            [ListGroups]: ITimeNumberGroup[],
            [Stats]: NumberGroupStats<TimeUnitQuantity>,
        };

        // ========================================
        // Epoch groups
        interface IEpochGroup extends IGroup<Epoch> {
            min: Epoch,
            max: Epoch,
        }

        type EpochGroupMap = IGroupMap<
            Epoch,
            IEpochGroup,
            NumberGroupStats<Epoch>,
            GroupBy.VisitTime,
        >;

        // type EpochGroupMap = {
        //     [key: Epoch]: IEpochGroup,
        //     [ItemType]: VisitItemType[],
        //     [Undefined]?: IEpochGroup,
        //     [GroupedBy]: QueryVisits.GroupBy.VisitTime,

        //     // List of all groups, ordered by Epoch
        //     // Undefined groups will be at the end
        //     [ListGroups]: IEpochGroup[],
        //     [Stats]: NumberGroupStats<Epoch>
        // };

        // ========================================
        // String groups
        type StringGroupStats = {
            groups: {
                // Number of groups (including Undefined)
                count: number,

                // Count of the smallest groups
                smallest: number | null,

                // Count of the biggest groups
                biggest: number | null,
            },
            items: {
                count: number,
            },
        };

        type StringGroupMap<
            GroupKeyType = string,
            ItemType = object,
        > = {
            [key: string]: IGroup<GroupKeyType, ItemType>,
            [ItemType]: VisitItemType[],
            [Undefined]?: IGroup<GroupKeyType, ItemType>,
            [GroupedBy]: QueryVisits.GroupBy.Criteria,
            [Stats]: StringGroupStats,
            // List of all groups, ordered by Epoch
            // Undefined groups will be at the end
            [ListGroups]: IGroup<GroupKeyType, ItemType>[],
        };

        type StringGroupMapWithList<
            GroupKeyType = string,
            ItemType = object,
        > = {
            [key: string]: IGroupWithList<GroupKeyType, ItemType>,
            [ItemType]: VisitItemType[],
            [Undefined]?: IGroupWithList<GroupKeyType, ItemType>,
            [GroupedBy]: QueryVisits.GroupBy.Criteria,
            [Stats]: StringGroupStats,
            // List of all groups, ordered by Epoch
            // Undefined groups will be at the end
            [ListGroups]: IGroupWithList<GroupKeyType, ItemType>[],
        };

        type ObjectFilter = {
            [key: string]: QueryVisits.FilterString
        };

        export type Filter = {
            visitId?: string[],
            file?: FilterFile | boolean,
            url?: FilterURL | boolean,
            visitTime?: Compare<Epoch>
            transition?: FilterEnum<History.TransitionType>,

            // If true, then keep onlyu entries that are reloads
            // If false, then excludes all reloads entry
            // If undefined, do not filter on reload criteria
            reloads?: FilterBoolean,

            // If true, then keep only entries that are root navigation
            // If false, then exclude all entries that are root navigation
            // If undefined, do not filter on root navigation criteria
            navigationRoot?: FilterBoolean,

            // Filter by realm ID
            realm?: FilterEnum,

            // Filter by ID product
            product?: FilterEnum,
        };

        type FilterURL = {
            [key: 'href' | 'domain' | 'hostname' | 'tld' | 'path' | 'protocol']: FilterString
        };
        type FilterFile = {
            [key: 'drive' | 'folder' | 'path' | 'filename' | 'extension']: FilterString
        };

        type FilterEnum<T extends string = string> = string
            | string[]
            | FilterIncludeExcludeList<T>;

        type FilterIncludeExcludeList<T extends string = string> = {
            include?: T[],
            exclude?: T[],
        };

        type FilterBoolean = boolean;

        type FilterString = FilterStringContent |
            // Equivalent of {match: string}
            string |

            // Equivalent of {match: RegExp}
            RegExp |

            // Equivalent of {allow: Array<string | RegExp>}
            Array<string | RegExp>;

        type FilterStringContent = {
            // Tested value must match the filter regex
            match?: RegExp | string | RegExp[],

            // Tested value must either match a string or a regex of the array
            // If array is empty, filter is ignored
            allow?: Array<string | RegExp>,
            forbid?: Array<string | RegExp>,

            startsWith?: string,
            endsWith?: string,
            contains?: string[],

            length?: Compare<number> | number,
        };

        /**
         * Compare two number together.
         *
         *
         */
        export type Compare<T = number> = Equal<T>
            | BetweenStrict<T>
            | BetweenOrEqual<T>
            | GreaterOrEqual<T>
            | LesserOrEqual<T>
            | CompareGt<T>
            | CompareGte<T>
            | CompareLt<T>
            | CompareLte<T>;

        type Equal<T> = {
            eq: T,
        };

        type BetweenStrict<T> = {
            gt: T
            lt: T,
        };

        type BetweenOrEqual<T> = {
            gte: T,
            lte: T,
        };

        type GreaterOrEqual<T> = {
            gte: T,
            lt: T,
        };

        type LesserOrEqual<T> = {
            gt: T,
            lte: T,
        };

        type CompareGt<T> = {
            gt: T,
        };

        type CompareLt<T> = {
            lt: T,
        };

        type CompareGte<T> = {
            gte: T
        };

        type CompareLte<T> = {
            lte: T
        };

        export namespace GroupBy {
            export type Criteria = VisitTime | { property: string };

            export type GroupByDate = {
                range: TimeRange,
                timeStep: TimeUnit,
            };

            export type VisitTime = {
                visitTime: GroupByDate,
            };

            export type HavingFilter = {
                count: Compare<number>,
            };
        }
    }

    namespace Dialog {
        export type Message = {
            type: DialogMessageType,
            parameters: any
        };

        export namespace Query {
            export type Parameters = {
                filter: QueryVisits.Filter,
            };

            export type Results<T extends IDbVisitItem = IDbVisitItem> = {
                list: T[],
                types: VisitItemType[],
            };
        }

        export namespace GroupBy {
            export type Parameters = {
                filter?: QueryVisits.Filter,
                groupBy: QueryVisits.GroupBy,
            };

            export type Results<T extends IDbVisitItem = IDbVisitItem> = QueryVisits.GroupMap<T>;
        }
    }
}

declare module 'webextension-polyfill' {
    namespace WebNavigation {
        interface OnCommittedDetailsType {
            transitionType: TransitionType,
        }
    }
}

export {
    Undefined,
    ItemType,
    VisitItemType,
};
