// ============================================================
// Import packages
import _ from 'underscore';
import cloneDeep from 'clone-deep';

// ============================================================
// Import modules
import * as testFilters from './testFilters';
import { formatEnumFilter } from './utils';

// ============================================================
// Functions
/**
 * Merge two object filters.
 *
 * The returning filter will be the union of both filters:
 *  - any object passing both parent filter will pass the result filter
 *  - any object not passing any of the parent filter will not pass the result filter.
 *
 * Example:
 *
 *      filterA = {
 *          lastName: {
 *              startsWith: 'Sm',
 *              length: {
 *                  lte: 10,
 *                  gt: 5
 *              }
 *          }
 *      }
 *
 *      filterB = {
 *          lastName: {
 *              startsWith: 'Smi',
 *              length: {
 *                  gte: 4,
 *              }
 *          },
 *          firstName = {
 *              forbid: ['John']
 *          }
 *      }
 *
 *      resultFilter = intersectFilters(filterA, filterB)
 *
 *      {
 *          lastName: {
 *              startsWith: 'Smi',
 *              length: {
 *                  gt: 5,
 *                  lte: 10,
 *              },
 *          },
 *          firstName: {
 *              forbid: ['John'],
 *          }
 *      }
 *
 * If the resulting filter is incoherent, then will return null.
 * For example:
 *
 *      filterA: {
 *          lastName: {
 *              startsWith: 'Smi',
 *          }
 *      }
 *
 *      filterB: {
 *          lastName: {
 *              length: 1,
 *          }
 *      }
 *
 *  resultFilter = intersectFilters(filterA, filterB);
 *  // undefined
 *
 * @param a
 * @param b
 * @returns
 */
function intersectFilters(
    a: QueryVisits.ObjectFilter | undefined,
    b: QueryVisits.ObjectFilter | undefined,
) : QueryVisits.ObjectFilter | null | undefined {
    if (a === undefined) {
        if (b === undefined) {
            return undefined;
        }

        return optimizeObjectFilter(b);
    }

    if (b === undefined) {
        return optimizeObjectFilter(a);
    }

    const keys = _.uniq([
        ...Object.keys(a),
        ...Object.keys(b),
    ]);

    const filterObject = keys
        .reduce<QueryVisits.ObjectFilter | null>(
            (acc, key) => {
                if (acc === null) {
                    return null;
                }

                let filter: QueryVisits.FilterString | null;

                if (a[key] === undefined) {
                    filter = b[key];
                } else {
                    filter = b[key] !== undefined
                        ? mergeFilterString(a[key], b[key])
                        : a[key];
                }

                if (filter === null) {
                    return null;
                }

                filter = optimizeStringFilter(filter);

                if (filter === null) {
                    return null;
                }

                acc[key] = filter;
                return acc;
            },
            {},
        );

    return cloneDeep(filterObject);
}

function mergeMatch(
    a: string | RegExp | Array<string | RegExp>,
    b: QueryVisits.FilterString,
) : QueryVisits.FilterStringContent | null {
    if (typeof a === 'string') {
        if (typeof b === 'string') {
            return a === b ? { match: a } : null;
        }

        if (b instanceof RegExp) {
            return b.test(a) ? { match: a } : null;
        }

        if (Array.isArray(b)) {
            return {
                match: a,
                allow: b,
            };
        }

        if (b.match) {
            if (typeof b.match === 'string') {
                if (b.match !== a) {
                    return null;
                }
            } else if (b.match instanceof RegExp) {
                if (!b.match.test(a)) {
                    return null;
                }
            } else {
                // b.match is a list of RegExp
                // Ensuring that "a" validate all regexp.
                const invalid = b.match.some((item) => !item.test(a));

                if (!invalid) {
                    return null;
                }
            }
        }

        return {
            ...b,
            match: a,
        };
    }

    if (a instanceof RegExp) {
        if (typeof b === 'string') {
            return a.test(b) ? { match: b } : null;
        }

        if (b instanceof RegExp) {
            return {
                match: [a, b],
            };
        }

        if (Array.isArray(b)) {
            return {
                match: a,
                allow: b,
            };
        }

        if (b.match) {
            if (typeof b.match === 'string') {
                if (!a.test(b.match)) {
                    return null;
                }

                return b;
            }

            if (b.match instanceof RegExp) {
                return {
                    ...b,
                    match: [a, b.match],
                };
            }

            return {
                ...b,
                match: [a, ...b.match],
            };
        }

        return {
            ...b,
            match: a,
        };
    }

    if (typeof b === 'string') {
        return isInList(a, b) ? { match: b } : null;
    }

    if (b instanceof RegExp) {
        return {
            match: b,
            allow: a,
        };
    }

    if (Array.isArray(b)) {
        return a.concat(b);
    }

    if (b.allow) {
        return {
            ...b,
            allow: a.concat(b.allow),
        };
    }

    return {
        ...b,
        allow: a,
    };
}

function mergeFilterString(
    a: QueryVisits.FilterString,
    rawB: QueryVisits.FilterString,
) : QueryVisits.FilterStringContent | null {
    const filter: QueryVisits.FilterStringContent = {};

    if (typeof a === 'string' || a instanceof RegExp || Array.isArray(a)) {
        return mergeMatch(a, rawB);
    }

    let b: QueryVisits.FilterStringContent;

    // Converting rowB to FilterStringContent
    if (typeof rawB === 'string' || rawB instanceof RegExp) {
        b = {
            match: rawB,
        };
    } else if (Array.isArray(rawB)) {
        b = {
            allow: rawB,
        };
    } else {
        b = rawB;
    }

    // Filter: match
    if (a.match instanceof RegExp || (typeof a.match === 'string' && a.match)) {
        const merge = mergeMatch(a.match, b);

        if (!merge) {
            return null;
        }

        filter.match = merge.match;

        b = merge;
    }

    // Filter: allow
    if (a.allow?.length) {
        filter.allow = a.allow.concat(b.allow || []);
    } else if (b.allow?.length) {
        filter.allow = b.allow;
    }

    // Filter: forbid
    if (a.forbid?.length) {
        filter.forbid = a.forbid.concat(b.forbid || []);
    } else if (b.forbid?.length) {
        filter.forbid = b.forbid;
    }

    // Filter: startsWith
    if (a.startsWith) {
        if (b.startsWith) {
            const [shorter, longer] = getLongerAndShorter(a.startsWith, b.startsWith);

            if (!longer.startsWith(shorter)) {
                return null;
            }

            filter.startsWith = longer;
        } else {
            filter.startsWith = a.startsWith;
        }
    } else if (b.startsWith) {
        filter.startsWith = b.startsWith;
    }

    // Filter: endsWith
    if (a.endsWith) {
        if (b.endsWith) {
            const [shorter, longer] = getLongerAndShorter(a.endsWith, b.endsWith);

            if (!longer.endsWith(shorter)) {
                return null;
            }

            filter.endsWith = longer;
        } else {
            filter.endsWith = a.endsWith;
        }
    } else if (b.endsWith) {
        filter.endsWith = b.endsWith;
    }

    // Filter: contains
    filter.contains = [
        ...(a.contains || []),
        ...(b.contains || []),
    ];

    // Filter: length
    const length = intersectCompareFilter(a.length, b.length);

    if (length === null) {
        return null;
    }

    filter.length = length;

    return filter;
}

/**
 * Return the intersection of two boolean filters.
 *
 * If return null, then the filters are incompatible.
 * If return undefined, this means that there is no
 * resulting filters (for example when both filters are undefined).
 *
 * @param a
 * @param b
 * @returns
 */
function intersectBooleanFilter(
    a: QueryVisits.FilterBoolean | undefined,
    b: QueryVisits.FilterBoolean | undefined,
) : QueryVisits.FilterBoolean | undefined | null {
    if (a === undefined) {
        return b;
    }

    if (b === undefined) {
        return a;
    }

    if (a !== b) {
        return null;
    }

    return a;
}

function intersectCompareFilter(
    a: QueryVisits.Compare<number> | number | undefined,
    b: QueryVisits.Compare<number> | number | undefined,
) : QueryVisits.Compare | undefined | null {
    if (a === undefined) {
        if (b === undefined) {
            return undefined;
        }

        if (typeof b === 'number') {
            return {
                eq: b,
            };
        }

        return optimizeCompare(b);
    }

    if (b === undefined) {
        if (typeof a === 'number') {
            return {
                eq: a,
            };
        }

        return optimizeCompare(a);
    }

    const objB : QueryVisits.Compare = typeof b === 'number'
        ? { eq: b }
        : b;

    const objA : QueryVisits.Compare = typeof a === 'number'
        ? { eq: a }
        : a;

    if ('eq' in objA) {
        if ('eq' in objB) {
            if (objA.eq !== objB.eq) {
                return null;
            }
        }

        return {
            eq: objA.eq,
        };
    }

    let filter: any;

    if ('lte' in objA) {
        filter.lte = 'lte' in objB
            ? Math.min(objA.lte, objB.lte)
            : objA.lte;
    }

    if ('lt' in objA) {
        filter.lt = 'lt' in objB
            ? Math.min(objA.lt, objB.lt)
            : objA.lt;
    }

    if ('gt' in objA) {
        filter.gt = 'gt' in objB
            ? Math.max(objA.gt, objB.gt)
            : objA.gt;
    }

    if ('gte' in objA) {
        filter.gte = 'gte' in objB
            ? Math.max(objA.gte, objB.gte)
            : objA.gte;
    }

    return filter;
}

function getLongerAndShorter(a: string, b: string): [string, string] {
    const longer = a.length >= b.length ? a : b;
    const shorter = a.length < b.length ? a : b;

    return [shorter, longer];
}

function isInList(list: Array<string | RegExp>, value: string) : boolean {
    return list.some((item) : boolean => {
        if (item instanceof RegExp) {
            return item.test(value);
        }

        return value === item;
    });
}

/**
 * Optimize filter by removing unnecessary conditions.
 * Will return undefined if it's incoherent.
 * @param filter
 * @returns
 */
function optimizeStringFilter(filter: QueryVisits.FilterString): QueryVisits.FilterStringContent | null {
    if (typeof filter === 'string' || filter instanceof RegExp) {
        return {
            match: filter,
        };
    }

    if (Array.isArray(filter)) {
        return {
            allow: uniqStringRexExp(filter),
        };
    }

    let allow: QueryVisits.FilterStringContent['allow'];
    let forbid: QueryVisits.FilterStringContent['forbid'];

    let {
        contains,
        length,
        endsWith,
        match,
        startsWith,
    } = filter;

    if (typeof match === 'string' && match) {
        // Ensuring that the match is in the allow list.
        if (allow && allow.length && !testFilters.allow(allow, match)) {
            return null;
        }

        // Ensuring that the match is not excluded
        if (forbid && !testFilters.forbid(forbid, match)) {
            return null;
        }

        if (contains && !testFilters.contains(contains, match)) {
            return null;
        }

        if (startsWith && !match.startsWith(startsWith)) {
            return null;
        }

        if (endsWith && !match.endsWith(endsWith)) {
            return null;
        }

        if (length !== undefined) {
            if (typeof length === 'number') {
                if (match.length !== length) {
                    return null;
                }
            } else if (!testFilters.number(length, match.length)) {
                return null;
            }

            return null;
        }

        allow = undefined;
        contains = undefined;
        endsWith = undefined;
        forbid = undefined;
        length = undefined;
        startsWith = undefined;
    } else if (Array.isArray(match)) {
        match = uniqRegExp(match);
    }

    // Filter: forbid
    if (forbid && forbid.length) {
        if (startsWith) {
            allow = keepStartsWith(forbid, startsWith);
        }

        if (endsWith) {
            allow = keepEndsWith(forbid, endsWith);
        }

        if (contains) {
            allow = keepContains(forbid, contains);
        }
    }

    // Filter: allow
    if (allow) {
        if (startsWith) {
            allow = keepStartsWith(allow, startsWith);
        }

        if (endsWith) {
            allow = keepEndsWith(allow, endsWith);
        }

        if (contains) {
            allow = keepContains(allow, contains);
        }
    }

    // Filter: length
    if (length) {
        if (typeof length === 'number') {
            if (length === 0) {
                return null;
            }

            length = {
                eq: length,
            };
        } else {
            const optimizedLength = optimizeCompare(length);

            if (optimizedLength === null) {
                return null;
            }

            if ('eq' in optimizedLength && optimizedLength.eq === 0) {
                return null;
            }

            length = optimizedLength;
        }
    }

    contains = contains?.length ? _.uniq(contains) : [];
    forbid = forbid?.length ? uniqStringRexExp(contains) : [];

    const optimizedFilter: QueryVisits.FilterStringContent = {};

    if (allow?.length) {
        optimizedFilter.allow = _.uniq(allow);
    }

    if (contains?.length) {
        optimizedFilter.contains = _.uniq(contains);
    }

    if (forbid?.length) {
        optimizedFilter.forbid = uniqStringRexExp(contains);
    }

    if (endsWith) {
        optimizedFilter.endsWith = endsWith;
    }

    if (startsWith) {
        optimizedFilter.startsWith = startsWith;
    }

    if (match) {
        optimizedFilter.match = match;
    }

    if (length && Object.keys(length).length !== 0) {
        optimizedFilter.length = length;
    }

    return optimizedFilter;
}

function optimizeCompare(init: QueryVisits.Compare<number>): QueryVisits.Compare<number> | null {
    let optimized: QueryVisits.Compare<number> = { ...init };

    if ('lt' in optimized && 'gte' in optimized && optimized.lt <= optimized.gte) {
        return null;
    }

    if ('lt' in optimized && 'gt' in optimized) {
        if (optimized.lt < optimized.gt) {
            return null;
        }

        if (optimized.lt === optimized.gt) {
            optimized = {
                eq: optimized.lt,
            };
        }
    }

    if ('lte' in optimized && 'gt' in optimized && optimized.lte <= optimized.gt) {
        return null;
    }

    if ('lte' in optimized && 'gte' in optimized) {
        if (optimized.lte < optimized.gte) {
            return null;
        }

        if (optimized.lte === optimized.gte) {
            optimized = {
                eq: optimized.lte,
            };
        }
    }

    return optimized;
}

function optimizeObjectFilter(filter: QueryVisits.ObjectFilter): QueryVisits.ObjectFilter | null {
    return Object
        .entries(filter)
        .reduce(
            (acc, [key, stringFilter]) => {
                if (acc === null) {
                    return null;
                }

                const optimizedFilter = optimizeStringFilter(stringFilter);

                if (optimizedFilter === null) {
                    return null;
                }

                if (optimizedFilter) {
                    acc[key] = optimizedFilter;
                }

                return acc;
            },
            {} as QueryVisits.ObjectFilter | null,
        );
}

/**
 * Keep all strings in the list that starts with the given string.
 * RegExp are return each time.
 * @returns
 */
function keepStartsWith<T extends StringRegExp>(list: T[], startsWith: string): T[] {
    return list.filter(
        (str) => {
            if (typeof str === 'string') {
                return str.startsWith(startsWith);
            }

            return true;
        },
    );
}

/**
 * Keep all strings in the list that ends with the given string.
 * RegExp are return each time.
 * @returns
 */
function keepEndsWith<T extends StringRegExp>(list: T[], endsWith: string): T[] {
    return list.filter(
        (str) => {
            if (typeof str === 'string') {
                return str.endsWith(endsWith);
            }

            return true;
        },
    );
}

/**
 * Keep all strings in the list that starts with the given string.
 * RegExp are return each time.
 * @returns
 */
function keepContains<T extends StringRegExp>(list: T[], contains: string[]): T[] {
    return list.filter(
        (str) => {
            if (typeof str === 'string') {
                return testFilters.contains(contains, str);
            }

            return true;
        },
    );
}

function uniqRegExp(list: RegExp[]): RegExp[] {
    return list.reduce(
        ({ listRegExp, listStr }, regExp) => {
            const str = regExp.toString();

            if (!listStr.includes(str)) {
                listStr.push(str);
                listRegExp.push(regExp);
            }

            return {
                listRegExp,
                listStr,
            };
        },
        {
            listRegExp: [],
            listStr: [],
        } as { listRegExp: RegExp[], listStr: string[] },
    ).listRegExp;
}

function uniqStringRexExp(list: StringRegExp[]) : StringRegExp[] {
    const listRegexp : RegExp[] = [];
    const listString = _.uniq(
        list
            .filter((item) => {
                if (item instanceof RegExp) {
                    listRegexp.push(item);
                    return false;
                }

                return true;
            }),
    );

    const uniqRegexp = uniqRegExp(listRegexp);

    return [
        ...listString,
        ...uniqRegexp,
    ];
}

// ============================================================
// Enum filters
function intersectEnumFilter<T extends string>(
    rawA: QueryVisits.FilterEnum<T> | undefined,
    rawB: QueryVisits.FilterEnum<T> | undefined,
) : QueryVisits.FilterEnum<T> | null | undefined {
    if (rawA === undefined) {
        if (rawB === undefined) {
            return undefined;
        }

        const b = formatEnumFilter(rawB);
        return optimizeEnumFilter(b);
    }

    const a = formatEnumFilter(rawA);

    if (rawB === undefined) {
        return optimizeEnumFilter(a);
    }

    const b = formatEnumFilter(rawB);

    const filter : QueryVisits.FilterEnum<T> = {
        exclude: (a.exclude || []).concat(b.exclude || []),
        include: (a.include || []).concat(b.include || []),
    };

    return optimizeEnumFilter(filter);
}

function optimizeEnumFilter<T extends string>(
    filter: QueryVisits.FilterIncludeExcludeList<T>,
) : QueryVisits.FilterIncludeExcludeList<T> | null {
    const optimizedFilter : QueryVisits.FilterEnum<T> = {};

    if (filter.include?.length === 0) {
        return null;
    }

    if (filter.include) {
        optimizedFilter.include = _.uniq(filter.include);
    }

    if (filter.exclude) {
        optimizedFilter.exclude = _.uniq(filter.exclude);
    }

    if (optimizedFilter.include && optimizedFilter.exclude) {
        const intersect = _.intersection(
            optimizedFilter.include,
            optimizedFilter.exclude,
        );

        if (intersect.length) {
            return null;
        }
    }

    return optimizedFilter;
}

// ============================================================
// Exports
export default intersectFilters;
export {
    formatEnumFilter,
    intersectBooleanFilter,
    intersectCompareFilter,
    intersectEnumFilter,
    optimizeCompare,
};
