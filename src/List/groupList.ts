// ============================================================
// Import modules
import * as test from './testFilters';

// ============================================================
// Functions

/**
 * Create groups
 *
 * @param list
 * @param reducer
 * @param having
 * @returns
 */
function groupList<
    T,
    K = string,
    G extends GroupStats<K> = GroupStats<K>,
>(
    list: T[],
    reducer: Reducer<T, K, G>,
    having?: Having<T, K, boolean>,
) : Omit<IGrouping<K, G>, 'criteria'> {
    const init : Map<K, G> = new Map();
    const groupMap = list.reduce(
        (acc, item) => reducer(acc, item),
        init,
    );

    // Filtering groups
    if (having) {
        havingGroups(having, groupMap);
    }

    // Building stats
    const stats = getGroupingStats(groupMap);

    return {
        map: groupMap,
        stats,
    };
}

/**
 * Create a callback that will allow group filtering.
 * @param having
 * @returns
 */
function getGroupFilter<T, K>(
    having: Having<T, K, boolean>,
) : HavingCallback<T, K, boolean> {
    const filter = typeof having === 'function'
        ? having
        : filterGroup.bind(undefined, having);

    return filter;
}

function havingGroups<
    T,
    K = string,
    G extends GroupStats<K> = GroupStats<K>,
>(
    having: Having<T, K, boolean>,
    map: Map<K, G>,
    stats?: GroupingStats,
) {
    const groupStats = stats || getGroupingStats(map);

    const havingCb = getGroupFilter(having);

    map.forEach((group, key) => {
        const keep = havingCb(group, groupStats);

        if (!keep) {
            map.delete(key);
        }
    });

    return map;
}

function getGroupingStats(
    groupMap: Map<any, GroupStats<any>>,
) : GroupingStats {
    let stats : GroupingStats = {
        groups: {
            biggest: null,
            count: 0,
            smallest: null,
        },
        items: {
            count: 0,
        },
    };

    groupMap.forEach((group) => {
        stats = {
            groups: {
                smallest: Math.min(stats.groups.smallest ?? Infinity, group.count),
                biggest: Math.max(stats.groups.biggest ?? -Infinity, group.count),
                count: stats.groups.count + 1,
            },
            items: {
                count: group.count + stats.items.count,
            },
        };
    });

    return stats;
}

function groupByProperty<
    T extends { [key in Property]: K },
    K,
    Property extends string | symbol,
    AddList extends boolean = false,
    RemoveUndef extends boolean = true,
>(
    list: T[],
    property: Property,
    having?: Having<T, K, AddList>,
    addList?: AddList,
    removeUndef?: RemoveUndef,
) : Grouping<T, RemoveUndefIf<K, RemoveUndef>, AddList, { property: Property }> {
    const reducer = getPropertyReducer<
        K,
        T,
        Property,
        AddList,
        RemoveUndef,
        GroupStatsWithList<T, RemoveUndefIf<K, RemoveUndef>, AddList>
    >(property, addList, removeUndef);

    const groupsNoParams = groupList(
        list,
        reducer,
        having,
    );

    return {
        ...groupsNoParams,
        criteria: {
            property,
        },
    };
}

/**
 * Group a list using a callback.
 *
 * Example:
 *
 * const list : Date[] = [...];
 *
 * const callback = (date) => date.getDay();
 *
 * groupByCallback(list, callback); // Will create a group for each day
 *
 * @param list
 * @param callback
 * @param addList
 * @param having
 * @returns
 */
function groupByCallback<
    T,
    K,
    RemoveUndef extends boolean,
    AddList extends boolean,
>(
    list: T[],
    callback: (item: T) => (K | undefined),
    having?: Having<T, K, AddList>,
    addList?: AddList,
    removeUndef?: RemoveUndef,
) : Grouping<T, RemoveUndefIf<K, RemoveUndef>, AddList, { callback: typeof callback }> {
    const reducer = getCallbackReducer(callback, addList, removeUndef);

    const groups = groupList(
        list,
        reducer as Reducer<
            T,
            RemoveUndefIf<K, RemoveUndef>,
            GroupStatsWithList<T, RemoveUndefIf<K, RemoveUndef>, AddList>
        >,
        having,
    );

    return {
        ...groups,
        criteria: {
            callback,
        },
    };
}

function getCallbackReducer<
    K,
    T,
    AddList extends boolean,
    RemoveUndef extends boolean,
    G extends GroupStatsWithList<T, RemoveUndefIf<K, RemoveUndef>, AddList>
        = GroupStatsWithList<T, RemoveUndefIf<K, RemoveUndef>, AddList>,
>(
    callback: (item: T) => K,
    addList?: AddList,
    removeUndef?: RemoveUndef,
) : Reducer<T, RemoveUndefIf<K, RemoveUndef>, G> {
    return (
        acc: Map<RemoveUndefIf<K, RemoveUndef>, G>,
        item: T,
    ) => {
        const groupKey = callback(item) as RemoveUndefIf<K, RemoveUndef>;

        if (groupKey === undefined && removeUndef) {
            return acc;
        }

        let group = acc.get(groupKey);

        if (!group) {
            const newGroup: any = {
                count: 0,
                value: groupKey,
            };

            if (addList) {
                newGroup.list = [];
            }

            group = newGroup as G;

            acc.set(groupKey, newGroup);
        }

        group.count += 1;

        return acc;
    };
}

function getPropertyReducer<
    K,
    T extends { [key in Property]: K },
    Property extends string | symbol,
    AddList extends boolean,
    RemoveUndef extends boolean = false,
    G extends GroupStatsWithList<T, K, AddList> = GroupStatsWithList<T, K, AddList>,
>(
    property: Property,
    addList?: AddList,
    removeUndef?: RemoveUndef,
) : Reducer<T, RemoveUndefIf<K, RemoveUndef>, G> {
    return (
        acc: Map<RemoveUndefIf<K, RemoveUndef>, G>,
        item: T,
    ) => {
        const groupKey = item[property] as unknown as RemoveUndefIf<K, RemoveUndef>;

        // If value excluded, do nothing
        if (removeUndef && groupKey === undefined) {
            return acc;
        }

        let group = acc.get(groupKey);

        if (!group) {
            const newGroup: any = {
                count: 0,
                value: groupKey,
            };

            if (addList) {
                newGroup.list = [];
            }

            group = newGroup as G;

            acc.set(groupKey, newGroup);
        }

        group.count += 1;

        if (addList) {
            (group as any).list.push(item);
        }

        return acc;
    };
}

function filterGroup(
    conditions: HavingFilter,
    group: GroupStats,
    stats: GroupingStats,
) {
    if (conditions.count) {
        return test.number(
            conditions.count,
            group.count,
            stats.items.count,
        );
    }

    return true;
}

// ============================================================
// Types

type Having<T, K, AddList extends boolean> =
    HavingFilter |
    HavingCallback<T, K, AddList>;

type HavingFilter = {
    count?: QueryVisits.Compare<number>,
};

type HavingCallback<
    T,
    K,
    AddList extends boolean> = (
    group: GroupStatsWithList<T, K, AddList>,
    stats: GroupingStats,
) => boolean;

type IGrouping<K, V, Criteria extends object = object> = {
    map: Map<K, V>,
    stats: GroupingStats,
    criteria: Criteria,
};

type Grouping<T, K, AddList extends boolean, Criteria extends object> =
    IGrouping<K, GroupStatsWithList<T, K, AddList>, Criteria>;

type GroupingStats = {
    groups: {
        // Number of groups
        count: number,

        // Count of the smallest groups
        smallest: number | null,

        // Count of the biggest groups
        biggest: number | null,
    },
    items: {
        count: number,
    },
};

type GroupStatsWithList<T, K, AddList extends boolean> =
    GroupStats<K> &

    // Include list ?
    (
        AddList extends true
            ? { list: T[] }
            : GroupStats<K>
    );

type Reducer<T, K, V> = (
        acc: GroupingMap<K, V>,
        item: T,
) => GroupingMap<K, V>;

interface GroupStats<K = any> {
    count: number,
    value: K,
}

type GroupingMap<
    KeyType,
    GroupType,
> = Map<KeyType, GroupType>;

// ============================================================
// Exports
export default groupList;
export {
    getGroupFilter,
    groupByCallback as callback,
    groupByProperty as property,
    havingGroups as having,
};

export type {
    GroupingStats,
    GroupStatsWithList,
    Grouping,
    GroupStats,
    Having,
    HavingCallback,
    HavingFilter,
};
