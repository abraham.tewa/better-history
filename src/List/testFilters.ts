/**
 * Return true if the "match" parameter is in the "list" parameter.
 *
 * Example:
 *
 *  allow = ['a', 'b', /abc/, 'd']
 *
 *  filterAllow(allow, 'a');   // true
 *  filterAllow(allow, 'abc'); // true
 *  filterAllow(allow, 'e');   // false
 *
 * @returns
 */
function testAllow(filter: StringRegExp[], match: string): boolean {
    return filter.some((allow) => {
        if (typeof allow === 'string') {
            return match === allow;
        }

        return allow.test(match);
    });
}

/**
 * Return true if the match contains all the string in the "contains" parameter.
 * @returns
 */
function testContains(filter: string[], match: string): boolean {
    return filter.every((str) => match.indexOf(str) !== -1);
}

/**
 * Return true if the "match" parameter is not in the "list" parameter.
 *
 * Example:
 *
 *  forbid = ['a', 'b', /abc/, 'd']
 *
 *  filterForbid(forbid, 'a');   // false
 *  filterForbid(forbid, 'abc'); // false
 *  filterForbid(forbid, 'ab');  // true
 *  filterForbid(forbid, 'e');   // true
 * @returns
 */
function testForbid(filter: StringRegExp[], match: string): boolean {
    return filter.every((forbid) => {
        if (typeof forbid === 'string') {
            return match !== forbid;
        }

        return !forbid.test(match);
    });
}

function testNumber(
    filter: QueryVisits.Compare<number>,
    value: number,
    total?: number,
) : boolean {
    const pct = total ? value / total : 0;

    if ('gt' in filter) {
        const compareToo = Number.isInteger(filter.gt) ? value : pct;

        if (compareToo <= filter.gt) {
            return false;
        }
    }

    if ('lt' in filter) {
        const compareToo = Number.isInteger(filter.lt) ? value : pct;

        if (compareToo >= filter.lt) {
            return false;
        }
    }

    if ('gte' in filter) {
        const compareToo = Number.isInteger(filter.gte) ? value : pct;

        if (compareToo < filter.gte) {
            return false;
        }
    }

    if ('lte' in filter) {
        const compareToo = Number.isInteger(filter.lte) ? value : pct;

        if (compareToo > filter.lte) {
            return false;
        }
    }

    return true;
}

function testRegexp(filter: RegExp[], value: string): boolean {
    const someDontValidate = filter.some((re) => !re.test(value));
    return !someDontValidate;
}

function testString(
    filter: QueryVisits.FilterStringContent,
    value: string,
) : boolean {
    if (filter.match) {
        const { match } = filter;

        if (typeof match === 'string') {
            if (match !== value) {
                return false;
            }
        } else if (match instanceof RegExp) {
            if (!match.test(value)) {
                return false;
            }
        } else if (Array.isArray(match)) {
            if (!testRegexp(match, value)) {
                return false;
            }
        }
    }

    if (filter.allow) {
        if (!testAllow(filter.allow, value)) {
            return false;
        }
    }

    if (filter.forbid) {
        if (!testForbid(filter.forbid, value)) {
            return false;
        }
    }

    if (filter.contains) {
        if (!testContains(filter.contains, value)) {
            return false;
        }
    }

    if (filter.endsWith) {
        if (!value.endsWith(filter.endsWith)) {
            return false;
        }
    }

    if (filter.startsWith) {
        if (!value.startsWith(filter.startsWith)) {
            return false;
        }
    }

    if (filter.length) {
        const length = typeof filter.length === 'number'
            ? { eq: filter.length }
            : filter.length;

        if (!testNumber(length, value.length)) {
            return false;
        }
    }

    return true;
}

export {
    testAllow as allow,
    testForbid as forbid,
    testContains as contains,
    testNumber as number,
    testRegexp as regExp,
    testString as string,
};
