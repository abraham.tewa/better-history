// ============================================================
// Import modules
// eslint-disable-next-line max-classes-per-file
import * as groupBy from './groupList';

// ============================================================
// Class: AbstractList

let lastId = 0;

abstract class AbstractList<T> extends EventTarget {
    readonly id = getNewId();

    readonly name?: string;

    constructor(
        name?: string,
    ) {
        super();
        this.id = lastId;
        this.name = name;
    }

    abstract get list(): T[];

    /**
     * Return the length of the list
     */
    get length() : number {
        return this.list.length;
    }

    abstract get lastUpdateDate(): Epoch;

    addEventListener(type: ListEventType.itemAdded, callback: (event: AddItemEvent<T>) => void) : void;
    addEventListener(type: ListEventType.itemRemoved, callback: (event: RemoveItemEvent<T>) => void) : void;
    addEventListener(type: ListEventType, callback: (event: CustomEvent) => void) : void {
        super.addEventListener(type, callback);
    }

    removeEventListener(type: ListEventType.itemAdded, callback: (event: AddItemEvent<T>) => void) : void;
    removeEventListener(type: ListEventType.itemRemoved, callback: (event: RemoveItemEvent<T>) => void) : void;
    removeEventListener(type: ListEventType, callback: (event: CustomEvent) => void) : void {
        super.removeEventListener(type, callback);
    }

    dispatchEvent(event: AddItemEvent<T>): boolean;
    dispatchEvent(event: RemoveItemEvent<T>): boolean;
    dispatchEvent(event: CustomEvent): boolean {
        return super.dispatchEvent(event);
    }

    /**
     * Filter by returning a new list linked to this one.
     * The function MUST be a pure function
     *
     * @param cb - Pure function performing the filtering
     */
    filter<U = T>(
        cb: (item: T) => boolean,
        name?: string,
    ) : VirtualList<U, T, AbstractList<T>> {
        const callback = (items: T[]) => (items.filter((item) => cb(item)) as unknown as U[]);

        return new VirtualList<U, T, AbstractList<T>>(
            this,
            callback,
            name,
        );
    }

    /**
     * Filter by returning a new list linked to this one.
     * The function MUST be a pure function.
     * The difference with the "filter" method is that the callback
     * receive the whole list at once.
     */
    filterAll(cb: (items: T[], list: this) => T[]) : VirtualList<T> {
        return new VirtualList<T>(
            this,
            (items: T[]) => cb(items, this),
        );
    }

    /**
     * Loop on each items of the list.
     */
    forEach(cb: (item: T, list: this) => void) : void {
        this.list.forEach((item) => cb(item, this));
    }

    /**
         * Return an unique item of the list.
         * If the list is empty, return undefined
         */
    getOne() : T | undefined {
        return this.list[0];
    }

    groupByCallback<
        K,
        AddList extends boolean,
        RemoveUndef extends boolean,
        Meta,
    >(
        callback: (item: T) => K | undefined,
        having?: groupBy.Having<T, K, AddList>,
        addList?: AddList,
        removeUndef?: RemoveUndef,
        meta?: Meta,
        name?: string,
    ) : VirtualGrouping<T, K, AddList, RemoveUndef, Meta> {
        return new VirtualGrouping(
            this,
            callback,
            having,
            addList,
            removeUndef,
            meta,
            name,
        );
    }

    groupByProperty<
        K,
        Property extends keyof T,
        AddList extends boolean,
        RemoveUndef extends boolean,
        Meta,
    >(
        property: Property,
        having?: groupBy.Having<T, K, AddList>,
        includeList?: AddList,
        removeUndefined?: RemoveUndef,
        meta?: Meta,
    ) : VirtualGrouping<T, K, AddList, RemoveUndef, Meta | { criteria: { property: Property } }> {
        const callback = (item: T) => ((item as any)[property] as K);

        return this.groupByCallback(
            callback,
            having,
            includeList,
            removeUndefined,
            meta || { criteria: { property } },
        );
    }

    map<U>(cb: (item: T, list: AbstractList<T>) => U) : VirtualList<U, T, AbstractList<T>> {
        const list = new VirtualList<U, T, AbstractList<T>>(
            this,
            (items: T[]) => items.map((item) => cb(item, this)),
        );

        return list;
    }

    reduce<U>(cb: ReduceItems<T, U>, init: U) : U {
        return this.list.reduce<U>(
            (acc, item, index) => cb(acc, item, index, this),
            init,
        );
    }

    toString() : string {
        return toString('AbstractList', this.id, this.name);
    }
}

// ============================================================
// Class

class List<T = any> extends AbstractList<T> {
    #lastUpdateDate: Epoch = new Date().getTime();

    #list: T[];

    constructor(
        list: T[] = [],
        name?: string,
    ) {
        super(name);
        this.#list = [...list];
    }

    /**
     * Return the last update date
     */
    get lastUpdateDate(): Epoch {
        return this.#lastUpdateDate;
    }

    /**
     * Return list array
     */
    get list() {
        return this.#list;
    }

    /**
     * Add a new item to the list.
     * @fires ListEventType.itemAdded
     * @returns
     */
    add(items: T[]): this {
        this.#list.push(...items);

        this.#lastUpdateDate = new Date().getTime();

        const event = new CustomEvent(
            ListEventType.itemAdded,
            {
                detail: {
                    items: [...items],
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);
        return this;
    }

    addOne(item: T): this {
        this.#list.push(item);

        this.#lastUpdateDate = new Date().getTime();

        const event = new CustomEvent(
            ListEventType.itemAdded,
            {
                detail: {
                    items: [item],
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);
        return this;
    }

    /**
     * Remove items from the list.
     * If an item is not in the list, it will be ignored.
     * @fires ListEventType.itemRemoved
     */
    remove(items: T[]): T[] {
        const removedItems : T[] = [];

        items.forEach((item) => {
            const index = this.#list.indexOf(item);

            if (index === -1) {
                return;
            }

            removedItems.push(item);

            this.#list.splice(index, 1);
        });

        this.#lastUpdateDate = new Date().getTime();

        const event = new CustomEvent(
            ListEventType.itemRemoved,
            {
                detail: {
                    items: removedItems,
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);

        return removedItems;
    }

    toString() : string {
        return toString('List', this.id, this.name);
    }

    stringify(
        replacer?: (this: any, key: string, value: any) => any,
        space?: string | number,
    ): string {
        return JSON.stringify(this.list, replacer, space);
    }
}

class VirtualList<
    T = any,
    U = T,
    L extends AbstractList<U> = AbstractList<U>,
> extends AbstractList<T> {
    #callback: (items: U[], list: L) => T[];

    #lastUpdateDate: Epoch = new Date().getTime();

    #list: T[] | undefined;

    #state: VirtualListState;

    #unlink?: () => void;

    readonly source: L;

    constructor(
        source: L,
        callback: (items: U[], list: L) => T[],
        name?: string,
    ) {
        super(name);
        this.#callback = callback;
        this.source = source;
        this.#state = VirtualListState.virtual;

        const onItemAdded = this.#onItemAdded.bind(this);
        const onItemRemoved = this.#onItemRemoved.bind(this);

        this.source.addEventListener(ListEventType.itemAdded, onItemAdded);
        this.source.addEventListener(ListEventType.itemRemoved, onItemRemoved);

        this.#unlink = () => {
            this.source.removeEventListener(ListEventType.itemAdded, onItemAdded);
            this.source.removeEventListener(ListEventType.itemRemoved, onItemRemoved);
        };
    }

    get lastUpdateDate(): number {
        return this.#lastUpdateDate;
    }

    get list(): T[] {
        if (!this.#list) {
            this.#list = this.#callback(this.source.list, this.source);

            if (!this.#list) {
                throw new Error('Invalid callback result');
            }

            this.#state = VirtualListState.solid;
        }

        return this.#list;
    }

    get state(): VirtualListState {
        return this.#state;
    }

    toString() : string {
        return toString('VirtualList', this.id, this.name);
    }

    /**
     * Unlink the virtual list from it's parent.
     * The list will no longer receive updates.
     */
    unlink(): void {
        if (!this.#unlink) {
            throw new Error('List already unlinked');
        }

        this.#unlink();
        this.#unlink = undefined;
    }

    #onItemAdded({ detail }: AddItemEvent<U>): void {
        const list = this.#callback(detail.items, this.source);

        // If no items pass the filter, we do nothing
        if (!list.length) {
            return;
        }

        // Updating cache if exists
        if (this.#list) {
            this.#list.push(...list);
        }

        this.#lastUpdateDate = new Date().getTime();

        const event = new CustomEvent(
            ListEventType.itemAdded,
            {
                detail: {
                    items: [...list],
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);
    }

    #onItemRemoved({ detail }: RemoveItemEvent<U>) : void {
        const list = this.#callback(detail.items, this.source);

        // If no items pass the filter, we do nothing
        if (!list.length) {
            return;
        }

        // If no cache, we do not make updates
        if (this.#list) {
            const cachedList = this.#list;

            // Removing items from the cached list
            list.forEach((item) => {
                const index = cachedList.indexOf(item);

                if (index === -1) {
                    return;
                }

                cachedList.splice(index, 1);
            });
        }

        this.#lastUpdateDate = new Date().getTime();

        const event = new CustomEvent(
            ListEventType.itemRemoved,
            {
                detail: {
                    items: [...list],
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);
    }
}

class VirtualGrouping<
    T = any,
    K = any,
    AddList extends boolean = boolean,
    RemoveUndef extends boolean = boolean,
    Meta = any,
> extends EventTarget {
    readonly id = getNewId();

    readonly name?: string;

    #filteredMap: VirtualGroupingMap<T, K, AddList, RemoveUndef> = new Map();

    #getGroupKey: (item: T) => (K | undefined);

    #having?: groupBy.Having<T, K, AddList>;

    #addList?: AddList;

    #removeUndef?: RemoveUndef;

    #map: VirtualGroupingMap<T, K, AddList, RemoveUndef> = new Map();

    #meta: Meta;

    #stats: groupBy.GroupingStats | undefined;

    #state: VirtualListState = VirtualListState.virtual;

    #source: AbstractList<T>;

    #unlink?: () => void;

    /**
     * @param source      - Data source of the grouping
     * @param getGroupKey - Callback returning the key in which each items will belongs
     * @param having      - Filter the groups
     * @param addList - Add the list of items to each group
     * @param removeUndef - If true, then the "undef" group keys will not be counted
     * @param meta        - Meta information about the group
     */
    constructor(
        source: AbstractList<T>,
        getGroupKey: (item: T) => (K | undefined),
        having?: groupBy.Having<T, K, AddList>,
        addList?: AddList,
        removeUndef?: RemoveUndef,
        meta?: Meta,
        name?: string,
    ) {
        super();
        this.name = name;
        this.#source = source;
        this.#getGroupKey = getGroupKey;
        this.#having = having;
        this.#addList = addList;
        this.#removeUndef = removeUndef;
        this.#meta = (meta as Meta);

        const onItemAdded = this.#onItemAdded.bind(this);
        const onItemRemoved = this.#onItemRemoved.bind(this);

        this.#source.addEventListener(ListEventType.itemAdded, onItemAdded);
        this.#source.addEventListener(ListEventType.itemRemoved, onItemRemoved);

        this.#unlink = () => {
            this.#source.removeEventListener(ListEventType.itemAdded, onItemAdded);
            this.#source.removeEventListener(ListEventType.itemRemoved, onItemRemoved);
        };
    }

    get map(): Map<RemoveUndefIf<K, RemoveUndef>, VirtualGroup<T, K, AddList>> {
        if (this.#state !== VirtualListState.solid) {
            this.#createMap();
        }

        return this.#filteredMap;
    }

    get meta(): Meta {
        return this.#meta;
    }

    get source(): AbstractList<T> {
        return this.#source;
    }

    get stats(): groupBy.GroupingStats {
        if (!this.#stats) {
            this.#createMap();
        }

        return this.#stats as groupBy.GroupingStats;
    }

    addEventListener(
        type: GroupingEventType.itemAdded,
        callback: (event: AddItemOnGroupEvent<K>) => void,
    ) : void;
    addEventListener(
        type: GroupingEventType.itemRemoved,
        callback: (event: RemoveItemOnGroupEvent<T, K>) => void,
    ) : void;
    addEventListener(type: GroupingEventType, callback: (event: CustomEvent) => void) : void {
        super.addEventListener(type, callback);
    }

    removeEventListener(
        type: GroupingEventType.itemAdded,
        callback: (event: AddItemOnGroupEvent<K>) => void,
    ) : void;
    removeEventListener(
        type: GroupingEventType.itemRemoved,
        callback: (event: RemoveItemOnGroupEvent<T, K>) => void,
    ) : void;
    removeEventListener(type: GroupingEventType, callback: (event: CustomEvent) => void) : void {
        super.removeEventListener(type, callback);
    }

    dispatchEvent(event: AddItemOnGroupEvent<K>): boolean;
    dispatchEvent(event: RemoveItemOnGroupEvent<T, K>): boolean;
    dispatchEvent(event: CustomEvent): boolean {
        return super.dispatchEvent(event);
    }

    get(key: RemoveUndefIf<K, RemoveUndef>): VirtualGroup<T, K, AddList> | undefined {
        if (!this.#map) {
            this.#createMap();
        }

        return this.#map?.get(key);
    }

    toString() : string {
        return toString('VirtualGrouping', this.id, this.name);
    }

    unlink() {
        if (!this.#unlink) {
            throw new Error('Item already unlinked');
        }

        this.#unlink();
        this.#unlink = undefined;
    }

    #addItems(list: T[], dispatchEvent: boolean = true) : void {
        // List all groups to update by key
        const updateMap = this.#groupByKeys(list);

        const groups : K[] = [];

        updateMap.forEach((items, key) => {
            const created = this.#addToGroup(key, items);
            if (created) {
                groups.push(key);
            }
        });

        this.#refreshFilteredMap();

        if (!dispatchEvent) {
            return;
        }

        const event = new CustomEvent(
            GroupingEventType.itemAdded,
            {
                detail: {
                    groups,
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);
    }

    #addToGroup(groupKey: RemoveUndefIf<K, RemoveUndef>, items: T[]): boolean {
        let group = this.#map.get(groupKey);

        let created = false;

        // Creating group if not exists
        if (!group) {
            created = true;
            const groupStats = {
                count: 0,
                value: groupKey,
                list: [],
            };

            const name = this.name
                ? `${this.name} > ${String(groupKey)}`
                : String(groupKey);

            group = new VirtualGroup(
                groupStats,
                returnIf<List<T>, AddList>(new List<T>(), this.#addList),
                name,
            );

            this.#map.set(groupKey, group);
        }

        group.addItems(items);

        return created;
    }

    #createMap() {
        this.#map = new Map();
        this.#addItems(this.#source.list, false);
        this.#state = VirtualListState.solid;
    }

    /**
     * Group by a list of items
     */
    #groupByKeys(items: T[]) : Map<RemoveUndefIf<K, RemoveUndef>, T[]> {
        // List all groups to update by key
        const updateMap : Map<RemoveUndefIf<K, RemoveUndef>, T[]> = new Map();

        items.forEach((item) => {
            const key = this.#getGroupKey(item) as RemoveUndefIf<K, RemoveUndef>;

            if (key === undefined && this.#removeUndef) {
                return;
            }

            let listUpdated = updateMap.get(key);

            if (!listUpdated) {
                listUpdated = [];
                updateMap.set(key, listUpdated);
            }

            listUpdated.push(item);
        });

        return updateMap;
    }

    /**
     * Method executed each time items are added to the source.
     * The method will update the corresponding groups and the stats.
     * @fires GroupingEventType.itemAdded
     * @returns
     */
    #onItemAdded({ detail }: AddItemEvent<T>): void {
        if (this.#state === VirtualListState.virtual) {
            return;
        }

        this.#addItems(detail.items);
    }

    /**
     * Method executed each time items are added to the source.
     * The method will update the corresponding groups and the stats.
     * @fires GroupingEventType.itemAdded
     * @returns
     */
    #onItemRemoved({ detail }: RemoveItemEvent<T>): void {
        if (this.#state === VirtualListState.virtual) {
            return;
        }

        // List all groups to update by key
        const groupMap = this.#groupByKeys(detail.items);

        const removedItems : Map<K, T[]> = new Map();

        groupMap.forEach((items, key) => {
            const removed = this.#removeFromGroup(key, items);

            if (!removed || removed.length === 0) {
                return;
            }

            removedItems.set(key, removed);
        });

        this.#refreshFilteredMap();

        const event = new CustomEvent(
            GroupingEventType.itemRemoved,
            {
                detail: {
                    items: removedItems,
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);
    }

    #refreshFilteredMap() : void {
        const stats = VirtualGrouping.#calcStats(this.#map);
        this.#filteredMap = new Map(this.#map);

        if (!this.#having) {
            this.#stats = stats;
            return;
        }

        // We need to refresh stats before filtering

        const havingCb = groupBy.getGroupFilter(this.#having);

        this.#filteredMap.forEach((group, key) => {
            const keep = havingCb(group, stats);

            if (!keep) {
                this.#filteredMap.delete(key);
            }
        });

        this.#stats = VirtualGrouping.#calcStats(this.#filteredMap);
    }

    /**
     * Remove a list of items from the group.
     * Return the list of removed items.
     * @returns
     */
    #removeFromGroup(groupKey: RemoveUndefIf<K, RemoveUndef>, items: T[]) : T[] | null {
        const group = this.#map.get(groupKey);

        if (!group) {
            return null;
        }

        return group.removeItems(items);
    }

    static #calcStats(map: Map<any, any>) : groupBy.GroupingStats {
        const stats : groupBy.GroupingStats = {
            items: {
                count: 0,
            },
            groups: {
                count: map.size,
                smallest: Infinity,
                biggest: -Infinity,
            },
        };

        if (map.size === 0) {
            stats.groups.smallest = null;
            stats.groups.biggest = null;
            return stats;
        }

        map.forEach(({ count }) => {
            stats.items.count += count;
            stats.groups.smallest = Math.min(stats.groups.smallest || Infinity, count);
            stats.groups.biggest = Math.max(stats.groups.biggest || -Infinity, count);
        });

        return stats;
    }
}

class VirtualGroup<
    T = any,
    K = any,
    AddList extends boolean = false,
> extends EventTarget {
    #count: number;

    readonly id : number;

    readonly name?: string;

    readonly value: K;

    readonly #list: AddList extends true ? List<T> : undefined;

    constructor(
        group: groupBy.GroupStatsWithList<T, K, AddList>,
        list: AddList extends true ? List<T> : undefined,
        name?: string,
    ) {
        super();
        this.id = getNewId();
        this.name = name;
        this.value = group.value;
        this.#count = group.count;
        this.#list = list;
    }

    get count() {
        return this.#count;
    }

    get list() : AddList extends true ? List<T> : undefined {
        return this.#list;
    }

    addEventListener(
        type: GroupEventType.itemAdded,
        callback: (event: AddItemEvent<T>) => void
    ) : void;
    addEventListener(
        type: GroupEventType.itemRemoved,
        callback: (event: RemoveItemEvent<T>) => void
    ) : void;
    addEventListener(
        type: GroupEventType,
        callback: (event: CustomEvent) => void,
    ) : void {
        super.addEventListener(type, callback);
    }

    addItems(items: T[]): void {
        this.#count += items.length;

        if (this.#list) {
            this.#list.add(items);
        }

        const event = new CustomEvent(
            GroupEventType.itemAdded,
            {
                detail: {
                    items: [...items],
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);
    }

    removeItems(items: T[]): T[] {
        let removedItems: T[];

        if (this.#list) {
            removedItems = this.#list.remove(items);
        } else {
            removedItems = [...items];
        }

        this.#count -= removedItems.length;

        const event = new CustomEvent(
            GroupEventType.itemRemoved,
            {
                detail: {
                    items: removedItems,
                    target: this,
                },
            },
        );

        this.dispatchEvent(event);

        return removedItems;
    }

    toString() : string {
        return toString('VirtualGroup', this.id, this.name);
    }
}

function toString(
    type: string,
    id: number,
    name?: string,
) : string {
    let str = `<${type}.${id}`;

    if (name) {
        str += `.${name}`;
    }

    str += '>';

    return str;
}

function getNewId() : number {
    const id = lastId;

    lastId += 1;

    return id;
}

function returnIf<T, Q extends boolean>(value: T, add: Q | undefined): Q extends true ? T : undefined {
    if (add === true) {
        return value as Q extends true ? T : undefined;
    }

    return undefined as Q extends true ? T : undefined;
}

// ============================================================
// Types

type AddItemEvent<T> = CustomEvent<{ items: T[] }>;
type RemoveItemEvent<T> = CustomEvent<{ items: T[] }>;

type VirtualGroupingMap<T, K, AddList extends boolean, RemoveUndef extends boolean>
    = Map<RemoveUndefIf<K, RemoveUndef>, VirtualGroup<T, K, AddList>>;

type AddItemOnGroupEvent<K> = CustomEvent<{
    groups: K[],
    target: VirtualGrouping,
}>;

type RemoveItemOnGroupEvent<T, K> = CustomEvent<{
    items: Map<K, T[]>,
    target: VirtualGrouping,
}>;

enum VirtualListState {
    // The list has not been created yet
    virtual = 'virtual',

    // Solid
    solid = 'solid',
}

enum ListEventType {
    itemAdded = 'List/itemAdded',
    itemRemoved = 'List/itemRemoved',
}

enum GroupEventType {
    itemAdded = 'Group/itemAdded',
    itemRemoved = 'Group/itemRemoved',
}

enum GroupingEventType {
    itemAdded = 'Grouping/itemAdded',
    itemRemoved = 'Grouping/itemRemoved',
}

type ReduceItems<T, U> = (
    acc: U,
    item: T,
    index: number,
    list: AbstractList<T>,
) => U;

// ============================================================
// Exports
export default List;
export {
    AbstractList,
    ListEventType,
    GroupEventType,
    GroupingEventType,

    VirtualGroup,
    VirtualGrouping,
    VirtualList,
    VirtualListState,
};

export type {
    AddItemEvent,
    RemoveItemEvent,
};
