function formatEnumFilter<T extends string>(
    filter: QueryVisits.FilterEnum<T>,
) : QueryVisits.FilterIncludeExcludeList<T> {
    if (typeof filter === 'string') {
        return {
            include: [filter as T],
        };
    }

    if (Array.isArray(filter)) {
        return {
            include: ([...filter] as T[]),
        };
    }
    return filter;
}

export {
    formatEnumFilter,
};
