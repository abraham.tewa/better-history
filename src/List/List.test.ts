// ============================================================
// Import modules
import List, { ListEventType, VirtualList, VirtualListState } from './List';

// ============================================================
// Tests
describe('List', () => {
    describe('filter', () => {
        it('Return a virtual list', () => {
            const list = new List<number>([]);
            const vList = list.filter(() => true);

            expect(vList).toBeInstanceOf(VirtualList);
        });

        it('Return a filtered list', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const list = new List(array);
            const vList = list.filter((item) => item % 2 === 0);

            expect(vList.list).toEqual([2, 4, 6, 8, 10]);

            expect(vList.source).toBe(list);
        });
    });

    describe('add', () => {
        it('Accept empty list', () => {
            const list = new List<number>([]);

            list.add([]);

            expect(list.list).toEqual([]);
        });

        it('Add each items', () => {
            const list = new List<number>([]);

            list.add([0]);

            expect(list.list).toEqual([0]);
        });

        it('It return this', () => {
            const list = new List<number>([]);

            const result = list.add([0]);

            expect(result).toBe(list);
        });

        it('Fire event', () => {
            const list = new List<number>([]);

            const spy = jest.fn();
            list.addEventListener(ListEventType.itemAdded, spy);

            list.add([0, 1]);

            expect(spy).toHaveBeenCalledTimes(1);

            const event = spy.mock.calls[0][0];

            expect(event.type).toEqual(ListEventType.itemAdded);
            expect(event.detail).toEqual({
                items: [0, 1],
                target: list,
            });

            expect(event.detail.target).toEqual(list);
        });
    });

    describe('remove', () => {
        it('Accept empty list', () => {
            const list = new List<number>([1, 2]);

            list.remove([]);

            expect(list.list).toEqual([1, 2]);
        });

        it('Remove each items', () => {
            const list = new List<number>([1, 2]);

            list.remove([1]);

            expect(list.list).toEqual([2]);
            expect(list.length).toEqual(1);
        });

        it('Do not remove items that is not present', () => {
            const list = new List<number>([1, 2]);

            list.remove([0]);

            expect(list.list).toEqual([1, 2]);
            expect(list.length).toBe(2);
        });

        it('Do not remove items that is not present (mixed)', () => {
            const list = new List<number>([1, 2, 3, 4, 5]);

            const removed = list.remove([0, 2, 3]);

            expect(list.list).toEqual([1, 4, 5]);
            expect(list.length).toBe(3);
            expect(removed).toEqual([2, 3]);
        });

        it('Fire an event', () => {
            const list = new List<number>([1, 2, 3, 4, 5]);

            const spy = jest.fn();
            list.addEventListener(ListEventType.itemRemoved, spy);

            list.remove([0, 2, 3]);

            expect(spy).toHaveBeenCalledTimes(1);

            const event = spy.mock.calls[0][0];

            expect(event.type).toEqual(ListEventType.itemRemoved);
            expect(event.detail).toEqual({
                items: [2, 3],
                target: list,
            });

            expect(event.detail.target).toEqual(list);
        });
    });
});

describe('VirtualList', () => {
    describe('state', () => {
        it('is virtual at creation', () => {
            const list = createList();

            const vList = list.filter(() => true);

            expect(vList.state).toEqual(VirtualListState.virtual);
        });

        it('is solid once list created', () => {
            const list = createList();

            const vList = list.filter(() => true);

            // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            vList.list;

            expect(vList.state).toEqual(VirtualListState.solid);
        });

        it('is solid once list created', () => {
            const list = createList();

            const vList = list.filter(() => true);

            // eslint-disable-next-line @typescript-eslint/no-unused-expressions
            vList.length;

            expect(vList.state).toEqual(VirtualListState.solid);
        });
    });

    describe('length', () => {
        it('it a correct value', () => {
            const list = createList();

            const vList = list.filter(() => true);

            expect(vList.length).toEqual(list.length);
        });

        it('it change with parent source update', () => {
            const list = createList();

            const vList = list.filter(() => true);

            const initialLength = vList.length;

            list.add([10]);

            expect(vList.length).toEqual(initialLength + 1);
        });
    });

    describe('source: List', () => {
        it('receive automatically new items', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const list = new List(array);
            const vList = list.filter((item) => item % 2 === 0);

            list.add([12]);
            expect(vList.list).toEqual([2, 4, 6, 8, 10, 12]);

            list.add([14]);
            expect(vList.list).toEqual([2, 4, 6, 8, 10, 12, 14]);
        });

        it('only receive items matching the filter', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const list = new List(array);
            const vList = list.filter((item) => item % 2 === 0);

            list.add([11, 12, 13]);
            expect(vList.list).toEqual([2, 4, 6, 8, 10, 12]);

            list.add([14, 15, 16]);
            expect(vList.list).toEqual([2, 4, 6, 8, 10, 12, 14, 16]);
        });

        it('loose automatically removed items', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const list = new List(array);
            const vList = list.filter((item) => item % 2 === 0);

            list.remove([8]);
            expect(vList.list).toEqual([2, 4, 6, 10]);

            list.remove([6]);
            expect(vList.list).toEqual([2, 4, 10]);
        });

        it('Fire an event if added item match filter', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const list = new List(array);
            const vList = list.filter((item) => item % 2 === 0);

            const spy = jest.fn();
            vList.addEventListener(ListEventType.itemAdded, spy);

            list.add([11, 12]);

            expect(spy).toHaveBeenCalledTimes(1);

            const event = spy.mock.calls[0][0];

            expect(event.type).toEqual(ListEventType.itemAdded);
            expect(event.detail).toEqual({
                items: [12],
                target: vList,
            });

            expect(event.detail.target).toEqual(vList);
        });

        it('Fire an event if removed item match filter', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const list = new List(array);
            const vList = list.filter((item) => item % 2 === 0);

            const spy = jest.fn();
            vList.addEventListener(ListEventType.itemRemoved, spy);

            list.remove([6, 7]);

            expect(spy).toHaveBeenCalledTimes(1);

            const event = spy.mock.calls[0][0];

            expect(event.type).toEqual(ListEventType.itemRemoved);
            expect(event.detail).toEqual({
                items: [6],
                target: vList,
            });

            expect(event.detail.target).toEqual(vList);
        });
    });

    describe('source: VirtualList', () => {
        it('receive automatically new items', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
            const list = new List(array);
            const listOdd = list.filter((item) => item % 2 === 0);
            const listThird = list.filter((item) => item % 3 === 0);
            const listOddThird = listOdd.filter((item) => item % 3 === 0);

            expect(listOdd.list).toEqual([2, 4, 6, 8, 10, 12, 14, 16, 18, 20]);
            expect(listThird.list).toEqual([3, 6, 9, 12, 15, 18]);
            expect(listOddThird.list).toEqual([6, 12, 18]);

            list.add([21, 22, 23, 24]);

            expect(listOdd.list).toEqual([2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24]);
            expect(listOddThird.list).toEqual([6, 12, 18, 24]);
            expect(listThird.list).toEqual([3, 6, 9, 12, 15, 18, 21, 24]);
        });

        it('Added items that do not match filter are not returned', () => {
            const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
            const list = new List(array);
            const vList = list.filter((item) => item % 2 === 0);

            expect(vList.list).toEqual([2, 4, 6, 8, 10]);

            list.add([11, 12]);

            expect(vList.list).toEqual([2, 4, 6, 8, 10, 12]);
        });
    });
});

function createList() {
    const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    return new List(array);
}
