// ============================================================
// Import modules
import { formatEnumFilter } from './utils';
import * as test from './testFilters';

// ============================================================
// Functions
function filterObject<
    T extends { [ key in keyof Filter]: string | undefined },
    Filter extends { [ key in keyof T]?: QueryVisits.FilterString },
>(
    list: T[],
    filter: Filter,
): T[] {
    return Object
        .entries(filter)
        .reduce(
            (acc, [property, propertyFilter]) => {
                if (!propertyFilter) {
                    return acc;
                }

                return filterPropertyString(
                    acc,
                    propertyFilter as QueryVisits.FilterString,
                    property,
                );
            },
            list,
        );
}

function filterPropertyString<
    T extends { [ key in Property ]: string | undefined },
    Property extends string | symbol,
>(
    list: T[],
    filter: QueryVisits.FilterString,
    property: Property,
) : T[] {
    let filteredList = list;

    if (typeof filter === 'string') {
        filteredList = filteredList.filter(({ [property]: value }) => value === filter);
        return filteredList;
    }

    if (filter instanceof RegExp) {
        filteredList = filteredList.filter(({ [property]: value }) => value && filter.test(value));
        return filteredList;
    }

    if (Array.isArray(filter)) {
        filteredList = filteredList.filter(({ [property]: value }) => value && test.allow(filter, value));
        return filteredList;
    }

    if (filter.allow) {
        const { allow } = filter;
        filteredList = filteredList.filter(({ [property]: value }) => value && test.allow(allow, value));
    }

    if (filter.forbid) {
        const { forbid } = filter;
        filteredList = filteredList.filter(({ [property]: value }) => value && test.forbid(forbid, value));
    }

    if (filter.startsWith) {
        const { startsWith } = filter;
        filteredList = filteredList.filter(({ [property]: value }) => value?.startsWith(startsWith));
    }

    if (filter.endsWith) {
        const { endsWith } = filter;
        filteredList = filteredList.filter(({ [property]: value }) => value?.endsWith(endsWith));
    }

    if (filter.contains) {
        const { contains } = filter;
        filteredList = filteredList.filter(({ [property]: value }) => value && test.contains(contains, value));
    }

    if (filter.length) {
        const length = typeof filter.length === 'number'
            ? { eq: filter.length }
            : filter.length;

        filteredList = filteredList.filter(({ [property]: value }) => value && test.number(length, value.length));
    }

    return filteredList;
}

function filterEnumProperty<
    T extends { [key in Property]: Enum },
    Enum extends string,
    Property extends string | symbol,
>(
    list: T[],
    filter: QueryVisits.FilterEnum<Enum>,
    property: Property,
): T[] {
    let filteredList = list;

    const formattedFilter = formatEnumFilter(filter);

    if (formattedFilter.include) {
        const { include } = formattedFilter;
        filteredList = filteredList.filter((item) => include.includes(item[property]));
    }

    if (formattedFilter.exclude) {
        const { exclude } = formattedFilter;
        filteredList = filteredList.filter((item) => !exclude.includes(item[property]));
    }

    return filteredList;
}

function filterNumberProperty<
    T extends { [key in Property]: number },
    Property extends string | symbol,
>(
    list: T[],
    filter: QueryVisits.Compare<Date>,
    property: Property,
) : T[] {
    let filteredList = list;

    if ('gt' in filter) {
        const gt = filter.gt.getTime();
        filteredList = filteredList.filter((item) => item[property] > gt);
    }

    if ('lt' in filter) {
        const lt = filter.lt.getTime();
        filteredList = filteredList.filter((item) => item[property] < lt);
    }

    if ('gte' in filter) {
        const gte = filter.gte.getTime();
        filteredList = filteredList.filter((item) => item[property] >= gte);
    }

    if ('lte' in filter) {
        const lte = filter.lte.getTime();
        filteredList = filteredList.filter((item) => item[property] >= lte);
    }

    return filteredList;
}

// ============================================================
// Exports
export {
    filterEnumProperty as enumProperty,
    filterObject as object,
    filterNumberProperty as numberProperty,
};
