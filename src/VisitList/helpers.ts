// ============================================================
// Import modules
import UrlVisitEntry from './UrlVisitEntry';
import FileVisitEntry from './FileVisitEntry';
import { VisitItemType } from '../constants';

// ============================================================
// Functions
function getOrCreate(entry: DbVisitItem) {
    if (entry.type === VisitItemType.file) {
        return FileVisitEntry.getOrCreate(entry);
    }

    return UrlVisitEntry.getOrCreate(entry);
}

export {
    getOrCreate,
};
