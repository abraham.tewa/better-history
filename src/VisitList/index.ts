import UrlVisitEntry from './UrlVisitEntry';
import FileVisitEntry from './FileVisitEntry';
import TmpVisitEntry from './VisitEntry';

type UVisitEntry = UrlVisitEntry | FileVisitEntry;

declare global {
    type VisitEntry = TmpVisitEntry;
}

// ============================================================
// Exports

export {
    getOrCreate,
} from './helpers';

export {
    default as VisitEntry,
    NavigationSession,
} from './VisitEntry';

export { default as UrlVisitEntry } from './UrlVisitEntry';
export { default as FileVisitEntry } from './FileVisitEntry';

export { default } from './VisitList';

export type {
    UVisitEntry,
};

export type {
    NavigationSessionGroupBy,
    ProductGroupBy,
    RealmGroupBy,
    VisitTimeGroupBy,
} from './VisitList';
