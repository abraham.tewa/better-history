// ============================================================
// Import packages
import _ from 'underscore';

// ============================================================
// Import modules
import {
    intersectFilter,
    intersectCompareFilter,
    intersectEnumFilter,
    intersectBooleanFilter,
} from '../List';

// ============================================================
// Functions
function intersectFilters(
    a: QueryVisits.Filter,
    b: QueryVisits.Filter,
) {
    const filter : QueryVisits.Filter = {};

    // ====================
    // Filter: visitId
    if (a.visitId) {
        filter.visitId = b.visitId
            ? _.intersection(a.visitId, b.visitId)
            : [...a.visitId];
    } else if (b.visitId) {
        filter.visitId = [...b.visitId];
    }

    // ====================
    // Filter: file
    if (typeof a.file === 'boolean' && typeof b.file === 'boolean' && b.file !== a.file) {
        return null;
    }

    if (a.file === false || b.file === false) {
        filter.file = false;
    } else if (typeof a.file === 'object' && typeof b.file === 'object') {
        const fileFilter = intersectFilter(
            a.file === true ? undefined : a.file,
            b.file === true ? undefined : b.file,
        );

        if (fileFilter === null) {
            return null;
        }

        filter.file = fileFilter;
    }

    // ====================
    // Filter: url
    if (typeof a.url === 'boolean' && typeof b.url === 'boolean' && b.url !== a.url) {
        return null;
    }

    if (a.url === false || b.url === false) {
        filter.url = false;
    } else {
        const urlFilter = intersectFilter(
            a.url === true ? undefined : a.url,
            b.url === true ? undefined : b.url,
        );

        if (urlFilter === null) {
            return null;
        }

        filter.url = urlFilter;
    }

    // ====================
    // Filter: visitTime
    const visitTimeFilter = intersectCompareFilter(a.visitTime, b.visitTime);

    if (visitTimeFilter === null) {
        return null;
    }

    filter.visitTime = visitTimeFilter;

    // ====================
    // Filter: transition
    const transition = intersectEnumFilter(a.transition, b.transition);

    if (transition === null) {
        return null;
    }

    if (transition !== undefined) {
        filter.transition = transition;
    }

    // ====================
    // Filter: navigationRoot
    const navigationRoot = intersectBooleanFilter(a.navigationRoot, b.navigationRoot);

    if (navigationRoot === null) {
        return null;
    }

    if (navigationRoot !== undefined) {
        filter.navigationRoot = navigationRoot;
    }

    // ====================
    // Filter: realm
    const realm = intersectEnumFilter(a.realm, b.realm);

    if (realm === null) {
        return null;
    }

    if (realm !== undefined) {
        filter.realm = realm;
    }

    // ====================
    // Filter: product
    const product = intersectEnumFilter(a.product, b.product);

    if (product === null) {
        return null;
    }

    if (product !== undefined) {
        filter.product = product;
    }

    return filter;
}

// ============================================================
// Exports
export default intersectFilters;
