// ============================================================
// Import packages
import { VisitItemType } from '../constants';

// ============================================================
// Import modules
import VisitEntry from './VisitEntry';

// ============================================================
// Class
class FileVisitEntry extends VisitEntry {
    readonly type = VisitItemType.file;

    readonly drive?: string;

    readonly folder: string;

    readonly path: string;

    readonly filename: string;

    readonly extension?: string;

    constructor(entry: DbVisitFile) {
        super(VisitItemType.file, entry);
        this.drive = entry.drive;
        this.folder = entry.folder;
        this.path = entry.path;
        this.filename = entry.filename;
        this.extension = entry.extension;
    }

    toString() {
        return `FileVisitEntry.${this.visit.visitId}`;
    }

    static getOrCreate(dbVisit: DbVisitFile) : FileVisitEntry {
        return VisitEntry.getOrCreateProtected(dbVisit, FileVisitEntry);
    }
}

export default FileVisitEntry;
export type {
    FileVisitEntry as Type,
};
