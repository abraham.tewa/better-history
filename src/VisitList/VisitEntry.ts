/* eslint-disable max-classes-per-file */
// ============================================================
// Import packages
import type {
    History,
} from 'webextension-polyfill';

import {
    VisitItemType,
} from '../constants';
import {
    Product,
    PageType,
    UrlParser,
    Realm,
} from '../Realm';

type VisitEntryRef = {
    entry?: VisitEntry,
    children: VisitEntry[],
    reloads: VisitEntry[],
};

const visitsMap: Map<string, VisitEntryRef> = new Map();

enum VisitEntryEvent {
    parentDeclared = 'parentDeclared',
}

// ============================================================
// Class

class VisitEntry extends EventTarget implements IVisitEntry {
    readonly directChildren: VisitEntry[];

    readonly id: string;

    readonly reloads: VisitEntry[];

    readonly title: string | undefined;

    readonly type: VisitItemType;

    readonly url: URL;

    readonly visit: History.VisitItem;

    #navigation : NavigationInfo | undefined;

    #navigationSession: NavigationSession | undefined;

    #parseInfo: UrlParser | undefined | null = null;

    #entryRef: VisitEntryRef;

    static navigationRoots = new Map();

    static navigationSessionRoots = new Set();

    constructor(
        type: VisitItemType,
        dbVisit: IDbVisitItem,
    ) {
        super();
        this.id = dbVisit.visit.visitId;
        this.type = type;
        this.visit = dbVisit.visit;
        this.title = dbVisit.title;
        this.url = new URL(dbVisit.url);

        const { referringVisitId } = this.visit;

        let entryRef = visitsMap.get(this.id);

        if (!entryRef) {
            entryRef = {
                entry: this,
                children: [],
                reloads: [],
            };
            visitsMap.set(this.id, entryRef);
        }

        this.#entryRef = entryRef;

        this.reloads = this.#entryRef.reloads;

        this.directChildren = this.#entryRef.children;

        if (referringVisitId === '-1') {
            this.#navigationSession = new NavigationSession(this);
        }
    }

    /**
     * List of all children of the entry.
     * An entry child is an entry to which the user navigate to from
     * the current one.
     * Reloads are not children.
     * Notice that all reloads will return the same child list.
     * The child list do not contain reloads.
     *
     * For example, if I do the current navigation:
     *
     * Current page:
     *      A: https://www.google.com?q=facebook
     *
     * Opening pages:
     *      B: https://www.facebook.com
     *
     * Doing 3 refresh of B:
     *      C: https://www.facebook.com
     *      D: https://www.facebook.com
     *      E: https://www.facebook.com
     *
     * Then:
     *
     *  A.children = [B];
     *
     */
    #children: VisitEntry[] | undefined;

    get children() : VisitEntry[] {
        if (!this.#children) {
            if (this.isAReload()) {
                this.#children = this.reloadOf?.children || [];
            } else {
                this.#children = [
                    ...this.directChildren,
                    ...this.reloads.map((reload) => reload.directChildren),
                ].flat();
            }
        }

        return this.#children;
    }

    // ====================
    // depth
    #depth: number | undefined;

    get depth() : number {
        if (this.#depth === undefined) {
            this.#depth = Math.max(
                -1,
                ...this.directChildren.map((child) => child.depth),
                ...this.reloads.map((reload) => reload.depth - 1),
            ) + 1;
        }

        return this.#depth;
    }

    /**
     * Descendants
     *
     * List of all descendants entries.
     * Descendants are entries to which the user navigates from the current
     * one. This list will also contains all descendants of the reloads.
     */
    #descendants: VisitEntry[] | undefined;

    get descendants(): VisitEntry[] {
        if (!this.#descendants) {
            if (this.reloadOf) {
                this.#descendants = this.reloadOf.descendants;
            } else {
                this.#descendants = [
                    ...this.children,
                    ...this.children.map((child) => child.descendants),
                ].flat();
            }
        }

        return this.#descendants;
    }

    /**
     * Title to display for the entry
     */
    #displayTitle: string | undefined;

    get displayTitle(): string {
        if (this.#displayTitle === undefined) {
            this.#displayTitle = this.parseInfo
                ? this.parseInfo.displayTitle || this.title || ''
                : this.title || '';
        }

        return this.#displayTitle;
    }

    get navigation() : NavigationInfo {
        if (!this.#navigation) {
            let info : NavigationInfo;

            if (!this.#parent) {
                // If the entry has no referrer, then it's complete
                info = {
                    complete: !this.hasReferrer(),
                    depth: 0,
                    path: [],
                };
            } else {
                const referrerInfo = this.#parent.navigation;

                info = {
                    path: [...referrerInfo.path, this.#parent],
                    complete: referrerInfo.complete,
                    depth: referrerInfo.depth + 1,
                };
            }

            this.#navigation = info;
        }

        return this.#navigation;
    }

    get navigationInfoCompleted(): boolean {
        return this.hasReferrer() && !this.#parent;
    }

    /**
     * Display information about the navigation session
     */
    get navigationSession(): NavigationSession {
        if (!this.#navigationSession) {
            if (!this.navigationInfoCompleted) {
                throw new Error('Navigation not completed');
            }

            this.#navigationSession = this.referrer?.navigationSession;
        }

        return this.#navigationSession as NavigationSession;
    }

    get pageTypes(): PageType[] | undefined {
        return this.parseInfo?.pageTypes;
    }

    /**
     * Parent of the entry.
     *
     * The parent is the entry from which the user came from.
     */
    #parent: VisitEntry | undefined | null = null;

    get parent(): VisitEntry | undefined {
        if (this.#parent === null) {
            // Searching for the parent
            // If the parent hasn't been retrieved yet, we set the "null"
            // value so the next time the parent may be retrieved again.
            if (this.visit.referringVisitId === '-1') {
                this.#parent = undefined;
            } else {
                this.#parent = visitsMap.get(this.visit.referringVisitId)?.entry || null;
            }
        }

        return this.#parent || undefined;
    }

    get parseInfo(): UrlParser | undefined {
        if (this.#parseInfo === null) {
            this.#parseInfo = this.product?.parse(this);
        }

        return this.#parseInfo;
    }

    // eslint-disable-next-line class-methods-use-this
    get product(): Product | undefined {
        return undefined;
    }

    // eslint-disable-next-line class-methods-use-this
    get realm() : Realm | undefined {
        return undefined;
    }

    get referrer(): VisitEntry | undefined {
        return this.#parent || undefined;
    }

    /**
     * Return the initial entry of which this visit is the reload.
     */
    get reloadOf(): VisitEntry | undefined {
        if (!this.isAReload()) {
            return undefined;
        }

        if (this.parent?.isAReload()) {
            return this.parent.reloadOf;
        }

        return this.parent;
    }

    hasReferrer() : boolean {
        return this.visit.referringVisitId !== '-1';
    }

    /**
     * Indicate if this entry is a reload entry or not
     */
    isAReload() {
        return this.visit.transition === 'reload';
    }

    toString() {
        return `VisitEntry.${this.visit.visitId}`;
    }

    #declareParent(entry: VisitEntry) {
        this.#parent = entry;

        const event = new CustomEvent(VisitEntryEvent.parentDeclared);

        this.dispatchEvent(event);
    }

    static get<T extends VisitEntry = VisitEntry>(id: string) : T | undefined {
        return visitsMap.get(id)?.entry as T;
    }

    protected static getOrCreateProtected<
        T extends VisitEntry,
        U extends IDbVisitItem,
    >(
        dbVisit: U,
        Constructor: { new(dbVisit: U): T },
    ) : T {
        const existing = VisitEntry.get(dbVisit.visit.visitId) as T;
        if (existing) {
            if (!(existing instanceof Constructor)) {
                throw new Error('Entry doesn\'t match an Constructor object');
            }

            return existing;
        }

        const visitEntry = new Constructor(dbVisit);
        VisitEntry.#declare(visitEntry);
        return visitEntry;
    }

    static #declare(visitEntry: VisitEntry) {
        let mapRef = visitsMap.get(visitEntry.id);

        if (mapRef?.entry && mapRef?.entry !== visitEntry) {
            throw new Error(`Entry already recorded: ${visitEntry}`);
        }

        if (mapRef) {
            mapRef.entry = visitEntry;
        } else {
            mapRef = {
                entry: visitEntry,
                children: [],
                reloads: [],
            };

            visitsMap.set(visitEntry.id, mapRef);
        }

        const parentId = visitEntry.visit.referringVisitId;

        if (parentId !== '-1') {
            let parentMapRef = visitsMap.get(parentId);

            if (!parentMapRef) {
                parentMapRef = {
                    reloads: [],
                    children: [],
                };

                visitsMap.set(parentId, parentMapRef);
            }

            if (visitEntry.isAReload()) {
                parentMapRef.reloads.push(visitEntry);
            } else {
                parentMapRef.children.push(visitEntry);
            }
        }
    }
}

type NavigationInfo = {
    complete: boolean,
    depth: number
    path: VisitEntry[],
};

class NavigationSession {
    readonly rootVisit: VisitEntry;

    #depth: number;

    #count: number | undefined;

    static readonly #list : NavigationSession[] = [];

    constructor(root: VisitEntry) {
        NavigationSession.#list.push(this);
        this.rootVisit = root;
        this.#depth = root.navigation.depth;
    }

    addToSession(visit: VisitEntry) {
        this.invalidate();

        this.#depth = Math.max(
            this.#depth,
            visit.navigation.depth,
        );
    }

    invalidate() {
        this.#count = undefined;
    }

    get depth() : number {
        return this.#depth;
    }

    static getAll() : NavigationSession[] {
        return NavigationSession.#list;
    }
}

export default VisitEntry;
export {
    NavigationSession,
};
