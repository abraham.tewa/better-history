// ============================================================
// Import packages
import { VisitItemType } from '../constants';

// ============================================================
// Import modules
import VisitEntry from './VisitEntry';

import {
    Product,
    Realm,
} from '../Realm';

// ============================================================
// Class
class UrlVisitEntry extends VisitEntry {
    readonly type = VisitItemType.url;

    readonly href: string;

    readonly domain: string | undefined;

    readonly hostname: string;

    readonly tld: string | undefined;

    readonly path: string;

    readonly protocol: string;

    constructor(entry: DbVisitUrl) {
        super(VisitItemType.url, entry);
        this.href = entry.url;
        this.domain = entry.domain;
        this.hostname = entry.hostname;
        this.tld = entry.tld;
        this.path = entry.path;
        this.protocol = entry.protocol;
    }

    #product: Product | null | undefined = null;

    get product(): Product | undefined {
        if (this.#product === null) {
            this.#product = this.domain
                ? Realm.getProduct(this.domain, this.url)
                : undefined;
        }

        return this.#product;
    }

    #realm: Realm | undefined | null = null;

    get realm() : Realm | undefined {
        if (this.#realm === null) {
            this.#realm = this.domain
                ? Realm.get(this.domain)
                : undefined;
        }

        return this.#realm;
    }

    toString() {
        return `UrlVisitEntry.${this.visit.visitId}`;
    }

    static getOrCreate(dbVisit: DbVisitUrl) : UrlVisitEntry {
        return VisitEntry.getOrCreateProtected(dbVisit, UrlVisitEntry);
    }
}

export default UrlVisitEntry;
