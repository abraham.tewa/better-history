// ============================================================
// Import packages
// eslint-disable-next-line max-classes-per-file
import { History } from 'webextension-polyfill';

// ============================================================
// Import modules
import {
    AbstractList,
    formatEnumFilter,
    groupBy,
    filters,
    VirtualList,
    VirtualGrouping,
    ListEventType,
} from '../List';

import { NavigationSession } from './VisitEntry';
import {
    TimeRange,
    TimeUnit,
    VisitItemType,
} from '../constants';
import {
    Product,
    Realm,
} from '../Realm';

import type {
    Grouping,
    Having,
} from '../List';

import * as utils from '../utils';
import { getOrCreate } from './helpers';
import { getList } from '../db';

// ============================================================
// Class
class VisitList<
    T extends IVisitEntry = IVisitEntry,
> extends VirtualList<T> {
    readonly sourceFilter?: QueryVisits.Filter | ((items: T[]) => T[]);

    #newestVisit: T | undefined;

    #oldestVisit: T | undefined;

    static readonly global = new VisitList(getList());

    constructor(
        source: AbstractList<T>,
        filter?: QueryVisits.Filter | ((items: T[]) => T[]),
        name?: string,
    ) {
        let callback: (items: T[]) => T[];

        if (filter) {
            callback = typeof filter === 'function'
                ? filter
                : (list: T[]) => filterVisitList(list, filter);
        } else {
            callback = (list: T[]) => [...list];
        }

        super(source, callback, name);

        if (filter) {
            this.sourceFilter = filter;
        }

        this.addEventListener(
            ListEventType.itemAdded,
            ({ detail: { items } }) => {
                // Updating latestVisit
                if (this.#newestVisit) {
                    this.#updateNewest(items);
                }

                if (this.#oldestVisit) {
                    this.#updateOldest(items);
                }
            },
        );

        this.addEventListener(
            ListEventType.itemRemoved,
            ({ detail: { items } }) => {
                // Updating latestVisit
                if (this.#newestVisit) {
                    const isLatest = items.some((item) => item.visit.visitId === this.#newestVisit?.visit.visitId);

                    // If the latest has been removed, we delete it
                    if (isLatest) {
                        this.#newestVisit = undefined;
                    }
                }

                if (this.#oldestVisit) {
                    const isOldest = items.some((item) => item.visit.visitId === this.#oldestVisit?.visit.visitId);

                    if (isOldest) {
                        this.#oldestVisit = undefined;
                    }
                }
            },
        );
    }

    #updateOldest(items: T[]) {
        this.#oldestVisit = items.reduce(
            (previous, current) => {
                if (!previous) {
                    return current;
                }

                return VisitList.compareVisits(previous, current) < 0
                    ? current
                    : previous;
            },
            this.#oldestVisit,
        );
    }

    #updateNewest(items: T[]) {
        this.#newestVisit = items.reduce(
            (previous, current) => {
                if (!previous) {
                    return current;
                }

                return VisitList.compareVisits(previous, current) > 0
                    ? current
                    : previous;
            },
            this.#newestVisit,
        );
    }

    get oldestVisit(): T | undefined {
        if (!this.#oldestVisit) {
            this.#updateOldest(this.list);
        }

        return this.#oldestVisit;
    }

    get newestVisit(): T | undefined {
        if (!this.#newestVisit) {
            this.#updateNewest(this.list);
        }

        return this.#newestVisit;
    }

    getFromId(entryId: VisitEntryId): IVisitEntry | undefined {
        return this.filter((entry) => entry.visit.visitId === entryId).getOne();
    }

    groupByNavigationSession(
        having?: Having<
            IVisitEntry,
            NavigationSession,
            false
        >,
    ): NavigationSessionGroupBy {
        const callback = (visit: IVisitEntry) => {
            if (!visit.navigationInfoCompleted) {
                return undefined;
            }

            return visit.navigationSession;
        };

        const groups = {
            ...groupBy.callback(
                this.list as any,
                callback,
                having,
                false,
                false,
            ),
            criteria: {},
        };

        return groups;
    }

    groupByRealm<
        IncludeList extends boolean,
        RemoveUndef extends boolean,
    >(
        having?: Having<
            IVisitEntry,
            Realm | undefined,
            IncludeList
        >,
        includeList?: IncludeList,
        removeUndef?: RemoveUndef,
    ): RealmGroupBy<IncludeList> {
        const groups = groupBy.property(
            this.list as unknown as IVisitEntry[],
            'realm',
            having,
            includeList,
            removeUndef,
        );

        return groups;
    }

    #groupByProductWithListNoUndef?: VirtualGrouping<T, Product, true, true>;

    #groupByProductWithoutListNoUndefined?: VirtualGrouping<T, Product, false, true>;

    groupByProduct<
        IncludeList extends boolean,
        RemoveUndef extends boolean,
    >(
        having?: Having<
            T,
            RemoveUndefIf<Product | undefined, RemoveUndef>,
            IncludeList
        >,
        addList?: IncludeList,
        removeUndef?: RemoveUndef,
    ): VirtualGrouping<
        T,
        RemoveUndefIf<Product | undefined, RemoveUndef>,
        IncludeList,
        RemoveUndef,
        { criteria: { property: 'product' } }
    > {
        if (!having && removeUndef) {
            if (addList) {
                this.#groupByProductWithListNoUndef = this.groupByProperty(
                    'product',
                    having,
                    addList as true,
                    removeUndef as true,
                );

                return this.#groupByProductWithListNoUndef as any;
            }

            this.#groupByProductWithoutListNoUndefined = this.groupByProperty(
                'product',
                having,
                addList as false,
                removeUndef as true,
            );

            return this.#groupByProductWithoutListNoUndefined as any;
        }

        const grouping = this.groupByProperty(
            'product',
            having,
            addList,
            removeUndef,
        );

        return grouping as any;
    }

    groupByVisitTime<
        IncludeList extends boolean,
    >(
        range: TimeRange,
        timeStep: TimeUnit,
        having?: Having<
            T,
            number,
            IncludeList
        >,
        includeList?: IncludeList,
    ): VisitTimeGroupBy<T, IncludeList> {
        const callback = (visit: T) => {
            const { visitTime } = visit.visit;

            if (!visitTime) {
                return undefined;
            }

            return utils.date.toUnit(visitTime, range, timeStep);
        };

        const groups = this.groupByCallback(
            callback,
            having,
            includeList,
            true,
            {
                criteria: {
                    visitTime: {
                        range,
                        timeStep,
                    },
                },
            },
        );

        return groups;
    }

    /**
     * Query a list of visits
     * @param filter
     */
    query(
        filter: QueryVisits.Filter = {},
        listName?: string,
    ) : VisitList<T> {
        return new VisitList<T>(this, filter, listName);
    }

    toString() {
        return `VisitList.${this.id} > ${this.source}`;
    }

    static filter<T extends IVisitEntry>(
        visitList: VisitList<T>,
        cb: (item: T) => boolean,
    ) : VisitList<T> {
        const callback = (items: T[]) => items.filter((item) => cb(item));

        return new VisitList<T>(
            visitList,
            callback,
        );
    }

    static compareVisits(latest: IVisitEntry, current: IVisitEntry): number {
        if (!latest.visit.visitTime) {
            return 1;
        }

        if (!current.visit.visitTime) {
            return -1;
        }

        return current.visit.visitTime - latest.visit.visitTime;
    }

    static fromDbVisitItem(
        list: DbVisitItem[],
    ) : IVisitEntry[] {
        return list.map((item) => getOrCreate(item));
    }
}

function filterVisitList<T extends IVisitEntry>(
    list: T[],
    filter?: QueryVisits.Filter,
) : T[] {
    let filteredList = list;

    if (!filter) {
        return [...list];
    }

    // Filter: include reload
    if (typeof filter.reloads === 'boolean') {
        filteredList = filteredList.filter((item) => item.isAReload() === filter.reloads);
    }

    // Filter: visit time
    if (filter.visitTime) {
        filteredList = filterVisitTime(filter.visitTime, filteredList);
    }

    // Filter: transition
    if (filter.transition) {
        filteredList = filterTransition(filter.transition, filteredList);
    }

    // Filter: URL
    if (filter.url === false) {
        filteredList = filteredList.filter((item) => item.type !== VisitItemType.url);
    } else if (typeof filter.url === 'object') {
        filteredList = filters.object(filteredList, filter.url) as unknown as T[];
    }

    // Filter: file
    if (filter.file === false) {
        filteredList = filteredList.filter((item) => item.type !== VisitItemType.file);
    } else if (typeof filter.file === 'object') {
        filteredList = filters.object(filteredList, filter.file);
    }

    // Filter: navigation root
    if (filter.navigationRoot === true) {
        filteredList = filteredList.filter((entry) => !entry.hasReferrer());
    } else if (filter.navigationRoot === false) {
        filteredList = filteredList.filter((entry) => entry.hasReferrer());
    }

    // Filter: visit id
    if (filter.visitId?.length) {
        const visitIdList = filter.visitId;
        filteredList = filteredList.filter((entry) => visitIdList.includes(entry.visit.visitId));
    }

    return filteredList;
}

function filterTransition<T extends IVisitEntry>(
    filter: QueryVisits.FilterEnum<History.TransitionType>,
    initialList: T[],
): T[] {
    const list = initialList;
    const formattedFilter = formatEnumFilter(filter);

    if (formattedFilter.include) {
        const { include } = formattedFilter;
        list.filter(({ visit }) => include.includes(visit.transition));
    }

    if (formattedFilter.exclude) {
        const { exclude } = formattedFilter;
        list.filter(({ visit }) => !exclude.includes(visit.transition));
    }

    return list;
}

function filterVisitTime<T extends IVisitEntry>(
    filter: QueryVisits.Compare<number>,
    initialList: T[],
) : T[] {
    let list = initialList;

    if ('gt' in filter) {
        list = list.filter(({ visit }) => (visit.visitTime as number) > filter.gt);
    }

    if ('lt' in filter) {
        list = list.filter(({ visit }) => (visit.visitTime as number) < filter.lt);
    }

    if ('gte' in filter) {
        list = list.filter(({ visit }) => (visit.visitTime as number) >= filter.gte);
    }

    if ('lte' in filter) {
        list = list.filter(({ visit }) => (visit.visitTime as number) >= filter.lte);
    }

    return list;
}

// ============================================================
// Types

type NavigationSessionGroupBy =
    Grouping<IVisitEntry, NavigationSession, false, {}>;

type VisitTimeGroupBy<T extends IVisitEntry = IVisitEntry, IncludeList extends boolean = false> =
    VirtualGrouping<
        T,
        number,
        IncludeList,
        true,
        { criteria: { visitTime: { range: TimeRange, timeStep: TimeUnit } } }
    >;

type RealmGroupBy<IncludeList extends boolean = false> =
    Grouping<IVisitEntry, Realm | undefined, IncludeList, { property: 'realm' }>;

type ProductGroupBy<
    IncludeList extends boolean,
    RemoveUndefined extends boolean,
> =
    Grouping<IVisitEntry, RemoveUndefIf<Product | undefined, RemoveUndefined>, IncludeList, { property: 'product' }>;

// ============================================================
// Exports
export default VisitList;
export type {
    NavigationSessionGroupBy,
    ProductGroupBy,
    RealmGroupBy,
    VisitTimeGroupBy,
};
