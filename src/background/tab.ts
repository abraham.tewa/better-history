// ============================================================
// Import packages
import { Tabs } from 'webextension-polyfill';

// ============================================================
// Functions
async function open(after: Tabs.Tab) {
    const views = browser.extension.getViews({
        type: 'tab',
    });

    if (views.length) {
        const tab = await browser.tabs.update(
            views[0].tabId as number,
            {
                active: true,
            },
        );

        await browser.windows.update(
            tab.windowId as number,
            {
                focused: true,
            },
        );

        return;
    }

    const tab = await browser.tabs.create({
        active: true,
        index: after.index + 1,
        url: '/better-history',
        windowId: after.windowId,
    });

    browser.tabs.executeScript(
        tab.id as number,
        {
            code: `window.tabId = ${tab.id};`,
        },
    );
}

// ============================================================
// Exports
export {
    open,
};
