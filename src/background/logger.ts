const prefix = '[background]';

function info(...args: any[]) {
    // eslint-disable-next-line no-console
    console.log(prefix, ...args);
}

export {
    info,
};
