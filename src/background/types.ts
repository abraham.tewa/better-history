type VisitsSave = {
    visits: DbVisitItem[],
    from: Date,
    to: Date,
};

export type {
    VisitsSave,
};
