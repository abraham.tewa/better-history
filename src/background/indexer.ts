// ============================================================
// Import packages
import type {
    History,
} from 'webextension-polyfill';
import Dexie from 'dexie';
import { isIP } from 'is-ip';

// ============================================================
// Import modules
import { runtime } from './dialog';
import { getDb } from '../db';
import * as logger from './logger';
import { LocalStorageKeys, Tables, VisitItemType } from '../constants';
import type {
    VisitsSave,
} from './types';

let ready = false;

const INDEX_INTERVAL = 1000 * 10; // 5s
const INDEX_RANGE = 24 * 60 * 60 * 1000; // 5h

// ============================================================
// Module's constants and variables
function isReady() {
    return ready;
}

async function initialize(onSave?: (save: VisitsSave) => void) {
    const db = await getDb();

    logger.info('Initialize indexer...');
    start(
        db,
        INDEX_INTERVAL,
        INDEX_RANGE,
        onSave,
    );

    ready = true;

    let saving = false;

    browser.history.onVisited.addListener(
        async () => {
            if (saving) {
                return;
            }

            saving = true;

            const saved = await saveNewest(
                db,
                INDEX_RANGE,
            );
            onSave?.(saved);

            saving = false;
        },
    );
}

/**
 * Start the indexer
 * @param db
 * @param interval
 * @param range
 * @param onSave
 * @returns
 */
function start(
    db: Dexie,
    interval: number,
    range: number,
    onSave?: (save: VisitsSave) => void,
) {
    logger.info('Starting indexation');

    setTimeout(
        async () => {
            await saveOldest(db, range);

            setInterval(
                async () => {
                    const visits = await saveOldest(db, range);
                    onSave?.(visits);
                },
                interval,
            );
        },
        0,
    );
}

function getIndexerStats() : IndexStats | undefined {
    const indexerStatsStr = localStorage.getItem(LocalStorageKeys.IndexerStats);

    if (!indexerStatsStr) {
        return undefined;
    }

    const parse = JSON.parse(indexerStatsStr);

    return {
        ...parse,
        oldest: new Date(parse.oldest),
        newest: new Date(parse.newest),
    };
}

function updateIndexerStats(stats?: Partial<IndexStats>) : IndexStats {
    const previous = getIndexerStats() || ({
        indexed: 0,
        oldest: new Date(),
        newest: new Date(),
    } as IndexStats);

    const current : IndexStats = {
        oldest: stats?.oldest || previous.oldest,
        newest: stats?.newest || previous.newest,
        indexed: stats?.indexed || previous.indexed,
    };

    const str = JSON.stringify(current);

    localStorage.setItem(LocalStorageKeys.IndexerStats, str);

    runtime.notify.indexUpdate(current, previous);

    return current;
}

async function saveNewest(
    db: Dexie,
    delta: number,
) : Promise<VisitsSave> {
    const indexerStats = getIndexerStats() || updateIndexerStats({});

    const oldestDate = indexerStats.newest;
    const newestDate = new Date(oldestDate.getTime() + delta);

    const visits = await saveVisitsBetweenDates(
        db,
        oldestDate,
        newestDate,
    );

    const newestVisitTime = visits.reduce(
        (newest, { visit }) => {
            if (!visit.visitTime) {
                return newest;
            }

            return Math.max(newest, visit.visitTime);
        },
        oldestDate.getTime(),
    );

    updateIndexerStats({
        newest: new Date(newestVisitTime + 1),
        indexed: indexerStats.indexed + visits.length,
    });

    return {
        visits,
        from: oldestDate,
        to: newestDate,
    };
}

async function saveOldest(
    db: Dexie,
    delta: number,
) : Promise<VisitsSave> {
    const indexerStats = getIndexerStats() || updateIndexerStats({});

    const newestDate = indexerStats.oldest;
    const oldestDate = new Date(newestDate.getTime() - delta);

    const visits = await saveVisitsBetweenDates(
        db,
        oldestDate,
        newestDate,
    );

    updateIndexerStats({
        oldest: oldestDate,
        indexed: indexerStats.indexed + visits.length,
    });

    return {
        visits,
        from: oldestDate,
        to: newestDate,
    };
}

/**
 * Search all visits done between two dates.
 * The visits will be saved into the database.
 * @param db
 * @param startDate
 * @param endDate
 * @returns
 */
async function saveVisitsBetweenDates(
    db: Dexie,
    startDate: Date,
    endDate: Date,
) : Promise<DbVisitItem[]> {
    const history = await browser.history.search({
        startTime: startDate,
        endTime: endDate,
        text: '',
        maxResults: 100_000_000,
    });

    const listUrl = history
        .filter(({ url }) => url);

    const arrayVisits = await Promise.all(
        listUrl.map(getVisits),
    );

    const startTime = startDate.getTime();
    const endTime = endDate.getTime();

    const visits = arrayVisits
        .flat()
        .filter(({ visit }) => (visit.visitTime as number) >= startTime
            && (visit.visitTime as number) < endTime);

    const visitsCollection = db.table<DbVisitItem>(Tables.visits);

    try {
        await visitsCollection.bulkAdd(visits);
    } catch (err) {
        console.error('Error while adding visits', err, visits);
    }

    return visits;
}

async function getVisits(item: History.HistoryItem): Promise<DbVisitItem[]> {
    const { url } = item as { url: string };

    const visits = await browser.history.getVisits({
        url,
    });

    return visits.map((visit) => {
        const urlObject = new URL(url);

        return urlObject.protocol === 'file'
            ? toDbVisitFile(visit, item)
            : toDbVisitUrl(visit, item);
    });
}

function toDbVisitFile(
    visit: History.VisitItem,
    item: History.HistoryItem,
): DbVisitFile {
    const file = new URL(item.url as string);

    const [,,...path] = file.pathname.split('/');

    const drive = path[0];
    const folder = [...path].slice(0, -1).join('/');
    const filename = path.at(-1) as string;

    const fileSplit = filename.split('.');

    const extension = fileSplit.length > 1
        ? fileSplit.at(-1)
        : '';

    return {
        drive,
        extension,
        filename,
        folder,
        path: path.join('/'),
        title: item.title,
        type: VisitItemType.file,
        url: (item.url as string),
        visit,
    };
}

function toDbVisitUrl(
    visit: History.VisitItem,
    historyItem: History.HistoryItem,
) : DbVisitUrl {
    const url = new URL(historyItem.url as string);

    let tld;
    let domain;

    if (!isIP(url.hostname)) {
        const domainSplit = url.hostname.split('.');
        tld = domainSplit.at(-1) as string;
        domain = `${domainSplit.at(-2)}.${tld}`;
    }

    return {
        visit,
        type: VisitItemType.url,
        url: url.href,
        domain,
        hostname: url.hostname,
        path: url.pathname,
        protocol: url.protocol,
        tld,
        title: historyItem.title,
    };
}

// ============================================================
// Exports
export {
    initialize,
    isReady,
    start,
};
