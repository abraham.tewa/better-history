// ============================================================
// Import modules
import * as logger from './logger';
import * as indexer from './indexer';
import * as dialog from './dialog';
import * as tab from './tab';
import * as db from '../db';
import VisitList from '../VisitList';

// ============================================================
// Functions
async function initialize() {
    logger.info('Declaring API');
    dialog.initialize();
    browser.browserAction.onClicked.addListener(tab.open);

    logger.info('Initializing db...');
    await db.initialize();

    logger.info('Starting indexer...');
    await indexer.initialize(({ visits }) => {
        db.getList().add(
            VisitList.fromDbVisitItem(visits),
        );
    });

    logger.info('Ready');
}

// ============================================================
// Run
initialize();
