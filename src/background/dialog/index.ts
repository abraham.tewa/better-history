// ============================================================
// Import modules
import initializeAnswers from './runtime';

// ============================================================
// Functions
function initialize() {
    initializeAnswers();
}

// ============================================================
// Exports
export {
    initialize,
};

export * as runtime from './runtime';
