// ============================================================
// Import modules
import { DialogMessageType } from '../../constants';

// ============================================================
// Notifications

function notifyReady() {
    sendMessage(
        DialogMessageType.Ready,
    );
}

function notifyIndexUpdate(
    current: IndexStats,
    previous: IndexStats | undefined,
) {
    sendMessage(
        DialogMessageType.IndexerUpdate,
        {
            current,
            previous,
        },
    );
}

// ============================================================
// Listeners

// ============================================================
// Initialize
async function browserListener(message: Dialog.Message) {
    switch (message.type) {
    default:
        throw new Error('Unknown type');
    }
}

function initialize() {
    browser.runtime.onMessage.addListener(browserListener);
}

function sendMessage(type: DialogMessageType, parameters?: object) : void {
    browser.runtime.sendMessage({
        type,
        parameters,
    });
}

// ============================================================
// Exports
export default initialize;

export const notify = {
    indexUpdate: notifyIndexUpdate,
    ready: notifyReady,
};
