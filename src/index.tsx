import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './realms';
import * as db from './db';

import initializeApp from './main';

async function initialize() {
    await db.initialize();

    initializeApp();
}

initialize();
