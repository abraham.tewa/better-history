// ============================================================
// Import packages
import React from 'react';
import { useTranslation } from 'react-i18next';

// ============================================================
// Import modules
import { Product, SearchEnginePageType } from '../Realm';
import { VisitEntry } from '../VisitList';

// ============================================================
// Components
function TitleCell(
    {
        stack = [],
        visit,
    } : {
        stack?: VisitEntry[],
        visit: VisitEntry,
    },
) {
    const { t } = useTranslation();

    const url = visit.url.href;

    const { product } = visit;

    let title: string | undefined;

    let tags: string[] | undefined;

    if (visit.parseInfo?.isHomepage) {
        tags = ['historyTable.products.homepage'];
        title = (visit.product as Product).name;
    } else if (product) {
        tags = visit.pageTypes?.map(
            (pageType) => `product.pageType.${pageType}`,
        );
    }

    if (!title) {
        title = visit.displayTitle;
    }

    const liTags = (tags || []).map((tag) => (
        <span key={tag} className="bh-tag badge bg-primary">
            {t(tag)}
        </span>
    ));

    const badge = stack.length > 1
        ? (<span className="badge bg-primary">{stack.length}</span>)
        : undefined;

    let results: JSX.Element | undefined;

    if (visit.parseInfo?.traits.searchEngine?.pageType === SearchEnginePageType.resultList) {
        const openedResults = stack.reduce(
            (count, current) => {
                const opened = current.parseInfo?.traits.searchEngine?.openedResults || [];
                return count + opened.length;
            },
            0,
        );

        results = (
            <>
                <br />
                <span>
                    Opened:
                    {' '}
                    {openedResults}
                </span>
            </>
        );
    }

    return (
        <div className="bh-link">
            <div className="title">
                {liTags}
                <a
                    target="_blank"
                    href={url}
                    className="link-primary"
                    rel="noreferrer"
                >
                    {title}
                    {badge}
                </a>
            </div>
            {results}
        </div>
    );
}

TitleCell.defaultProps = {
    stack: [],
};

export default TitleCell;
