const prefix = '[front]';

function info(...args: any[]) {
    console.log(prefix, ...args);
}

export {
    info,
};
