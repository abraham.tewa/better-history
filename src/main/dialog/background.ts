import { DialogMessageType } from '../../constants';
import VisitList, { VisitEntry } from '../../VisitList';

// ============================================================
// Functions

async function query <T extends VisitEntry>(filter: QueryVisits.Filter = {}) {
    const { list } = await sendMessage(
        DialogMessageType.Query,
        {
            filter,
        },
    );

    return new VisitList<T>(list);
}

// ============================================================
// Listeners
function listenToIndexUpdate({
    previous,
    current,
} : {
    previous: IndexStats | undefined,
    current: IndexStats,
}) {
    const event = new CustomEvent(
        EventType.IndexerUpdate,
        {
            detail: {
                previous,
                current,
            },
        },
    );

    background.dispatchEvent(event);
}

// ============================================================
// Helpers
async function sendMessage(type: DialogMessageType, parameters?: object) : Promise<any> {
    const results = await browser.runtime.sendMessage({
        type,
        parameters,
    });

    return results;
}

// ============================================================
// Initialization
function runtimeListener(message: Dialog.Message) {
    switch (message.type) {
    case DialogMessageType.IndexerUpdate:
        listenToIndexUpdate(message.parameters);
        break;
    default:
    }
}

function initialize() {
    browser.runtime.onMessage.addListener(runtimeListener);
}

// ============================================================
// Types
enum EventType {
    IndexerUpdate = 'indexerUpdate',
}

// ============================================================
// Exports
const background = new EventTarget() as Background;

interface Background extends EventTarget {
    query: typeof query,

    EventType: typeof EventType
}

background.query = query;
background.EventType = EventType;

export default background;
export {
    initialize,

    query,

    EventType,
};
