// ============================================================
// Import modules
import * as background from './background';

// ============================================================
// Functions
function initialize() {
    background.initialize();
}

// ============================================================
// Exports
export {
    initialize,
};

export { default as background } from './background';
export {
    EventType as BackgroundEventType,
} from './background';
