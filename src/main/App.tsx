// ============================================================
// Import packages
import React from 'react';
import {
    BrowserRouter,
    Routes,
    Route,
    useParams,
    Navigate,
} from 'react-router-dom';

// ============================================================
// Import modules
import Header from './Header';
import HomePage from './Homepage';
import Address from './Address';

// ============================================================
// Component
function App() {
    return (
        <BrowserRouter basename="/better-history">
            <Header />
            <Routes>
                <Route path="" element={<HomePage />} />
                <Route path="/" element={<HomePage />} />
                <Route path="/entry/:entryId" element={<AddressRoute />} />
                <Route
                    path=""
                    element={<Navigate to="" replace />}
                />
            </Routes>
        </BrowserRouter>
    );
}

function AddressRoute() {
    const { entryId } = useParams<AddressRouteParams>();

    if (!entryId) {
        return <> </>;
    }

    return (
        <Address entryId={entryId} />
    );
}

type AddressRouteParams = {
    entryId: string,
};

// ============================================================
// Exports
export default App;
