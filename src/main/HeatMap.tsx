// ============================================================
// Import packages
import React, {
    useMemo,
} from 'react';
import Color from 'color';
import _ from 'underscore';
import Chart from 'react-apexcharts';
import classnames from 'classnames';

import type { ApexOptions } from 'apexcharts';
import * as utils from '../utils';

// ============================================================
// Import modules
import {
    TimeRange, TimeUnit,
} from '../constants';
import { VisitTimeGroupBy } from '../VisitList';
import { GroupStats } from '../List';
import useGroupingMap from './hook/useGroupingMap';

// ============================================================
// Components
function HeatMap({
    id,
    className: classNameProps,
    grouping,
    formatterOptions,
} : {
    id: string,
    className?: string,
    grouping: VisitTimeGroupBy,
    formatterOptions: FormatterOptions,
}) {
    const options = useOptions(id, grouping, formatterOptions);

    if (!grouping) {
        return (
            <div />
        );
    }

    const className = classnames(
        classNameProps,
    );

    return (
        <Chart
            className={className}
            options={options}
            series={options.series}
            type="heatmap"
        />
    );
}

HeatMap.defaultProps = {
    className: undefined,
};

// ============================================================
// Hooks

function useOptions(
    id: string,
    data: VisitTimeGroupBy,
    formatterOptions: FormatterOptions,
) : ApexOptions {
    const series = useSeries(data);

    const options : ApexOptions = useMemo(
        () => {
            if (!data) {
                return {};
            }

            const opts : ApexOptions = {
                chart: {
                    id,
                    height: '700px',
                },
                legend: {
                    show: false,
                },
                plotOptions: {
                    heatmap: {
                        distributed: true,
                        colorScale: {
                            ranges: toColorScale(data, 50),
                        },
                    },
                },
                dataLabels: {
                    enabled: false,
                },
                xaxis: {
                    type: 'category',
                    categories: [0, 1, 2, 3, 4, 5, 6],
                    labels: {
                        formatter: (value) => {
                            const name = utils.date.getWeekDayName(formatterOptions.locale, Number(value));
                            return name[0].toUpperCase();
                        },
                        showDuplicates: true,
                    },
                },
                series,
            };

            return opts;
        },
        [
            data,
            formatterOptions,
            id,
            series,
        ],
    );

    return options;
}

function useSeries(
    data: VisitTimeGroupBy,
) : ApexAxisChartSeries {
    const map = useGroupingMap(data);
    const { range, timeStep } = data.meta.criteria.visitTime;

    const series : ApexAxisChartSeries = useMemo(
        () => {
            const toSeriesCb = ALLOWED_COMBINATIONS[range]?.[timeStep];

            if (!toSeriesCb) {
                throw new Error('Invalid combination');
            }

            const list = Array.from(map.values());

            const seriesData : { [key: number]: SeriesItem[] } = _.groupBy(
                list.map(toSeriesCb),
                'series',
            );

            return Object
                .entries(seriesData)
                .map(([name, items]) => toApexSeries(name, items));
        },
        [range, timeStep, map],
    );

    return series;
}

// ============================================================
// Custom maps
const ALLOWED_COMBINATIONS : {
    [key in TimeRange]?: {
        [unit in TimeUnit]?: GetSeriesInfo
    }
} = {
    [TimeRange.day]: {
        [TimeUnit.hour]: toSeriesInfo.bind(undefined, undefined),
        [TimeUnit.amPm]: toSeriesInfo.bind(undefined, undefined),
    },

    [TimeRange.week]: {
        [TimeUnit.hour]: toSeriesInfo.bind(undefined, 7),
        [TimeUnit.amPm]: toSeriesInfo.bind(undefined, 2),
    },
    [TimeRange.year]: {
        [TimeUnit.week]: toSeriesInfo.bind(undefined, 52),
    },
    [TimeRange.always]: {
        [TimeUnit.day]: (group: GroupStats<number>) => {
            const date = new Date(group.value as number);

            return {
                series: date.getDay().toString(),
                x: utils.date.getFirstWeekDay(date).getTime().toString(),
                value: group.count,
            };
        },
        [TimeUnit.month]: (group: GroupStats<number>) => {
            const date = new Date(group.value as number);

            return {
                series: date.getMonth().toString(),
                x: date.getFullYear().toString(),
                value: group.count,
            };
        },
    },
};

// ============================================================
// Helpers
function toColorScale(
    data: VisitTimeGroupBy,
    nb: number = 5,
) : ColorScaleItem[] {
    const { biggest } = data.stats.groups;

    if (!biggest) {
        return [];
    }

    const pow = (biggest * 0.9) ** (1 / nb);

    const listRanges = new Array(nb)
        .fill(undefined)
        .map((pass, index) => Math.floor(pow ** (index + 1)));

    // const { biggest: max } = data[Stats].groups;
    // const steps = max ? Math.floor(max / (nb - 2)) : undefined;
    const initialColor = '#CDE9FE';

    // const listRanges = !steps
    //     ? []
    //     : new Array(nb)
    //         .fill(undefined)
    //         .map((pass, index) => index * steps);

    const colorModifier = (color: Color, index: number) => {
        const value = listRanges[index + 1] ?? biggest;
        return color.lightness(100).darken(value / (biggest * 1.3));
    };

    const list = utils.color.getGradient(initialColor, listRanges.length, colorModifier)
        .reduce(
            (acc, color, index) => {
                const current : ColorScaleItem = {
                    from: listRanges[index],
                    to: listRanges[index + 1],
                    color,
                };

                acc.push(current);
                return acc;
            },
            [] as ColorScaleItem[],
        );

    list[0].from = 0;
    list[list.length - 1].to = biggest + 1;

    return list;
}

function toSeriesInfo(
    divisor: number | undefined,
    data: GroupStats<number>,
) : SeriesItem {
    if (!divisor) {
        return {
            series: data.value?.toString(),
            value: data.count,
        };
    }

    return {
        series: data.value === undefined ? undefined : Math.floor(data.value / divisor).toString(),
        value: data.count,
        x: data.value === undefined ? undefined : (data.value % divisor).toString(),
    };
}

function toApexSeries(
    name: string,
    series: SeriesItem[],
): ArrayElement<ApexAxisChartSeries> {
    return {
        name,
        data: series.map((item) => ({
            x: item.x,
            y: item.value,
        })),
    };
}

// ============================================================
// Types

type ColorScaleItem = {
    from?: number
    to?: number
    color?: string
    foreColor?: string
    name?: string
};

// Code from https://stackoverflow.com/questions/41253310/typescript-retrieve-element-type-information-from-array-type
type ArrayElement<ArrayType extends readonly unknown[]> =
  ArrayType extends readonly (infer ElementType)[] ? ElementType : never;

type SeriesItem = {
    series: string | Symbol | undefined,
    x?: string,
    value: number,
};

type GetSeriesInfo = GetSeriesInfoFromNumber | GetSeriesInfoFromEpoch;

type GetSeriesInfoFromNumber = (group: GroupStats<number>) => SeriesItem;

type GetSeriesInfoFromEpoch = (group: GroupStats<number>) => SeriesItem;

// ============================================================
// Exports
export default HeatMap;
