import React from 'react';
import { NavigationSessionGroupBy } from '../VisitList';

function NavigationProfile({
    profile,
}: {
    profile?: NavigationSessionGroupBy
}) {
    if (!profile) {
        return (<> </>);
    }
}

NavigationProfile.defaultProps = {
    profile: undefined,
};

export default NavigationProfile;
