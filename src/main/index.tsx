import React from 'react';
import ReactDOM from 'react-dom';

import './i18n';
import App from './App';
import reportWebVitals from '../reportWebVitals';
import * as dialog from './dialog';
import * as db from './db';
import * as log from './log';
import * as UseDb from './hook/useDb';
import * as UseStats from './hook/useStats';
import * as UseCurrentEntry from './hook/useCurrentEntry';

async function initializeApp() {
    ReactDOM.render(
        <React.StrictMode>
            <UseDb.Context>
                <UseStats.Context>
                    <UseCurrentEntry.Context>
                        <App />
                    </UseCurrentEntry.Context>
                </UseStats.Context>
            </UseDb.Context>
        </React.StrictMode>,
        document.getElementById('root'),
    );

    // If you want to start measuring performance in your app, pass a function
    // to log results (for example: reportWebVitals(console.log))
    // or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
    reportWebVitals();
}

async function initialize() {
    log.info('Initialize db...');
    await db.initialize();

    log.info('Declaring API...');
    dialog.initialize();

    log.info('Displaying app...');
    await initializeApp();

    log.info('Ready');
}

export default initialize;
