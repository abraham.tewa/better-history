// ============================================================
// Import packages
import React, {
    ChangeEvent, useCallback, useMemo, useState,
} from 'react';
import { useTranslation } from 'react-i18next';
import { Product } from '../../Realm';
import VisitList, { VisitEntry } from '../../VisitList';
import useGroupingMap from '../hook/useGroupingMap';

// ============================================================
// Components

function ProductFilter(
    {
        list,
        onProductSelect,
    } : {
        list: VisitList<VisitEntry>
        onProductSelect: (product: Product | undefined) => void,
    },
) {
    const { t } = useTranslation();
    const [currentProduct, setCurrentProduct] = useState<Product | undefined>();

    const groupByProduct = useMemo(
        () => list.groupByProduct(undefined, false, true),
        [list],
    );

    const productMap = useGroupingMap(groupByProduct);

    const products = useMemo(
        () => {
            const listProducts = Array
                .from(productMap.values())
                .map((group) => group.value);

            listProducts.sort((a, b) => a.name.localeCompare(b.name));

            const options = listProducts.map((product) => (
                <option key={product.id} value={product.id}>{product.name}</option>
            ));

            return options;
        },
        [productMap],
    );

    const onSelectChange = useCallback(
        (event: ChangeEvent<HTMLSelectElement>) => {
            const productName = event.target.value;
            const product = Product.get(productName);

            setCurrentProduct(product);
            onProductSelect(product);
        },
        [onProductSelect],
    );

    return (
        <select
            className="form-select form-select-sm"
            onChange={onSelectChange}
            defaultValue=""
        >
            <option
                value=""
                disabled={!currentProduct}
            >
                {t('historyTable.filters.products.placeholder')}
            </option>
            {products}
        </select>
    );
}

// ============================================================
// Exports
export default ProductFilter;
