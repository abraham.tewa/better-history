// ============================================================
// Import packages
import { DateTime } from 'luxon';
import React, {
    useCallback, useMemo, useState,
} from 'react';
import { useNavigate } from 'react-router-dom';
import DataTable from 'react-data-table-component';
import { TFunction, useTranslation } from 'react-i18next';
import styled from 'styled-components';
import type { TableColumn } from 'react-data-table-component';

// ============================================================
// Import modules
import VisitList, { FileVisitEntry, UrlVisitEntry, VisitEntry } from '../../VisitList';
import useCurrentEntry from '../hook/useCurrentEntry';
import useArray from '../hook/useArray';
import Filter from './Filter';
import { Product } from '../../Realm';
import TitleCell from '../TitleCell';
import ProductFilter from './ProductFilter';

const MIN_FILTER_TEXT = 3;

enum Columns {
    childCount = 'child count',
    title = 'link',
    date = 'date',
    directChildrenCount = 'direct child count',
    descendantCount = 'descendant count',
    domains = 'domains',
    id = 'id',
    realm = 'realm',
    realmOrDomain = 'realm / domain',
    reloadCount = 'reload count',
    product = 'product',
    productOrDomain = 'productOrDomain',
    transitionType = 'transition type',
    protocol = 'protocol',
    stackLength = 'stackLength',
}

// ============================================================
// Component
function HistoryList(
    {
        className,
        list,
        defaultColumns = [Columns.date, Columns.title],
        noAbort = true,
        noProductFilter = false,
        noTextFilter = false,
    } : {
        className?: string,
        defaultColumns?: Columns[],
        list: VisitList<VisitEntry>
        noAbort?: boolean,
        noProductFilter?: boolean,
        noTextFilter?: boolean,
    },
) {
    const { t } = useTranslation();
    const { definitions: columnDefinitions } = useColumnsOptions(t, defaultColumns);
    const [, setEntry] = useCurrentEntry();
    const navigate = useNavigate();
    const [currentProduct, setCurrentProduct] = useState<Product | undefined>();

    const onRowClicked = useCallback(
        (row: StackedEntries) => {
            setEntry(row.latestEntry);
            navigate(`/entry/${row.latestEntry.visit.visitId}`);
        },
        [navigate, setEntry],
    );

    const {
        items,
        setFilterText,
        filteredLength,
        totalLength,
    } = useEntries(list, noAbort, currentProduct);

    let filterSummary: JSX.Element | undefined;

    const textFilter = noTextFilter || list.length < 10
        ? undefined
        : (
            <Filter
                placeholder="filter"
                onFilter={setFilterText}
                totalLength={totalLength}
                filteredLength={filteredLength}
            />
        );

    const productFilter = noProductFilter || list.length < 10
        ? undefined
        : (
            <ProductFilter
                list={list}
                onProductSelect={setCurrentProduct}
            />
        );

    return (
        <StyledDiv className={className}>
            <form className="row row-cols-lg-auto g-3 align-items-center">
                <div className="col-12">
                    {textFilter}
                    {filterSummary}
                </div>
                <div className="col-12">
                    {productFilter}
                </div>
            </form>
            <DataTable
                columns={columnDefinitions}
                data={items}
                pagination
                defaultSortFieldId={1}
                defaultSortAsc={false}
                highlightOnHover
                pointerOnHover
                onRowClicked={onRowClicked}
            />

        </StyledDiv>
    );
}

HistoryList.defaultProps = {
    className: '',
    defaultColumns: [Columns.date, Columns.title],
    noAbort: true,
    noProductFilter: false,
    noTextFilter: false,
};

const StyledDiv = styled.div`
    .bh-tag {
        margin-right: 1em;
    }
`;

type StackedEntries = {
    stack: VisitEntry[],
    latestEntry: VisitEntry,
};

// ============================================================
// Hooks

function useEntries(
    list: VisitList<VisitEntry>,
    noAbort: boolean,
    currentProduct: Product | undefined,
) : {
    items: StackedEntries[],
    filtered: boolean,
    filteredLength: number,
    setFilterText: (text: string) => void,
    totalLength: number,
} {
    const [filterText, setFilterText] = useState<string>('');
    const filter = useCallback(
        (item: VisitEntry) => {
            if (noAbort && !item.title) {
                return false;
            }

            if (currentProduct && item.product !== currentProduct) {
                return false;
            }

            return true;
        },
        [noAbort, currentProduct],
    );

    const filtering = filterText.length > MIN_FILTER_TEXT;

    const name = list.name
        ? `HistoryList.${list.name}`
        : 'HistoryList';

    const items = useArray(list, filter, name);

    const stackedItems = useMemo<StackedEntries[]>(
        () => stackEntries(items),
        [items],
    );

    const filteredItems = useMemo<StackedEntries[]>(
        () => {
            if (!filtering) {
                return stackedItems;
            }

            const filteredList = stackedItems.filter(
                ({ latestEntry }) => ((latestEntry.title || '')?.indexOf(filterText) !== -1),
            );

            return filteredList;
        },
        [stackedItems, filtering, filterText],
    );

    return {
        items: filteredItems,
        setFilterText,
        totalLength: items.length,
        filteredLength: filteredItems.length,
        filtered: filtering,
    };
}

function stackEntries(items: VisitEntry[]) : StackedEntries[] {
    const init = {
        current: undefined,
        stackedEntries: ([] as StackedEntries[]),
    } as {
        current: {
            stackedEntry: StackedEntries,
            product?: Product,
            homepage: boolean,
        } | undefined,
        stackedEntries: StackedEntries[],
    };

    return items.reduce(
        (acc, item) => {
            if (acc.current === undefined) {
                const stackedEntry = {
                    stack: [item],
                    latestEntry: item,
                };
                acc.current = {
                    stackedEntry,
                    product: item.product,
                    homepage: item.parseInfo?.isHomepage || false,
                };

                acc.stackedEntries.push(stackedEntry);

                return acc;
            }

            const stack = isSamePage(acc.current.stackedEntry.latestEntry, item, acc.current.homepage);

            if (stack) {
                acc.current.stackedEntry.stack.push(item);
            } else {
                acc.stackedEntries.push(acc.current.stackedEntry);
                acc.current = {
                    stackedEntry: {
                        stack: [item],
                        latestEntry: item,
                    },
                    homepage: item.parseInfo?.isHomepage || false,
                };
            }

            return acc;
        },
        init,
    ).stackedEntries;
}

function isSamePage(previous: VisitEntry, current: VisitEntry, wasHomepage: boolean) {
    if (wasHomepage && previous.product) {
        if (current.product !== previous.product) {
            return false;
        }

        return current.parseInfo?.isHomepage || false;
    }

    if (current.url.hostname === previous.url.hostname) {
        return false;
    }

    return current.displayTitle === previous.displayTitle;
}

type UseColumnsOptionsReturn = {
    definitions: TableColumn<StackedEntries>[],
    columns: Columns[],
    setColumns: (columns: Columns[]) => void,
};

function useColumnsOptions(t: TFunction, initialColumns: Columns[]) : UseColumnsOptionsReturn {
    const [columns, setColumns] = useState<Columns[]>(initialColumns);

    const definitions = useMemo(
        () => buildColumnOptions(t, initialColumns),
        [initialColumns, t],
    );

    return {
        definitions,
        columns,
        setColumns,
    };
}

// ============================================================
// Helpers

type DefColumn =
    ((t: TFunction<'translation', undefined>) => TableColumn<StackedEntries>) |
    TableColumn<StackedEntries>;

const confColumns : { [key in Columns]: DefColumn } = {
    // Column: Child count
    [Columns.childCount]: {
        name: 'historyTable.columns.childCount.title',
        selector: ({ stack }) => stack.reduce(
            (acc, item) => acc + item.children.length,
            0,
        ),
    },

    // Column: Date
    [Columns.date]: {
        name: 'historyTable.columns.date.title',
        selector: ({ latestEntry }) => latestEntry.visit.visitTime || 0,
        maxWidth: '200px',
        format: ({ latestEntry }) => {
            const { visitTime } = latestEntry.visit;
            if (!visitTime) {
                return '';
            }

            const date = DateTime.fromMillis(visitTime);

            return date.toLocaleString(DateTime.DATETIME_SHORT);
        },
    },

    // Column: Descendant count
    [Columns.descendantCount]: {
        name: 'historyTable.columns.descendantCount.title',
        selector: ({ stack }) => stack.reduce(
            (acc, item) => acc + item.descendants.length,
            0,
        ),
    },

    // Column: Direct children count
    [Columns.directChildrenCount]: {
        name: 'historyTable.columns.directChildrenCount.title',
        selector: ({ stack }) => stack.reduce(
            (acc, item) => acc + item.directChildren.length,
            0,
        ),
    },

    // Column: Domain
    [Columns.domains]: (t) => ({
        name: t('historyTable.columns.productOrDomain.title'),
        selector: ({ latestEntry }) => {
            if (!(latestEntry instanceof UrlVisitEntry)) {
                return '';
            }

            return latestEntry.domain || latestEntry.hostname;
        },
        maxWidth: '200px',
    }),

    // Column: ID
    [Columns.id]: {
        name: 'historyTable.columns.id.title',
        selector: ({ latestEntry }) => latestEntry.id,
        maxWidth: '40px',
    },

    // Column: Product
    [Columns.product]: {
        name: 'historyTable.columns.product.title',
        selector: ({ latestEntry }) => (latestEntry instanceof UrlVisitEntry ? latestEntry.product?.name || '' : ''),
    },

    // Column: Product or domain
    [Columns.productOrDomain]: (t) => ({
        name: t('historyTable.columns.productOrDomain.title'),
        selector: ({ latestEntry }) => {
            if (latestEntry instanceof UrlVisitEntry) {
                return latestEntry?.product?.name || (latestEntry.domain as string);
            }

            if (latestEntry instanceof FileVisitEntry) {
                return latestEntry.extension || t('historyTable.columns.productOrDomain.file') as string;
            }

            return t('historyTable.columns.productOrDomain.none') as string;
        },
        maxWidth: '200px',
    }),

    // Column: Protocol
    [Columns.protocol]: (t) => ({
        name: t('historyTable.columns.protocol.title'),
        selector: ({ latestEntry }) => {
            if (latestEntry instanceof UrlVisitEntry) {
                return latestEntry.protocol;
            }

            if (latestEntry instanceof FileVisitEntry) {
                return latestEntry.extension || t('historyTable.columns.protocol.none') as string;
            }

            return t('historyTable.columns.protocol.none') as string;
        },
    }),

    // Column: Realm
    [Columns.realm]: {
        name: 'historyTable.columns.realm.title',
        selector: ({ latestEntry }) => (latestEntry as UrlVisitEntry).realm?.name || '',
    },

    // Column: Realm or domain
    [Columns.realmOrDomain]: {
        name: 'historyTable.columns.realmOrDomain.title',
        selector: ({ latestEntry }) => (
            latestEntry as UrlVisitEntry).realm?.name
            || (latestEntry as UrlVisitEntry).domain
            || '',
    },

    // Column: Reload count
    [Columns.reloadCount]: {
        name: 'historyTable.columns.reload.title',
        selector: ({ stack }) => stack.reduce(
            (acc, item) => acc + item.reloads.length,
            0,
        ),
    },

    [Columns.stackLength]: {
        name: 'historyTable.columns.stackLength.title',
        selector: ({ stack }) => stack.length,
    },

    // Column: Title
    [Columns.title]: (t) => ({
        name: t('historyTable.columns.link.title'),
        selector: ({ latestEntry }) => latestEntry.title || '',
        grow: 1,
        cell: ({ stack, latestEntry }) => (
            <TitleCell
                visit={latestEntry}
                stack={stack}
            />
        ),
    }),

    // Column: Transition type
    [Columns.transitionType]: {
        name: 'historyTable.columns.transition.title',
        selector: ({ latestEntry }) => latestEntry.visit.transition,
        sortable: true,
        reorder: true,
    },
};

function buildColumnOptions(
    t: TFunction<'translation', undefined>,
    listColumns: Columns[],
) : TableColumn<StackedEntries>[] {
    const columns : TableColumn<StackedEntries>[] = listColumns
        .map((name) => {
            const def = confColumns[name];

            if (typeof def === 'function') {
                return def(t);
            }

            return {
                reorder: true,
                sortable: true,
                ...def,
                name: typeof def.name === 'string' ? t(def.name) : name,
            };
        });

    columns.push();

    return columns;
}

// ============================================================
// Exports
export default HistoryList;
export { Columns };
