// ============================================================
// Import packages
import React, { ChangeEvent, useCallback } from 'react';

// ============================================================
// Components
function Filter({
    onFilter,
    placeholder,
    filteredLength,
    totalLength,
}: {
    onFilter: (text: string) => void,
    placeholder: string,
    filteredLength?: number,
    totalLength: number,
}) {
    const onChange = useCallback(
        (event: ChangeEvent<HTMLInputElement>) => onFilter(event.target.value),
        [onFilter],
    );

    let filterSummary: JSX.Element | undefined;

    if (filteredLength !== undefined) {
        <small>
            (
            {filteredLength}
            {' '}
            on
            {' '}
            {totalLength}
            )
        </small>;
    }

    return (
        // eslint-disable-next-line jsx-a11y/label-has-associated-control
        <label
            className="form-label"
        >
            <input
                type="email"
                className="form-control"
                onChange={onChange}
                placeholder={placeholder}
            />
            {filterSummary}
        </label>
    );
}

Filter.defaultProps = {
    filteredLength: undefined,
};

// ============================================================
// Exports
export default Filter;
