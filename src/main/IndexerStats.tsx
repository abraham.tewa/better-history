// ============================================================
// Import packages
import React from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

// ============================================================
// Import modules
import useStats from './hook/useStats';

// ============================================================
// Component
function IndexerStats() {
    const { t } = useTranslation();
    const stats = useStats();

    if (!stats) {
        return <div />;
    }

    const { oldest: latestIndexed } = stats;

    const day = latestIndexed.getDate().toString().padStart(2, '0');
    const month = (latestIndexed.getMonth() + 1).toString().padStart(2, '0');
    const year = latestIndexed.getFullYear();

    const delta = Math.floor(
        (new Date().getTime() - latestIndexed.getTime()) / (1000 * 60 * 60 * 24),
    );

    const position = `${day}/${month}/${year}`;

    return (
        <small>
            <StyledTable>
                <tbody>
                    <tr>
                        <th>{t('homepage.indexStats.entries')}</th>
                        <td>{stats.indexed}</td>
                    </tr>
                    <tr>
                        <th>{t('homepage.indexStats.lastDateIndexed')}</th>
                        <td>{position}</td>
                    </tr>
                    <tr>
                        <th>{t('homepage.indexStats.dayIndexed')}</th>
                        <td>{delta}</td>
                    </tr>
                </tbody>
            </StyledTable>
        </small>
    );
}

const StyledTable = styled.table`
`;

// ============================================================
// Exports
export default IndexerStats;
