// ============================================================
// Import packages
import React, {
    useCallback, useMemo,
} from 'react';

import { useTranslation } from 'react-i18next';

// ============================================================
// Import modules
import * as utils from '../utils';
import { TimeRange, TimeUnit } from '../constants';
import CountEvolutionChart from './CountEvolutionChart';
import HeatMap from './HeatMap';
import TableData from './TableData';
import VisitList, {
    UrlVisitEntry,
    VisitEntry,
    VisitTimeGroupBy,
} from '../VisitList';
import HistoryList, { Columns } from './HistoryList';
import ProductUsageSummary from './ProductUsageSummary';

import { HavingFilter, VirtualGroup } from '../List';
import useVisitList from './hook/useVisitList';
import useGroupingMap from './hook/useGroupingMap';
import {
    Product,
    Realm,
} from '../Realm';
import useList from './hook/useList';
import useGrouping from './hook/useGrouping';

const INITIAL_DATE = new Date();
INITIAL_DATE.setFullYear(INITIAL_DATE.getFullYear() - 1);
INITIAL_DATE.setHours(0);
INITIAL_DATE.setMinutes(0);
INITIAL_DATE.setSeconds(0);
INITIAL_DATE.setMilliseconds(0);

const evolutionGroupBy = {
    range: TimeRange.always,
    timeStep: TimeUnit.day,
};

const heatMapGroupBy = {
    range: TimeRange.week,
    timeStep: TimeUnit.hour,
};

const formatterOptions = {
    locale: 'fr',
};

const appFilter : QueryVisits.Filter = {
    reloads: false,
    visitTime: {
        gte: INITIAL_DATE.getTime(),
    },
};

const historyListDefaultColumns = [
    Columns.date,
    Columns.productOrDomain,
    Columns.title,
];

// ============================================================
// Components
function App() {
    const { t } = useTranslation();

    const list = useVisitList(appFilter);

    const urlEntries = useList(list, filterUrlVisitEntry) as unknown as VisitList<UrlVisitEntry>;

    const evolutionData = useVisitTimeGroupBy(list, evolutionGroupBy.range, evolutionGroupBy.timeStep);
    const heatMapData = useVisitTimeGroupBy(list, heatMapGroupBy.range, heatMapGroupBy.timeStep);
    const realmData = useRealmGroupBy(urlEntries);
    const productData = useProductGroupBy(urlEntries);
    const domainsData = useDomainGroupBy(urlEntries);

    return (
        <div className="App container-fluid">
            <ProductUsageSummary entries={urlEntries} />
            <div className="row">
                <HistoryList
                    className="col-lg-8"
                    list={list}
                    defaultColumns={historyListDefaultColumns}
                />

                <div className="col-lg-4">
                    <div className="row">
                        <CountEvolutionChart
                            className="col-md-6 col-lg-12"
                            id="evolution"
                            grouping={evolutionData}
                            formatterOptions={formatterOptions}
                        />
                        <HeatMap
                            className="col-md-6 col-lg-12"
                            id="heatmap"
                            grouping={heatMapData}
                            formatterOptions={formatterOptions}
                        />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <h2>{t('homepage.realms')}</h2>
                    <TableData
                        data={realmData}
                        columnLabel="Realm"
                        format={getRealmName}
                    />
                </div>
                <div className="col">
                    <h2>{t('homepage.products')}</h2>
                    <TableData
                        data={productData}
                        format={getProductName}
                        columnLabel="Product"
                    />
                </div>
            </div>
            <div className="row">
                <h2>{t('homepage.domains')}</h2>
                <TableData
                    data={domainsData}
                    columnLabel="Domain"
                />
            </div>
        </div>
    );
}

function filterUrlVisitEntry(item: VisitEntry) : boolean {
    return item instanceof UrlVisitEntry;
}

function getRealmName(realm: Realm) {
    return realm.name;
}

function getProductName(product: Product) {
    return product.name;
}

const domainFilter : HavingFilter = {
    count: {
        gte: 0.005,
    },
};

function useDomainGroupBy(
    list: VisitList<UrlVisitEntry>,
) : GroupBy<string> {
    const domains = useGroupingMap(
        list,
        'domain',
        domainFilter,
        false,
        true,
        'homepage.domain',
    ) as GroupBy<string>;

    return domains;
}

const realmFilter : HavingFilter = {
    count: {
        gte: 0.005,
    },
};

function useRealmGroupBy(
    list: VisitList<UrlVisitEntry>,
) : GroupBy<Realm> {
    const grouping = useGroupingMap(
        list,
        'realm',
        realmFilter,
        false,
        true,
        'homepage.realm',
    ) as GroupBy<Realm>;

    return grouping;
}

const productFilter : HavingFilter = {
    count: {
        gte: 0.005,
    },
};

function useProductGroupBy(
    list: VisitList<UrlVisitEntry>,
) : GroupBy<Product> {
    const groups = useGroupingMap(
        list,
        'product',
        productFilter,
        false,
        true,
        'homepage.product',
    ) as GroupBy<Product>;

    return groups;
}

function useVisitTimeGroupBy(
    list: VisitList<VisitEntry>,
    range: TimeRange,
    timeStep: TimeUnit,
) : VisitTimeGroupBy {
    const callback = useCallback(
        (visit: VisitEntry) => {
            const { visitTime } = visit.visit;

            if (!visitTime) {
                return undefined;
            }

            return utils.date.toUnit(visitTime, range, timeStep);
        },
        [range, timeStep],
    );

    const criteria = useMemo(
        () => ({
            criteria: {
                visitTime: {
                    range,
                    timeStep,
                },
            },
        }),
        [range, timeStep],
    );

    const group = useGrouping(
        list,
        callback,
        undefined,
        false,
        true,
        criteria,
        'homepage.groupByVisitTime',
    );

    return group;
}

type GroupBy<T> = Map<T, VirtualGroup<UrlVisitEntry, T>>;

// ============================================================
// Exports
export default App;
