// ============================================================
// Import packages
import React from 'react';
import styled from 'styled-components';

// ============================================================
// Import module
import { VisitEntry } from '../../../VisitList';
import TitleCell from '../../TitleCell';

// ============================================================
// Component: Navigation
function Navigation({
    hideRedirection = true,
    visits,
    maxDepth = 5,
} : {
    hideRedirection?: boolean
    maxDepth?: number,
    visits: VisitEntry[],
}) {
    const list = visits.map(
        (visit) => (
            <li key={visit.id}>
                <Visit
                    visit={visit}
                    maxDepth={maxDepth - 1}
                    hideRedirection={hideRedirection}
                />
            </li>
        ),
    );

    return (
        <StyledUl>
            {list}
        </StyledUl>
    );
}

Navigation.defaultProps = {
    hideRedirection: true,
    maxDepth: 5,
};

const StyledUl = styled.ul`
    list-style: none;
`;

// ============================================================
// Component: Visit
function Visit({
    hideRedirection,
    maxDepth,
    visit,
} : {
    hideRedirection: boolean,
    maxDepth: number,
    visit: VisitEntry
}) {
    const { product } = visit;

    const children = maxDepth > 0 && visit.children.length
        ? <Navigation visits={visit.children} maxDepth={maxDepth} />
        : undefined;

    const entryComponent = hideRedirection && visit.parseInfo?.isRedirection
        ? undefined
        : (
            <StyledDiv>
                <div className="name">{product?.name}</div>
                <TitleCell visit={visit} />
            </StyledDiv>
        );

    return (
        <>
            {entryComponent}
            {children}
        </>
    );
}

const StyledDiv = styled.div`
    display: flex;
    max-width: 500px;

    flex-direction: row;
    flex-grow: 0;
    margin: 6px;
    padding: 3px;

    & > .name {
        padding: 1em;
        overflow-wrap: wrap;
        border-right: solid 1px black;

        &:empty {
            display: none;
        }
    }
`;

// ============================================================
// Exports
export default Navigation;
