// ============================================================
// Import packages
import React, { useMemo, useState } from 'react';

// ============================================================
// Import modules
import VisitList, { UrlVisitEntry, VisitEntry } from '../../VisitList';
import HistoryList from '../HistoryList';
import useVisitEntry from '../hook/useVisitEntry';
import useDb from '../hook/useDb';
import List from '../../List';
import Navigation from './Navigation';

// ============================================================
// Component
function Address({
    entryId,
}: {
    entryId: VisitEntryId,
}) {
    const entry = useVisitEntry(entryId);
    const info = useInfo(entry);

    if (!entry || !info) {
        return <> </>;
    }

    const domain = info.entry instanceof UrlVisitEntry
        ? info.entry.domain
        : '';

    return (
        <div className="container-fluid px-4">
            <h1 className="row">
                {info.entry.title}
            </h1>
            <div>
                <a
                    href={info.entry.url.href}
                    target="_blank"
                    rel="noreferrer"
                >
                    {info.entry.url.href}
                </a>
            </div>
            <div>
                (
                <i>{domain}</i>
                )
            </div>
            <div>
                Id.:
                {' '}
                {info.entry.id}
            </div>
            <div>
                Entry title:
                {' '}
                {info.entry.title}
            </div>
            <div>
                Transition:
                {' '}
                {info.entry.visit.transition}
            </div>
            <div>
                Children:
                {info.entry.children.length}
            </div>
            <div>
                Reloads:
                {info.entry.reloads.length}
            </div>
            <div className="row">
                <div className="col-8">
                    <h2>Navigation path</h2>
                    <Navigation visits={[entry]} />
                </div>
                <div className="col-4">
                    <h2>Other visits</h2>
                    <HistoryList
                        list={info.otherVisits}
                        noAbort
                        noProductFilter
                        noTextFilter
                    />
                </div>
                <div className="col-4">
                    <h2>Other visits of the domain</h2>
                    <HistoryList
                        list={info.domainVisits}
                        noAbort
                        noProductFilter
                    />
                </div>
            </div>
            <div>{info.entry.navigation.complete ? 'Complete' : 'On going...'}</div>
        </div>
    );
}

function useInfo(entry: VisitEntry | undefined) {
    const [info, setInfo] = useState<EntryInfo>();

    const db = useDb();

    useMemo(
        () => {
            if (!entry) {
                return;
            }

            const listSameVisits = db.query({
                url: {
                    href: entry.url.href,
                },
            });

            let listProductVisits : VisitList<VisitEntry> = new VisitList(new List([] as VisitEntry[]));

            if (entry instanceof UrlVisitEntry) {
                const filter = entry.product
                    ? { domain: entry.product.domains }
                    : { domain: (entry.domain || entry.hostname) };

                listProductVisits = db.query({
                    url: filter,
                });
            }

            setInfo({
                entry,
                otherVisits: listSameVisits,
                domainVisits: listProductVisits,
            });
        },
        [entry, db],
    );

    return info;
}

type EntryInfo = {
    entry: VisitEntry,
    otherVisits: VisitList<VisitEntry>,
    domainVisits: VisitList<VisitEntry>,
};

// ============================================================
// Exports
export default Address;
