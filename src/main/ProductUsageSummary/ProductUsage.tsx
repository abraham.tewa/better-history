// ============================================================
// Import packages
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

// ============================================================
// Import modules
import * as utils from '../../utils';

import {
    groupBy,
    VirtualGroup,
} from '../../List';

import {
    Product,
    UrlParser,

    ProductTraits,
} from '../../Realm';
import { UrlVisitEntry } from '../../VisitList';
import useArray from '../hook/useArray';

// ============================================================
// Functions

enum SummaryMode {
    latest,
    usage,
}

function ProductUsage(
    {
        className,
        group,
        count = 5,
    }:
    {
        className?: string,
        count?: number,
        group: VirtualGroup<UrlVisitEntry, Product, true>
    },
) {
    const { t } = useTranslation();
    const [mode, setMode] = useState<SummaryMode>(SummaryMode.latest);
    const product = group.value;

    const products = useProductList(group, mode, count);

    const toggleMode = useCallback(
        () => setMode((value) => (value === SummaryMode.latest
            ? SummaryMode.usage
            : SummaryMode.latest)),
        [],
    );

    const li = products
        .filter((item) => item)
        .map(({ label, id, url }) => {
            const node = mode === SummaryMode.usage
                ? label
                : (<a href={url?.href} target="_blank" rel="noreferrer">{label}</a>);
            return (
                <li className="item" key={id}>
                    {node}
                </li>
            );
        });

    const buttonLabel = mode === SummaryMode.latest
        ? t('homepage.productUsage.latest')
        : t('homepage.productUsage.count');

    return (
        <StyledDiv className={className}>
            <h2>{product.name}</h2>
            <button
                onClick={toggleMode}
                type="button"
                className="btn btn-link"
            >
                {buttonLabel}
            </button>
            <ul>{li}</ul>
        </StyledDiv>
    );
}

ProductUsage.defaultProps = {
    className: '',
    count: 5,
};

const StyledDiv = styled.div`
    & li.item {
        overflow: hidden;

        white-space: nowrap;
        text-overflow: ellipsis;
    }
`;

// ==================================================
// Hooks
function useProductList(
    group: VirtualGroup<UrlVisitEntry, Product, true>,
    mode: SummaryMode,
    count: number,
) : Array<{ label: string, id: string, url?: URL }> {
    const items = useArray(group.list);

    const products = useMemo(
        () => {
            const visits = mode === SummaryMode.latest
                ? listLatestItems(group.value, items, count)
                : listTopItems(group.value, items, count);

            return visits;
        },
        [items, group.value, mode, count],
    );

    return products;
}

// ==================================================
// Helpers

function sortByVisitTime(a: UrlVisitEntry, b: UrlVisitEntry) {
    if (a.visit.visitTime === undefined) {
        return -1;
    }

    if (b.visit.visitTime === undefined) {
        return 1;
    }

    return b.visit.visitTime - a.visit.visitTime;
}

function listLatestItems(
    product: Product,
    listEntry: UrlVisitEntry[],
    count: number,
) : Array<{ label: string, id: string, url: URL }> {
    const { traits } = product;

    const mainTraits = traits[0];

    listEntry.sort(sortByVisitTime);

    const visits = utils.array.ensureCount(
        count,
        listEntry,
        (entry) => {
            const info = product.parse(entry);

            if (!info) {
                return undefined;
            }

            const label = getProductItemLabel(info, mainTraits);

            if (!label) {
                return undefined;
            }

            return {
                label,
                id: entry.visit.visitId,
                url: entry.url,
            };
        },
    );

    return visits;
}

function listTopItems(product: Product, list: UrlVisitEntry[], count: number) {
    const { traits } = product;

    const mainTrait = traits[0];

    const property = getProductItemLabelProperty(mainTrait);

    if (!property) {
        return [];
    }

    const listParse = list
        .map((entry) => product.parse(entry))
        .filter((parse) => parse) as UrlParser[];

    const groups = groupBy.property(
        listParse,
        property as keyof UrlParser,
    );

    const listGroups = Array.from(groups.map.values());

    listGroups.sort((a, b) => {
        if (!a.value) {
            return 1;
        }

        if (!b.value) {
            return -1;
        }

        const diff = b.count - a.count;

        if (diff !== 0) {
            return diff;
        }

        return (a.value as string)?.localeCompare(b.value as string);
    });

    return listGroups
        .slice(0, count)
        .map((group) => {
            const label = group.value as string;

            return {
                label: `${label} (${group.count})`,
                id: label,
            };
        });
}

function getProductItemLabelProperty(
    trait: ProductTraits,
) : string | null {
    switch (trait) {
    case ProductTraits.encyclopedia: {
        return 'encyclopediaArticleTitle';
    }
    case ProductTraits.searchEngine: {
        return 'searchTerms';
    }
    case ProductTraits.videoLibrary: {
        return 'videoTitle';
    }
    default:
        return null;
    }
}

function getProductItemLabel(
    info: UrlParser,
    trait: ProductTraits,
) : string | undefined {
    switch (trait) {
    case ProductTraits.encyclopedia: {
        return info.traits.encyclopedia?.articleTitle;
    }
    case ProductTraits.searchEngine: {
        return info.traits.searchEngine?.searchTerms;
    }
    case ProductTraits.videoLibrary: {
        return info.traits.videoLibrary?.videoTitle;
    }
    default:
        return undefined;
    }
}

// ============================================================
// Exports
export default ProductUsage;
