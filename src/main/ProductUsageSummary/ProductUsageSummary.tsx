// ============================================================
// Import packages
import React, { useMemo } from 'react';

// ============================================================
// Import modules
import { Product } from '../../Realm';
import VisitList, { UrlVisitEntry } from '../../VisitList';
import useGroupingMap from '../hook/useGroupingMap';
import ProductUsage from './ProductUsage';

// const filter = {};

// ============================================================
// Functions
function ProductUsageSummary(
    {
        entries,
    }:
    {
        entries: VisitList<UrlVisitEntry>,
    },
) {
    const groups = useGroupingMap<Product>(
        entries,
        'product',
        undefined,
        true,
        true,
        'ProductUsageSummary',
    );

    const topProducts = useMemo(
        () => {
            const list = [...Array.from(groups.values())];

            list.sort((a, b) => b.count - a.count);

            const l = list
            // Keeping only groups with traits
                .filter((group) => group.value.traits.length)

            // Keeping only the first 5 groups
                .slice(0, 5);

            return l

                // Creating list of page for each groups
                .map((group) => (
                    <ProductUsage
                        key={(group.value as Product).id}
                        className="col-4"
                        group={group}
                    />
                ));
        },
        [groups],
    );

    return (
        <div className="row">
            {topProducts}
        </div>
    );
}

// ============================================================
// Exports
export default ProductUsageSummary;
