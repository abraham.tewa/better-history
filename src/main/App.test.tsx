import React from 'react';
import { render, screen } from '@testing-library/react';

test('renders learn react link', () => {
    render(<div>Better History</div>);
    const linkElement = screen.getByText(/Better History/i);
    expect(linkElement).toBeInTheDocument();
});
