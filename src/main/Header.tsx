// ============================================================
// Import packages
import { Settings } from 'luxon';
import { NavLink } from 'react-router-dom';
import React, { MouseEvent, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';

// ============================================================
// Import modules
import { Locales } from './i18n';
import IndexerStats from './IndexerStats';

// ============================================================
// Components
function Header() {
    const { i18n } = useTranslation();

    const changeLanguage = useCallback(
        (event: MouseEvent<HTMLElement>) => {
            const { locale } = (event.target as HTMLElement).dataset as { locale: string };
            i18n.changeLanguage(locale);
            Settings.defaultLocale = locale;
        },
        [i18n],
    );

    const locales = Object
        .values(Locales)
        .map((locale) => (
            <li key={locale}>
                <button
                    type="button"
                    className="btn btn-link"
                    disabled={locale === i18n.language}
                    data-locale={locale}
                    onClick={changeLanguage}
                >
                    {locale}
                </button>
            </li>
        ));

    return (
        <StyledHeader className="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
            <span className="display-4">
                <NavLink to="" className="header-link" end>
                    Better History
                </NavLink>
            </span>
            <StyledDiv>
                <ListLocals>{locales}</ListLocals>
                <IndexerStats />
            </StyledDiv>
        </StyledHeader>
    );
}

// ============================================================
// Styles
const StyledHeader = styled.header`
    .header-link {
        text-decoration: none;
        color: inherit;
    }
`;
const StyledDiv = styled.div`
    display: flex;
    flex-direction: column;

    position: absolute;
    top: 0;
    right: 0;

    margin-bottom: 3px;
`;

const ListLocals = styled.ul`
    list-style: none;

    display: flex;
    justify-content: flex-end;

    padding: 0;
    margin: 0;

    button {
        padding-top: 0;
        padding-bottom: 0;
    }
`;

// ============================================================
// Exports
export default Header;
