// ============================================================
// Import packages
import { useEffect, useState } from 'react';
import { AbstractList, ListEventType } from '../../List';
import useList from './useList';

// ============================================================
// Functions

function useArray<T>(
    source: AbstractList<T>,
    filter?: ((item: T) => boolean),
    listName?: string,
) : T[] {
    const [array, setArray] = useState<T[]>([]);
    const dataList = useList(source, filter, listName);

    if (dataList === undefined) {
        throw new Error('dataList undefined');
    }

    useEffect(
        () => {
            let timeoutId: NodeJS.Timeout | undefined;

            const onChange = () => {
                // Doing nothing if an update is already planned
                if (timeoutId !== undefined) {
                    return;
                }

                timeoutId = setTimeout(
                    () => {
                        // Updating asynchronously, so series of list updates will not
                        // trigger each time a DOM update.
                        setArray(dataList.list.slice(0));
                        timeoutId = undefined;
                    },
                    0,
                );
            };

            onChange();

            dataList.addEventListener(ListEventType.itemAdded, onChange);
            dataList.addEventListener(ListEventType.itemRemoved, onChange);

            return () => {
                if (timeoutId !== undefined) {
                    clearTimeout(timeoutId);
                }

                dataList.removeEventListener(ListEventType.itemAdded, onChange);
                dataList.removeEventListener(ListEventType.itemRemoved, onChange);
            };
        },
        [dataList],
    );

    return array;
}

// ============================================================
// Exports
export default useArray;
