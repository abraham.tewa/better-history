// ============================================================
// Import packages
import React, { useContext, useEffect, useState } from 'react';

// ============================================================
// Import modules
import { background } from '../dialog';

// ============================================================
// Module's constants and variables
const IndexerContext = React.createContext<IndexStats | undefined>(undefined);

// ============================================================
// Functions
function Context(
    {
        children,
    }:
    {
        children: JSX.Element,
    },
) {
    const [stats, setStats] = useState<IndexStats>();

    useEffect(
        () => {
            const cb = (event: any) => {
                setStats(event.detail.current);
            };

            background.addEventListener(
                'indexerUpdate',
                cb,
            );

            return () => {
                background.removeEventListener(
                    'indexerUpdate',
                    cb,
                );
            };
        },
        [],
    );

    return (
        <IndexerContext.Provider value={stats}>
            {children}
        </IndexerContext.Provider>
    );
}

function useStats() : IndexStats | undefined {
    return useContext(IndexerContext);
}

// ============================================================
// Exports
export default useStats;
export {
    Context,
};
