// ============================================================
// Import packages
import { useEffect, useMemo, useState } from 'react';

// ============================================================
// Import modules
import List, {
    GroupEventType,
    VirtualGroup,
} from '../../List';

// ============================================================
// Hook
function useGroup<T, K, AddList extends boolean>(
    group: VirtualGroup<T, K, AddList>,
) : UseGroupList<T, K, AddList> {
    const [value] = useState<K>(group.value);
    const [count, setCount] = useState(group.count);
    const [list] = useState(group.list);

    useEffect(
        () => {
            const onGroupUpdate = () => {
                setCount(group.count);
            };

            group.addEventListener(GroupEventType.itemAdded, onGroupUpdate);
            group.addEventListener(GroupEventType.itemRemoved, onGroupUpdate);

            return () => {
                group.removeEventListener(GroupEventType.itemAdded, onGroupUpdate);
                group.removeEventListener(GroupEventType.itemRemoved, onGroupUpdate);
            };
        },
        [group],
    );

    const ret = useMemo(
        () => ({ count, value, list }),
        [count, value, list],
    ) as UseGroupList<T, K, AddList>;

    return ret;
}

type UseGroupList<T, K, AddList extends boolean> = AddList extends true
    ? { value: K, count: number, list: List<T> }
    : { value: K, count: number };

// ============================================================
// Exports
export default useGroup;
