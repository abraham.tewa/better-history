// ============================================================
// Import packages
import { useEffect, useState } from 'react';
import { VirtualList } from '../../List';
import VisitList from '../../VisitList';

// ============================================================
// Functions
function useList(
    filter: QueryVisits.Filter,
    source: VisitList = VisitList.global,
    name?: string,
) : VisitList {
    const [list, setList] = useState<VisitList>(VisitList.global);

    useEffect(
        () => {
            const newList = filter ? source.query(filter, name) : source;

            setList(newList);

            return () => {
                if (filter) {
                    (newList as VirtualList).unlink();
                }
            };
        },
        [source, filter, name],
    );

    return list;
}

// ============================================================
// Exports
export default useList;
