import { useEffect, useRef, useState } from 'react';

function useMemoized<T>(
    callback: () => T,
    cleanup: (oldValue: T) => void,
    dependencies: any[],
) : T {
    const [value, setValue] = useState<T>(
        () => callback(),
    );

    const firstRun = useRef(true);

    useEffect(
        () => {
            if (firstRun.current) {
                firstRun.current = false;
                return;
            }

            setValue(callback);
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        dependencies,
    );

    useEffect(
        () => () => {
            cleanup(value);
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [value],
    );

    return value;
}

export default useMemoized;
