import { AbstractList, groupBy, VirtualGrouping } from '../../List';
import useMemoized from './useMemoized';

function useGrouping<
    T,
    K,
    AddList extends boolean,
    RemoveUndef extends boolean,
    Meta,
>(
    list: AbstractList<T>,
    callback: (item: T) => K | undefined,
    having?: groupBy.Having<T, K, AddList>,
    includeList?: AddList,
    removeUndefined?: RemoveUndef,
    meta?: Meta,
    name?: string,
) : VirtualGrouping<T, K, AddList, RemoveUndef, Meta> {
    const group = useMemoized<VirtualGrouping<T, K, AddList, RemoveUndef, Meta>>(
        () => list.groupByCallback(
            callback,
            having,
            includeList,
            removeUndefined,
            meta,
            name,
        ),
        (oldGroup: VirtualGrouping) => oldGroup.unlink(),
        [list, callback, having, includeList, removeUndefined, meta],
    );

    return group;
}

export default useGrouping;
