// ============================================================
// Import packages
import { useEffect, useState } from 'react';
import {
    AbstractList,
    Having,
    GroupingEventType,
    VirtualGroup,
    VirtualGrouping,
} from '../../List';

// ============================================================
// Functions
function useGroupingMap<K, T, AddList extends boolean>(
    source: VirtualGrouping<T, K, AddList, boolean, any>,
) : Map<K, VirtualGroup<T, K, AddList>>;
function useGroupingMap<K, T = any, AddList extends boolean = boolean, RemoveUndef extends boolean = boolean>(
    source: AbstractList<T>,
    criteria: keyof T | ((item: T) => RemoveUndefIf<K, RemoveUndef>),
    having?: Having<T, K, AddList>,
    addList?: AddList,
    removeUndef?: RemoveUndef,
    name?: string,
) : Map<K, VirtualGroup<T, K, AddList>>;
function useGroupingMap<
    K,
    T,
    AddList extends boolean,
    RemoveUndef extends boolean,
>(
    source: AbstractList<T> | VirtualGrouping<T, any, boolean, boolean, any>,
    criteria?: keyof T | ((item: T) => RemoveUndefIf<K, RemoveUndef>),
    having?: Having<T, K, AddList>,
    addList?: AddList,
    removeUndef?: RemoveUndef,
    name?: string,
) : Map<K, VirtualGroup<T, K, AddList>> {
    const [groups, setGroups] = useState<Map<K, VirtualGroup<T, K, AddList>>>(new Map());

    useEffect(
        () => {
            const groupByCallback = typeof criteria === 'string'
                ? (item: T) => ((item as any)[criteria] as RemoveUndefIf<K, RemoveUndef>)
                : criteria;

            const createGroup = source instanceof AbstractList;

            let grouping: VirtualGrouping;

            if (createGroup) {
                grouping = source.groupByCallback<K, AddList, RemoveUndef, undefined>(
                        groupByCallback as ((item: T) => RemoveUndefIf<K, RemoveUndef>),
                        having,
                        addList,
                        removeUndef,
                        undefined,
                        name,
                );
            } else {
                grouping = source;
            }

            let timeoutId: NodeJS.Timeout | undefined;

            const onChange = () => {
                if (timeoutId !== undefined) {
                    return;
                }

                timeoutId = setTimeout(
                    () => {
                        setGroups(new Map(grouping.map));
                        timeoutId = undefined;
                    },
                    0,
                );
            };

            onChange();

            grouping.addEventListener(GroupingEventType.itemAdded, onChange);
            grouping.addEventListener(GroupingEventType.itemRemoved, onChange);

            return () => {
                if (timeoutId !== undefined) {
                    clearTimeout(timeoutId);
                }

                // If the group have been created, we unlink it
                if (createGroup) {
                    grouping.unlink();
                }

                grouping.removeEventListener(GroupingEventType.itemAdded, onChange);
                grouping.removeEventListener(GroupingEventType.itemRemoved, onChange);
            };
        },
        [criteria, addList, having, removeUndef, source, name],
    );

    return groups;
}

// ============================================================
// Exports
export default useGroupingMap;
