// ============================================================
// Import packages
import { useMemo } from 'react';
import { VisitEntry } from '../../VisitList';
import useDb from './useDb';

// ============================================================
// Functions

function useVisitEntry(visitEntryId: VisitEntryId) : VisitEntry | undefined {
    const db = useDb();

    const entry = useMemo(
        () => {
            if (!db) {
                return undefined;
            }

            return db.getFromId(visitEntryId);
        },
        [visitEntryId, db],
    );

    return entry;
}

// ============================================================
// Exports
export default useVisitEntry;
