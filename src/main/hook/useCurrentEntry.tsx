// ============================================================
// Import packages
import React, {
    useContext, useMemo, useState,
} from 'react';
import { VisitEntry } from '../../VisitList';

// ============================================================
// Module's constants and variables
const CurrentEntryContext = React.createContext<any>(undefined);

// ============================================================
// Functions
function Context(
    {
        children,
    }:
    {
        children: JSX.Element,
    },
) {
    const [entry, setEntry] = useState<VisitEntry>();

    const value = useMemo(
        () => [entry, setEntry],
        [entry],
    );

    return (
        <CurrentEntryContext.Provider value={value}>
            {children}
        </CurrentEntryContext.Provider>
    );
}

function useCurrentEntry() : [VisitEntry | undefined, React.Dispatch<React.SetStateAction<VisitEntry | undefined>>] {
    const memoisedEntry = useContext(CurrentEntryContext);

    return memoisedEntry;
}

// ============================================================
// Exports
export default useCurrentEntry;
export {
    Context,
};
