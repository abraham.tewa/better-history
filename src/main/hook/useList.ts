// ============================================================
// Import packages
import { useEffect, useState } from 'react';
import List, { AbstractList, VirtualList } from '../../List';

// ============================================================
// Functions
function useList<T>(
    source: AbstractList<T>,
    filter?: ((item: T) => boolean) | undefined,
    name?: string,
) : AbstractList<T> {
    const [list, setList] = useState<AbstractList<T>>(
        () => new List([] as T[], name),
    );

    useEffect(
        () => {
            const newList = filter ? source.filter(filter, name) : source;

            setList(newList);

            return () => {
                if (filter) {
                    (newList as VirtualList).unlink();
                }
            };
        },
        [source, filter, name],
    );

    return list;
}

// ============================================================
// Exports
export default useList;
