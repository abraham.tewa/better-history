// ============================================================
// Import packages
import React, {
    useContext, useState,
} from 'react';
import VisitList, { VisitEntry } from '../../VisitList';

// ============================================================
// Module's constants and variables
const DbContext = React.createContext<VisitList<VisitEntry>>(VisitList.global);

// ============================================================
// Functions
function Context(
    {
        children,
    }:
    {
        children: JSX.Element,
    },
) {
    const [list] = useState<VisitList<VisitEntry>>(VisitList.global);

    return (
        <DbContext.Provider value={list}>
            {children}
        </DbContext.Provider>
    );
}

function useDb() : VisitList<VisitEntry> {
    const list = useContext(DbContext);

    return list;
}

// ============================================================
// Exports
export default useDb;
export {
    Context,
};
