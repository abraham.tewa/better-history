// ============================================================
// Import packages
import React, {
    useMemo,
} from 'react';
import classnames from 'classnames';

import { VirtualGroup } from '../List';

// ============================================================
// Component
function TableData<K>({
    className: classNameProps = 'col',
    data,
    columnLabel,
    format,
} : {
    className?: string,
    data: Map<K, VirtualGroup>,
    columnLabel: string,
    format?: (groupKey: K) => string | undefined,
}) : JSX.Element {
    const tableData = useMemo(
        () => Array
            .from(data.entries()).map(([key, group]) => ({
                value: format ? format(key) : (group.value as unknown as string),
                count: group.count,
            }))
            .filter((value) => value !== undefined)
            .map(({
                value, count,
            }) => (
                <tr key={value}>
                    <td>{value}</td>
                    <td>{count}</td>
                </tr>
            )),
        [data, format],
    );

    const className = classnames(
        classNameProps,
        'table',
    );

    return (
        <table className={className}>
            <thead>
                <tr>
                    <td style={{ textTransform: 'capitalize' }}>{columnLabel}</td>
                    <td>Count</td>
                </tr>
            </thead>
            <tbody>
                {tableData}
            </tbody>
        </table>
    );
}

TableData.defaultProps = {
    className: undefined,
    format: undefined,
};

// ============================================================
// Exports
export default TableData;
