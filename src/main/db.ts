import {
    getList,
    getTable,
    initialize as initializeDb,
} from '../db';
import VisitList from '../VisitList';
import { background, BackgroundEventType } from './dialog';

function initialize() {
    let timeoutId: NodeJS.Timeout | undefined;

    background.addEventListener(
        BackgroundEventType.IndexerUpdate,
        async ({ detail: { previous, current } }) => {
            if (timeoutId) {
                return;
            }

            // Ensuring that two updates are not called simultaneously.
            timeoutId = setTimeout(
                async () => {
                    const indexed = current.indexed - previous.indexed;

                    if (previous.indexed === undefined || indexed) {
                        await updateDb();
                    }

                    timeoutId = undefined;
                },
                0,
            );
        },
    );

    initializeDb();

    setTimeout(
        updateDb,
        0,
    );
}

async function updateDb() {
    const list = getList();
    const visitList = VisitList.global;

    const table = await getTable();

    let dbVisitItems: DbVisitItem[] = [];

    {
        const visitTime = visitList.oldestVisit?.visit.visitTime;

        const query = visitTime
            ? table.where('visit.visitTime').below(visitTime)
            : table;

        dbVisitItems = await query.toArray();
    }

    {
        const visitTime = visitList.newestVisit?.visit.visitTime;

        if (visitTime) {
            const query = table.where('visit.visitTime').above(visitTime);
            const found = await query.toArray();
            dbVisitItems = dbVisitItems.concat(found);
        }
    }

    const items = VisitList.fromDbVisitItem(dbVisitItems);

    list.add(items);
}

export {
    initialize,
};
