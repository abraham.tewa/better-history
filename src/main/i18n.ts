import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { Settings } from 'luxon';

import translationEnUS from '../locales/en.json';
import translationFrFR from '../locales/fr.json';
import translationDev from '../locales/dev.json';

enum Locales {
    dev = 'dev',
    enUS = 'en',
    frFR = 'fr',
}

const resources = {
    [Locales.dev]: {
        translation: translationDev,
    },

    [Locales.enUS]: {
        translation: translationEnUS,
    },

    [Locales.frFR]: {
        translation: translationFrFR,
    },
};

const DEFAULT_LOCALE = Locales.enUS;

Settings.defaultLocale = DEFAULT_LOCALE;

i18n
    .use(initReactI18next)
    .init({
        lng: DEFAULT_LOCALE,
        debug: true,

        supportedLngs: Object.values(Locales),

        interpolation: {
            escapeValue: false, // react already safes from xss
        },

        resources,
    });

export default i18n;

export {
    Locales,
};
