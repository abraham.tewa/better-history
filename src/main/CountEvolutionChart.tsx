// ============================================================
// Import packages
import React, {
    useMemo,
} from 'react';
import Chart from 'react-apexcharts';
import classnames from 'classnames';

import type { ApexOptions } from 'apexcharts';
import * as utils from '../utils';

// ============================================================
// Import modules
import {
    TimeRange, TimeUnit,
} from '../constants';
import { VisitEntry, VisitTimeGroupBy } from '../VisitList';
import useGroupingMap from './hook/useGroupingMap';
import { VirtualGroup } from '../List';

const MAX_TIME = new Date(8_640_000_000_000_000).getTime();
const MIN_TIME = new Date(-8_640_000_000_000_000).getTime();

const secondInMs = 1000;
const minuteInMs = 60 * secondInMs;
const hourInMs = 60 * minuteInMs;
const dayInMs = 24 * hourInMs;

// ============================================================
// Components
function CountEvolutionChart({
    id,
    className: classNameProps,
    grouping,
    formatterOptions,
} : {
    id: string,
    className?: string,
    grouping: VisitTimeGroupBy,
    formatterOptions: FormatterOptions,
}) {
    const map = useGroupingMap(grouping);
    const options : ApexOptions = useMemo(
        () => {
            const { range, timeStep } = grouping.meta.criteria.visitTime;

            return buildOptions(id, map, range, timeStep, formatterOptions);
        },
        [
            grouping,
            map,
            formatterOptions,
            id,
        ],
    );

    if (!grouping) {
        return (
            <div />
        );
    }

    const className = classnames(
        classNameProps,
    );

    return (
        <Chart
            className={className}
            options={options}
            series={options.series}
            type="bar"
            height={320}
        />
    );
}

function buildOptions(
    id: string,
    map: Map<number, VirtualGroup<VisitEntry, number, false>>,
    range: TimeRange,
    timeStep: TimeUnit,
    formatterOptions: FormatterOptions,
) : ApexOptions {
    const list = Array.from(map.entries());

    const seriesData : [number, number][] = list
        .map(([k, v]) => [
            Number(k),
            v.count,
        ]);

    const bounds = list
        .reduce(
            ({ min, max }, [, group]) => {
                const { value } = group;
                if (value === undefined) {
                    return { min, max };
                }

                return {
                    min: Math.min(min, value),
                    max: Math.max(max, value),
                };
            },
            {
                min: MAX_TIME,
                max: MIN_TIME,
            },
        );

    const delta = bounds.max - bounds.min;
    let min: number;
    let max: number;

    if (delta <= 5 * secondInMs) {
        min = bounds.min;
        max = min + 5 * secondInMs;
    } else if (delta <= 5 * minuteInMs) {
        min = bounds.min;
        max = min + 5 * minuteInMs;
    } else if (delta <= 5 * hourInMs) {
        min = bounds.min;
        max = min + 5 * hourInMs;
    } else if (delta <= 5 * dayInMs) {
        min = bounds.min;
        max = min + 5 * dayInMs;
    } else {
        const minDate = new Date(bounds.min);
        const maxDate = new Date(bounds.max);

        min = new Date(minDate.getFullYear(), minDate.getMonth()).getTime();
        max = new Date(maxDate.getFullYear(), maxDate.getMonth() + 1).getTime();
    }

    const options : ApexOptions = {
        chart: {
            id,
        },
        dataLabels: {
            enabled: false,
        },
        xaxis: {
            min,
            max,
            labels: {
                formatter: (value) => utils.date.formatDate(
                    range,
                    timeStep,
                    formatterOptions,
                    Number(value),
                ),
                rotate: -45,
            },
        },
        series: [
            {
                name: 'Series 1',
                data: seriesData,
            },
        ],
    };

    return options;
}

CountEvolutionChart.defaultProps = {
    className: undefined,
};

// ============================================================
// Types
type FormatterOptions = {
    locale: string,
};

// ============================================================
// Exports
export default CountEvolutionChart;
