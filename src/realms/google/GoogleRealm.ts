// ============================================================
// Import modules
import {
    Product,
    Realm,
} from '../../Realm';

import products, {
    gmailProduct,
    googleChatProduct,
    googleImageProduct,
    googleMapsProduct,
    googleSearchProduct,
} from './products';

import { domains } from './domains';

// ============================================================
// Class

class GoogleRealm extends Realm {
    static readonly #realm: GoogleRealm = new GoogleRealm();

    constructor() {
        super('google', 'Google', domains, products);
    }

    getAllProducts(domain: string, url: URL): Product[] {
        const list = super.getAllProducts(domain, url);

        if (list.length === 1) {
            return list;
        }

        // Google Chat
        if (list.includes(googleChatProduct) && isGoogleChatUrl(url)) {
            return [googleChatProduct];
        }

        if (list.includes(gmailProduct)) {
            return [gmailProduct];
        }

        if (!list.includes(googleSearchProduct)) {
            return list;
        }

        if (url.pathname.startsWith('/gmail')) {
            return [gmailProduct];
        }

        if (url.pathname.startsWith('/maps')) {
            return [googleMapsProduct];
        }

        const tbm = url.searchParams.get('tbm');

        return tbm === 'isch'
            ? [googleImageProduct]
            : [googleSearchProduct];
    }
}

function isGoogleChatUrl(url: URL): boolean {
    if (url.hostname !== 'mail.google.com') {
        return false;
    }

    return url.pathname.startsWith('/chat/');
}

// ============================================================
// Exports
export default GoogleRealm;
