/* eslint-disable max-classes-per-file */
import {
    SearchEngine,
    SearchEnginePageType,
    SearchEngineResultPageInfo,
    VideoLibraryPageType,
    UrlParser,
    VideoLibrary,
} from '../../../Realm';

const reTitle = /^(^.*)\s-\sYouTube$/;

class YoutubeUrlParser extends UrlParser {
    // ============================================================
    // Trait: Video Library

    protected getVideoLibraryTrait(): VideoLibrary {
        return new VideoLibraryTraitBuilder(this);
    }

    protected getSearchEngineTrait(): SearchEngine {
        return new SearchEngineTraitBuilder(this);
    }
}

// ============================================================
// Trait: Search Engine
class SearchEngineTraitBuilder extends SearchEngine {
    #searchTerms: string | null | undefined = null;

    protected calcPageType() : SearchEnginePageType | undefined {
        if (this.searchTerms) {
            return SearchEnginePageType.resultList;
        }

        return undefined;
    }

    protected calcSearchTerms(): string | undefined {
        if (this.#searchTerms === null) {
            this.#searchTerms = this.urlParser.url.searchParams.get('search_query') || undefined;
        }

        return this.#searchTerms;
    }

    // eslint-disable-next-line class-methods-use-this
    get searchResultsInfo(): SearchEngineResultPageInfo {
        return {
            pagination: false,
        };
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcOpenedResults(): IVisitEntry[] {
        return this.urlParser.entry.children;
    }
}

// ============================================================
// Trait: VideoLibrary
class VideoLibraryTraitBuilder extends VideoLibrary {
    protected calcPageType() : VideoLibraryPageType | undefined {
        if (this.urlParser.url.searchParams.has('v')) {
            return VideoLibraryPageType.video;
        }

        if (this.urlParser.url.pathname.startsWith('/shorts/')) {
            return VideoLibraryPageType.video;
        }

        if (this.urlParser.url.pathname.startsWith('/c/') || this.urlParser.url.pathname.startsWith('/channel/')) {
            return VideoLibraryPageType.channel;
        }

        return undefined;
    }

    protected calcChannelTitle(): string | undefined {
        if (this.urlParser.title === undefined) {
            return undefined;
        }

        const res = reTitle.exec(this.urlParser.title) as string[];

        if (!res) {
            return undefined;
        }

        return res[1];
    }

    protected calcVideoTitle(): string | undefined {
        if (this.urlParser.title === undefined) {
            return undefined;
        }

        if (this.pageType !== VideoLibraryPageType.video) {
            return undefined;
        }

        const res = reTitle.exec(this.urlParser.title);

        if (!res) {
            return undefined;
        }

        return res[1];
    }

    protected calcVideoId(): string | undefined {
        return this.urlParser.url.searchParams.get('v') || undefined;
    }
}

export default YoutubeUrlParser;
