import { Product, UrlParserConstructor, ProductTraits } from '../../../Realm';
import { supportedDomains } from '../domains';
import GoogleSearchParser from './GoogleSearchParser';
import YoutubeUrlParser from './YoutubeUrlParser';
import GmailUrlParser from './GmailUrlParser';

enum GoogleProduct {
    Chat = 'Google Chat',
    ChromeWebStore = 'Chrome Web Store',
    Gmail = 'Gmail',
    GoogleAccount = 'Google Account',
    GoogleAppsScript = 'Google Apps Script',
    GoogleBooks = 'Google Books',
    GoogleCalendar = 'Google Calendar',
    GoogleCloudPlatform = 'Google Cloud Platform',
    GoogleContacts = 'Google Contacts',
    GoogleDevelopers = 'Google Developers',
    GoogleDomains = 'Google Domains',
    GoogleDrive = 'Google Drive',
    GoogleDuo = 'Google Duo',
    GoogleEarth = 'Google Earth',
    GoogleHangouts = 'Google Hangout',
    GoogleImage = 'Google Image',
    GoogleKeep = 'Google Keep',
    GoogleOne = 'Google One',
    GoogleMaps = 'Google Maps',
    GoogleMeet = 'Google Meet',
    GoogleNews = 'Google News',
    GooglePhotos = 'Google Photos',
    GooglePlay = 'Google Play',
    GoogleScholar = 'Google Scholar',
    GoogleSearch = 'Google Search',
    GoogleTranslate = 'Google Translate',
    GoogleWorkspace = 'Google Workspace',
    Youtube = 'Youtube',
}

type ProductInfo = {
    urlParser?: UrlParserConstructor,
    domains: Domain[],
    traits?: ProductTraits[],
    constructorFunction?: {
        new(...args: any[]): any
    },
};

const productsInfo : { [key in GoogleProduct]: ProductInfo } = {
    [GoogleProduct.Chat]: {
        domains: ['mail.google.com', 'chat.google.com'],
    },
    [GoogleProduct.ChromeWebStore]: {
        domains: ['chrome.google.com'],
    },
    [GoogleProduct.Gmail]: {
        domains: [
            'gmail.com',
            'googlemail.com',
            'mail.google.com',
        ],
        urlParser: (product, entry) => new GmailUrlParser(product, entry),
    },
    [GoogleProduct.GoogleAccount]: {
        domains: [
            'accounts.google.com',
            'myaccount.google.com',
            'passwords.google.com',
        ],
    },
    [GoogleProduct.GoogleAppsScript]: {
        domains: ['script.google.com'],
    },
    [GoogleProduct.GoogleBooks]: {
        domains: supportedDomains.map((domain) => `books.${domain}`),
    },
    [GoogleProduct.GoogleCalendar]: {
        domains: ['calendar.google.com'],
    },
    [GoogleProduct.GoogleCloudPlatform]: {
        domains: ['cloud.google.com'],
    },
    [GoogleProduct.GoogleContacts]: {
        domains: ['contacts.google.com'],
    },
    [GoogleProduct.GoogleDevelopers]: {
        domains: ['developers.google.com'],
    },
    [GoogleProduct.GoogleDomains]: {
        domains: ['domains.google.com'],
    },
    [GoogleProduct.GoogleDrive]: {
        domains: [
            'docs.google.com',
            'drive.google.com',
            'googleusercontent.com',
        ],
    },
    [GoogleProduct.GoogleDuo]: {
        domains: ['duo.google.com'],
    },
    [GoogleProduct.GoogleEarth]: {
        domains: ['earth.google.com'],
    },
    [GoogleProduct.GoogleHangouts]: {
        domains: ['hangouts.google.com'],
    },
    [GoogleProduct.GoogleImage]: {
        domains: supportedDomains
            .map((domain) => `image.${domain}`)
            .concat(supportedDomains),
    },
    [GoogleProduct.GoogleKeep]: {
        domains: ['keep.google.com'],
    },
    [GoogleProduct.GoogleMaps]: {
        domains: [
            ...supportedDomains.map((domain) => `maps.${domain}`),
            ...supportedDomains,
        ],
    },
    [GoogleProduct.GoogleMeet]: {
        domains: ['meet.google.com'],
    },
    [GoogleProduct.GoogleNews]: {
        domains: ['news.google.com'],
    },
    [GoogleProduct.GoogleOne]: {
        domains: ['one.google.com'],
    },
    [GoogleProduct.GooglePlay]: {
        domains: ['play.google.com'],
    },
    [GoogleProduct.GooglePhotos]: {
        domains: ['photos.google.com'],
    },
    [GoogleProduct.GoogleScholar]: {
        domains: supportedDomains.map((domain) => `scholar.${domain}`),
    },
    [GoogleProduct.GoogleSearch]: {
        domains: supportedDomains,
        urlParser: (product, entry) => new GoogleSearchParser(product, entry),
        traits: [
            ProductTraits.searchEngine,
        ],
    },
    [GoogleProduct.GoogleTranslate]: {
        domains: supportedDomains.map((domain) => `translate.${domain}`),
    },
    [GoogleProduct.GoogleWorkspace]: {
        domains: supportedDomains.map((domain) => `workspace.${domain}`),
    },
    [GoogleProduct.Youtube]: {
        domains: ['youtube.com', 'youtu.be', 'yt.be', 'youtubegaming.com'],
        urlParser: (product, entry) => new YoutubeUrlParser(product, entry),
        traits: [
            ProductTraits.videoLibrary,
            ProductTraits.searchEngine,
        ],
    },
};

const products = Object.entries(productsInfo)
    .map((entry) => {
        const [product, info] = entry;

        const constructor = info.constructorFunction || Product;

        return new constructor(
            `google/${product}`,
            product,
            info.domains,
            info.urlParser,
            info.traits,
        );
    });

const productMap : { [key in GoogleProduct]: Product } = Object.fromEntries(
    products.map(
        (product) => [product.name, product],
    ),
) as { [key in GoogleProduct]: Product };

const googleChatProduct = productMap[GoogleProduct.Chat];
const googleImageProduct = productMap[GoogleProduct.GoogleImage];
const googleMapsProduct = productMap[GoogleProduct.GoogleMaps];
const googleSearchProduct = productMap[GoogleProduct.GoogleSearch];
const gmailProduct = productMap[GoogleProduct.Gmail];

export default products;
export {
    gmailProduct,
    googleChatProduct,
    googleImageProduct,
    googleMapsProduct,
    googleSearchProduct,

    GoogleProduct,
};
