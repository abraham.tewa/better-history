/* eslint-disable max-classes-per-file */
// ============================================================
// Imports modules
import {
    Mailbox,
    MailboxPageType,
    SearchEngine,
    SearchEnginePageType,
    SearchEngineResultPageInfo,
    UrlParser,
} from '../../../Realm';

// ============================================================
// Module's constants and variables
const RE_SEARCH_HASH = /^#search\/([\w+%]+)(\/p(\d+))?$/;
const RE_EMAIL_HASH = /^#(\w+)?\/(\w{30,})$/;
const RE_EMAIL_TITLE = /^(.+) - ([\w+.@-]+) - Gmail$/;

// ============================================================
// Class
class GmailUrlParser extends UrlParser {
    // eslint-disable-next-line class-methods-use-this
    get isHomepage(): boolean {
        return isInbox(this.url);
    }

    protected getMailboxTrait(): Mailbox | undefined {
        return new MailboxTraitBuilder(this);
    }

    protected getSearchEngineTrait(): SearchEngine | undefined {
        return new SearchEngineTraitBuilder(this);
    }
}

// ============================================================
// Trait: Mailbox
class MailboxTraitBuilder extends Mailbox {
    #emailInfo: EmailInfo | undefined | null = null;

    protected calcPageType(): MailboxPageType | undefined {
        const { hash } = this.urlParser.url;

        if (RE_EMAIL_HASH.test(hash)) {
            return MailboxPageType.email;
        }

        if (isInbox(this.urlParser.url)) {
            return MailboxPageType.inbox;
        }

        if (hash === '#sent') {
            return MailboxPageType.sentMails;
        }

        return undefined;
    }

    protected calcEmailAccount() : string | undefined {
        if (this.#emailInfo === null) {
            this.#calcEmailInfo();
        }

        return this.#emailInfo?.email;
    }

    protected calcEmailTitle() : string | undefined {
        if (this.#emailInfo === null) {
            this.#calcEmailInfo();
        }

        return this.#emailInfo?.title;
    }

    #calcEmailInfo() : void {
        if (this.pageType !== MailboxPageType.email) {
            this.#emailInfo = undefined;
        }

        const res = RE_EMAIL_TITLE.exec(this.urlParser.title || '');

        if (!res) {
            this.#emailInfo = undefined;
        } else {
            this.#emailInfo = {
                email: res[1],
                title: res[2],
            };
        }
    }
}

// ============================================================
// Trait: Search Engine
class SearchEngineTraitBuilder extends SearchEngine {
    #searchInfo: SearchInfo | null | undefined = null;

    protected calcPageType(): SearchEnginePageType | undefined {
        const { hash } = this.urlParser.url;

        if (!hash.startsWith('#search')) {
            return undefined;
        }

        if (RE_SEARCH_HASH.test(hash)) {
            return SearchEnginePageType.resultList;
        }

        return undefined;
    }

    protected calcSearchTerms(): string | undefined {
        if (this.#searchInfo === null) {
            this.#calcSearchInfo();
        }

        return this.#searchInfo?.terms;
    }

    protected calcSearchResultsInfo(): SearchEngineResultPageInfo | undefined {
        if (this.#searchInfo === null) {
            this.#calcSearchInfo();
        }

        if (!this.#searchInfo) {
            return undefined;
        }

        const pageSize = 50;

        return {
            pagination: true,
            limit: pageSize,
            offset: pageSize * (this.#searchInfo.page - 1),
            page: this.#searchInfo.page,
            pageSize,
        };
    }

    #calcSearchInfo() {
        const res = RE_SEARCH_HASH.exec(this.urlParser.title || '');

        if (res) {
            this.#searchInfo = {
                terms: decodeURIComponent(res[1]),
                page: Number(res[3]) || 1,
            };
        } else {
            this.#searchInfo = undefined;
        }
    }
}

// ============================================================
// Functions
function isInbox(url: URL) {
    if (url.pathname === '/' || url.pathname === '/mail/') {
        return true;
    }

    return url.hash === '' || url.hash === '#inbox';
}

// ============================================================
// Types
type EmailInfo = {
    email: string,
    title: string,
};

type SearchInfo = {
    terms: string,
    page: number,
};

// ============================================================
// Exports
export default GmailUrlParser;
