/* eslint-disable max-classes-per-file */
import {
    SearchEngine,
    SearchEnginePageType,
    SearchEngineResultPageInfo,
    UrlParser,
} from '../../../Realm';

class GoogleSearchParser extends UrlParser {
    get isRedirection(): boolean {
        return this.url.searchParams.has('url');
    }

    protected getSearchEngineTrait(): SearchEngine | undefined {
        return new SearchEngineTraitBuilder(this);
    }
}

class SearchEngineTraitBuilder extends SearchEngine {
    #searchTerms: string | null | undefined = null;

    protected calcPageType() : SearchEnginePageType | undefined {
        if (this.searchTerms) {
            return SearchEnginePageType.resultList;
        }

        if (this.urlParser.url.searchParams.has('url')) {
            return SearchEnginePageType.resultRedirection;
        }

        return undefined;
    }

    protected calcSearchTerms(): string | undefined {
        if (this.#searchTerms === null) {
            this.#searchTerms = this.urlParser.url.searchParams.get('q') || undefined;
        }

        return this.#searchTerms;
    }

    // eslint-disable-next-line class-methods-use-this
    get searchResultsInfo(): SearchEngineResultPageInfo {
        return {
            pagination: false,
        };
    }
}

export default GoogleSearchParser;
