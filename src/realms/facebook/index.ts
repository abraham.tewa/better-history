import FacebookRealm from './FacebookRealm';

const realm = FacebookRealm.get();

export {
    domains,
} from './domains';

export {
    realm,
};
