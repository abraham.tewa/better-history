enum FacebookProducts {
    Facebook = 'Facebook',
    Instagram = 'Instagram',
    Messenger = 'Messenger',
    WhatsApp = 'WhatsApp',
}

const productDomains : { [key in FacebookProducts]: Domain[] } = {
    [FacebookProducts.Facebook]: ['facebook.com'],
    [FacebookProducts.Messenger]: ['messenger.com'],
    [FacebookProducts.Instagram]: ['instagram.com'],
    [FacebookProducts.WhatsApp]: ['whatsapp.com'],
};

export {
    productDomains,

    FacebookProducts,
};
