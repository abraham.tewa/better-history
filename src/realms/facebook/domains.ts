const domains = [
    'facebook.com',
    'messenger.com',
    'instagram.com',
    'whatsapp.com',
];

export {
    domains,
};
