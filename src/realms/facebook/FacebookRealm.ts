// ============================================================
// Import modules
import {
    Product,
    Realm,
} from '../../Realm';

import {
    productDomains,
} from './products';

import { domains } from './domains';

// ============================================================
// Class

class FacebookRealm extends Realm {
    static #realm : FacebookRealm;

    constructor() {
        if (FacebookRealm.#realm) {
            throw new Error('Realm already created');
        }

        const products = Object.entries(productDomains)
            .map(([product, listDomains]) => new Product(
                `facebook/${product}`,
                product,
                listDomains,
            ));

        super('facebook', 'Facebook', domains, products);
    }

    static get() {
        if (!this.#realm) {
            this.#realm = new FacebookRealm();
        }

        return this.#realm;
    }
}

// ============================================================
// Exports
export default FacebookRealm;
