import _ from 'underscore';
import { productDomains } from './products';

const listProductDomains = _.uniq(
    Object
        .values(productDomains)
        .flat(),
);

const domains = [
    'wikimediafoundation.org',
    ...listProductDomains,
];

export {
    domains,
};
