// ============================================================
// Import modules
import {
    Product,
    Realm,
} from '../../Realm';

import products, { WikimediaProducts } from './products';
import { domains } from './domains';

// ============================================================
// Class

class WikimediaRealm extends Realm {
    static readonly #realm: WikimediaRealm = new WikimediaRealm();

    constructor() {
        super('wikimedia', 'Wikimedia', domains, products);
    }

    getAllProducts(domain: string, completeUrl: URL): Product[] {
        if (domain !== 'wikimedia.org') {
            return super.getAllProducts(domain, completeUrl);
        }

        let product: Product | undefined;

        switch (completeUrl.hostname) {
        case 'species.wikimedia.org':
            product = this.getProductFromName(WikimediaProducts.Wikispecies);
            break;
        case 'commons.wikimedia.org':
            product = this.getProductFromName(WikimediaProducts.WikimediaCommons);
            break;
        case 'meta.wikimedia.org':
            product = this.getProductFromName(WikimediaProducts.MetaWiki);
            break;
        default:
            product = undefined;
        }

        return product
            ? [product]
            : [];
    }
}

// ============================================================
// Exports
export default WikimediaRealm;
