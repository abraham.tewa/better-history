import {
    Product,
    ProductTraits,
    UrlParserConstructor,
} from '../../../Realm';
import WikipediaUrlParser from './Wikipedia';

enum WikimediaProducts {
    Wikipedia = 'Wikipedia',
    Wikibooks = 'Wikibooks',
    Wiktionary = 'Wiktionary',
    Wikiquote = 'Wikiquote',
    WikimediaCommons = 'Wikimedia Commons',
    Wikisource = 'Wiki Source',
    Wikiversity = 'Wikiversity',
    Wikispecies = 'Wikispecies',
    Wikidata = 'Wikidata',
    MediaWiki = 'MediaWiki',
    Wikivoyage = 'Wikivoyage',
    Wikinews = 'Wikinews',
    MetaWiki = 'Meta-Wiki',
}

const productDomains : { [key in WikimediaProducts]?: Domain[] } = {
    [WikimediaProducts.Wikipedia]: ['wikipedia.org'],
    [WikimediaProducts.Wikibooks]: ['wikibooks.org'],
    [WikimediaProducts.Wiktionary]: ['wiktionary.org'],
    [WikimediaProducts.Wikiquote]: ['wikiquote.org'],
    [WikimediaProducts.WikimediaCommons]: ['wikimedia.org'],
    [WikimediaProducts.Wikisource]: ['wikisource.org'],
    [WikimediaProducts.Wikiversity]: ['wikiversity.org'],
    [WikimediaProducts.Wikispecies]: ['wikimedia.org'],
    [WikimediaProducts.Wikidata]: ['wikidata.org'],
    [WikimediaProducts.MediaWiki]: ['mediawiki.org'],
    [WikimediaProducts.Wikivoyage]: ['wikivoyage.org'],
    [WikimediaProducts.Wikinews]: ['wikinews.org'],
    [WikimediaProducts.MetaWiki]: ['wikimedia.org'],
};

const urlParsers : { [key in WikimediaProducts]?: UrlParserConstructor } = {
    [WikimediaProducts.Wikipedia]: (product, entry) => new WikipediaUrlParser(product, entry),
};

const traits : { [key in WikimediaProducts]?: ProductTraits[] } = {
    [WikimediaProducts.Wikipedia]: [ProductTraits.encyclopedia],
};

const products = Object.entries(productDomains)
    .map((entry) => {
        const [product, listDomains] = entry as [WikimediaProducts, string[]];

        const urlParser = urlParsers[product];

        return new Product(
            `google/${product}`,
            product,
            listDomains,
            urlParser,
            traits[product],
        );
    });

export default products;
export {
    WikimediaProducts,

    productDomains,
};
