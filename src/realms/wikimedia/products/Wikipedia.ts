/* eslint-disable max-classes-per-file */
import {
    UrlParser,
    SearchEnginePageType,
    Encyclopedia,
    EncyclopediaPageType,
    SearchEngine,
    SearchEngineResultPageInfo,
} from '../../../Realm';

const reTitle = /^(^.*)\s-\sWikipedia$/;

class WikipediaUrlParser extends UrlParser {
    protected getEncyclopediaTrait(): Encyclopedia {
        return new EncyclopediaTraitBuilder(this);
    }

    protected getSearchEngineTrait(): SearchEngine {
        return new SearchEngineTraitBuilder(this);
    }
}

// ============================================================
// Trait: SearchEngine
class SearchEngineTraitBuilder extends SearchEngine {
    protected calcPageType() : SearchEnginePageType | undefined {
        if (this.searchTerms) {
            return SearchEnginePageType.resultList;
        }

        return undefined;
    }

    #searchTerms: string | null | undefined = null;

    #searchResultsInfo: SearchEngineResultPageInfo | undefined;

    get searchResultsInfo() {
        if (!this.#searchResultsInfo) {
            const offset = Number(this.urlParser.url.searchParams.get('offset')) || 0;
            const limit = Number(this.urlParser.url.searchParams.get('limit')) || 20;

            this.#searchResultsInfo = {
                pagination: true,
                limit,
                offset,
                page: offset / limit + 1,
                pageSize: limit,
            };
        }

        return this.#searchResultsInfo;
    }

    get searchTerms(): string | undefined {
        if (this.#searchTerms === null) {
            this.#searchTerms = this.urlParser.url.searchParams.get('search') || undefined;
        }

        return this.#searchTerms;
    }
}

// ============================================================
// Trait: Encyclopedia
class EncyclopediaTraitBuilder extends Encyclopedia {
    protected calcPageType() : EncyclopediaPageType | undefined {
        if (this.urlParser.url.pathname.startsWith('/wiki/')) {
            return EncyclopediaPageType.article;
        }

        return undefined;
    }

    protected calcArticleTitle() : string | undefined {
        if (this.urlParser.title === undefined) {
            return undefined;
        }

        const res = reTitle.exec(this.urlParser.title);

        if (!res) {
            return undefined;
        }

        return res[1];
    }
}

// ============================================================
// Exports
export default WikipediaUrlParser;
