enum EncyclopediaPageType {
    article = 'encyclopedia/article',
}

enum MailboxPageType {
    inbox = 'mailbox/inbox',
    sentMails = 'mailbox/sent',
    email = 'mailbox/email',
}

enum SearchEnginePageType {
    resultList = 'searchEngine/results',
    resultRedirection = 'searchEngine/redirection',
}

enum VideoLibraryPageType {
    video = 'videoLibrary/video',
    channel = 'videoLibrary/channel',
    playlist = 'videoLibrary/playlist',
}

enum ProductTraits {
    encyclopedia = 'encyclopedia',
    searchEngine = 'searchEngine',
    videoLibrary = 'videoLibrary',
}

type PageType = EncyclopediaPageType
    | MailboxPageType
    | SearchEnginePageType
    | VideoLibraryPageType;

export {
    ProductTraits,
    EncyclopediaPageType,
    MailboxPageType,
    SearchEnginePageType,
    VideoLibraryPageType,
};

export type {
    PageType,
};
