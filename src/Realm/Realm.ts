/* eslint-disable max-classes-per-file */
// ============================================================
// Imports
import {
    ProductTraits,
    PageType,
} from './enum';

import {
    getDisplayTitle, Mailbox,
} from './traits';

import type {
    Encyclopedia,
    SearchEngine,
    VideoLibrary,
    Traits,
} from './traits';

// ============================================================
// Class: Realm
abstract class Realm {
    readonly id: string;

    readonly name: string;

    readonly domains: string[];

    readonly products: Product[];

    readonly #mapIdProduct: { [key: string]: Product };

    readonly #mapNameProduct: { [key: string]: Product };

    #mapDomainProductList: { [key: Domain]: Product[] };

    static #mapDomainRealm: { [key: Domain]: Realm } = {};

    static #productList: Product[] = [];

    static #staticMapDomainProductList: { [key: Domain]: Product[] };

    constructor(
        id: string,
        name: string,
        domains: string[],
        products: Product[],
    ) {
        this.id = id;
        this.name = name;
        this.products = products;
        this.domains = domains;

        this.products.forEach((product) => {
            product.setRealm(this);
        });

        this.#mapIdProduct = this.products.reduce(
            (acc, product) => {
                acc[product.id] = product;
                return acc;
            },
            {} as { [key: string]: Product },
        );

        this.#mapNameProduct = this.products.reduce(
            (acc, product) => {
                acc[product.name] = product;
                return acc;
            },
            {} as { [key: string]: Product },
        );

        this.#mapDomainProductList = mapProductsByDomains(this.products);

        Realm.#productList = Realm.#productList.concat(this.products);
        Realm.#staticMapDomainProductList = mapProductsByDomains(Realm.#productList);
        this.domains.forEach((domain) => {
            if (Realm.#mapDomainRealm[domain]) {
                const realm = Realm.#mapDomainRealm[domain];
                const msg = `Realm "${name}" cannot be affected to domain "${domain}".
                Domain already associated to realm "${realm.name}"`;
                throw new Error(msg);
            }

            Realm.#mapDomainRealm[domain] = this;
        });
    }

    static getMapDomainRealm() {
        return Realm.#mapDomainRealm;
    }

    /**
     * Return all products served by a given domain.
     * @param domain
     * @returns
     */
    getAllProducts(domain: Domain, completeUrl?: URL): Product[] {
        let products: Product[];

        if (completeUrl) {
            products = this.#mapDomainProductList[completeUrl.hostname];

            if (products) {
                return products;
            }
        }

        return this.#mapDomainProductList[domain] || [];
    }

    /**
     * Return a product from it's ID
     * @param id
     * @returns
     */
    getProductFromId(id: string): Product | undefined {
        return this.#mapIdProduct[id];
    }

    /**
     * Return a product from it's name
     * @returns
     */
    getProductFromName(name: string): Product | undefined {
        return this.#mapNameProduct[name];
    }

    /**
     * Return a product provided by a domain.
     * If the domain provide several domains, then it will return the first one listed.
     */
    getProduct(domain: Domain, completeUrl: URL): Product | undefined {
        const products = this.getAllProducts(domain, completeUrl);

        return products?.[0];
    }

    toString() {
        return this.name;
    }

    /**
     * Return the realm corresponding to the given domain
     */
    static get(domain: Domain) : Realm | undefined {
        return Realm.#mapDomainRealm[domain];
    }

    /**
     * Return all products served by a given domain.
     * @param domain
     * @returns
     */
    static getAllProducts(domain: Domain, completeUrl: URL): Product[] {
        const realm = Realm.get(domain);

        if (!realm) {
            return [];
        }

        const products = realm.getAllProducts(domain, completeUrl);

        return products;
    }

    /**
     * Return a product provided by a domain.
     * If the domain provide several domains, then it will return the first one listed.
     */
    static getProduct(domain: Domain, completeUrl: URL): Product | undefined {
        const products = Realm.getAllProducts(domain, completeUrl);
        return products[0];
    }
}

// ============================================================
// Class: Product

class Product {
    readonly id: string;

    #realm? : Realm;

    readonly name: string;

    readonly domains: string[];

    readonly urlParser?: UrlParserConstructor;

    readonly traits: ProductTraits[];

    static #indexById = new Map<string, Product>();

    constructor(
        id: string,
        name: string,
        domains: string[],
        urlParser?: UrlParserConstructor,
        traits: ProductTraits[] = [],
    ) {
        if (Product.#indexById.has(id)) {
            throw new Error(`Product ID already declared: ${id}`);
        }

        this.id = id;
        this.name = name;
        this.domains = domains;
        this.urlParser = urlParser;
        this.traits = [...traits];

        Product.#indexById.set(this.id, this);
    }

    get realm() : Realm | undefined {
        return this.#realm;
    }

    parse(entry: IVisitEntry) : UrlParser {
        if (!this.urlParser) {
            return new UrlParser(this, entry);
        }

        return this.urlParser(this, entry);
    }

    setRealm(realm: Realm) {
        this.#realm = realm;
    }

    toString() {
        return this.name;
    }

    static get(id: string) : Product | undefined {
        return Product.#indexById.get(id);
    }
}

// ============================================================
// Class: UrlParser
class UrlParser {
    readonly entry: IVisitEntry;

    readonly product: Product;

    readonly title: string | undefined;

    readonly url: URL;

    #pageType: PageType[] | undefined;

    constructor(product: Product, entry: IVisitEntry) {
        this.entry = entry;
        this.url = entry.url;
        this.title = entry.title;
        this.product = product;
    }

    #displayTitle: string | undefined;

    get displayTitle() : string {
        if (this.#displayTitle === undefined) {
            this.#displayTitle = getDisplayTitle(this.traits) || this.title || '';
        }

        return this.#displayTitle;
    }

    get pageTypes() : PageType[] {
        if (!this.#pageType) {
            this.#pageType = this.calcPageTypes();
        }

        return [...this.#pageType];
    }

    get isHomepage() : boolean {
        return this.url.pathname === '/';
    }

    // eslint-disable-next-line class-methods-use-this
    get isRedirection() : boolean {
        return false;
    }

    #traits: Traits | undefined;

    get traits() : Traits {
        const self = this;

        if (!this.#traits) {
            let encyclopedia: Encyclopedia | undefined | null = null;
            let searchEngine: SearchEngine | undefined | null = null;
            let mailbox: Mailbox | undefined | null = null;
            let videoLibrary: VideoLibrary | undefined | null = null;

            this.#traits = {
                get encyclopedia() : Encyclopedia | undefined {
                    if (encyclopedia === null) {
                        encyclopedia = self.getEncyclopediaTrait();
                    }

                    return encyclopedia;
                },
                get mailbox() : Mailbox | undefined {
                    if (mailbox === null) {
                        mailbox = self.getMailboxTrait();
                    }

                    return mailbox;
                },
                get searchEngine() : SearchEngine | undefined {
                    if (searchEngine === null) {
                        searchEngine = self.getSearchEngineTrait();
                    }

                    return searchEngine;
                },
                get videoLibrary() : VideoLibrary | undefined {
                    if (videoLibrary === null) {
                        videoLibrary = self.getVideoLibraryTrait();
                    }

                    return videoLibrary;
                },
            };
        }

        return this.#traits;
    }

    equal(urlParser: UrlParser): boolean {
        if (this.product !== urlParser.product) {
            return false;
        }

        return this.url.hostname === urlParser.url.hostname
            && this.url.pathname === urlParser.url.pathname;
    }

    // eslint-disable-next-line class-methods-use-this
    protected getVideoLibraryTrait() : VideoLibrary | undefined {
        return undefined;
    }

    // eslint-disable-next-line class-methods-use-this
    protected getEncyclopediaTrait() : Encyclopedia | undefined {
        return undefined;
    }

    // eslint-disable-next-line class-methods-use-this
    protected getMailboxTrait() : Mailbox | undefined {
        return undefined;
    }

    // eslint-disable-next-line class-methods-use-this
    protected getSearchEngineTrait() : SearchEngine | undefined {
        return undefined;
    }

    protected calcPageTypes(): PageType[] {
        const pageType : PageType[] = [];

        if (this.traits.encyclopedia?.pageType) {
            pageType.push(this.traits.encyclopedia.pageType);
        }

        if (this.traits.mailbox?.pageType) {
            pageType.push(this.traits.mailbox.pageType);
        }

        if (this.traits.searchEngine?.pageType) {
            pageType.push(this.traits.searchEngine.pageType);
        }

        if (this.traits.videoLibrary?.pageType) {
            pageType.push(this.traits.videoLibrary.pageType);
        }

        return pageType;
    }
}

function mapProductsByDomains(products: Product[]) : { [key: Domain]: Product[] } {
    const productDomainMap : Array<{ domain: Domain, product: Product }> = products
        .flatMap((product) => product.domains.map((domain) => ({ domain, product })));

    const map = productDomainMap.reduce(
        (acc, { domain, product }) => {
            if (!acc[domain]) {
                acc[domain] = [];
            }

            acc[domain].push(product);
            return acc;
        },
        {} as { [key: Domain]: Product[] },
    );

    return map;
}

type UrlParserConstructor = (product: Product, entry: IVisitEntry) => UrlParser;

// ============================================================
// Exports
export default Realm;
export {
    Product,
    UrlParser,
};
export type {
    UrlParserConstructor,
};
