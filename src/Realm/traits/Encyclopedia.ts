// ============================================================
// Imports
import { EncyclopediaPageType } from '../enum';
import Trait from './Trait';

// ============================================================
// Class
abstract class Encyclopedia extends Trait<EncyclopediaPageType> {
    /**
     * ============================================================
     * Property: Article Title
     */
    #articleTitle: GetOrCalc<string> = null;

    // Title of the encyclopedia article
    get articleTitle(): string | undefined {
        if (this.#articleTitle === null) {
            this.#articleTitle = this.calcArticleTitle();
        }

        return this.#articleTitle;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcArticleTitle(): string | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Property: Statics
     */
    static getDisplayTitle(trait: Encyclopedia): string | undefined {
        switch (trait.pageType) {
        case EncyclopediaPageType.article:
            return trait.articleTitle;
        default:
            return undefined;
        }
    }
}

// ============================================================
// Exports
export default Encyclopedia;
