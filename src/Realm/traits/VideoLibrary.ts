// ============================================================
// Imports modules
import { VideoLibraryPageType } from '../enum';
import Trait from './Trait';

// ============================================================
// Class
/**
 * Describe a website that is displaying videos
 */
abstract class VideoLibrary extends Trait<VideoLibraryPageType> {
    /**
     * ============================================================
     * Property: Channel title
     */
    #channelTitle: GetOrCalc<string> = null;

    get channelTitle(): string | undefined {
        if (this.#channelTitle === null) {
            this.#channelTitle = this.calcChannelTitle();
        }

        return this.#channelTitle;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcChannelTitle() : string | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Property: Video title
     */
    #videoTitle: GetOrCalc<string> = null;

    // eslint-disable-next-line class-methods-use-this
    get videoTitle(): string | undefined {
        if (this.#videoTitle === null) {
            this.#videoTitle = this.calcVideoTitle();
        }

        return this.#videoTitle;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcVideoTitle(): string | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Property: Video ID
     */
    #videoId: GetOrCalc<string> = null;

    get videoId(): string | undefined {
        if (this.#videoId === null) {
            this.#videoId = this.calcVideoId();
        }

        return this.#videoId;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcVideoId(): string | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Statics
     */
    static getDisplayTitle(trait: VideoLibrary): string | undefined {
        switch (trait.pageType) {
        case VideoLibraryPageType.channel:
            return trait.channelTitle;
        case VideoLibraryPageType.playlist:
            return undefined;
        case VideoLibraryPageType.video:
            return trait.videoTitle;
        default:
            return undefined;
        }
    }
}

// ============================================================
// Exports
export default VideoLibrary;
