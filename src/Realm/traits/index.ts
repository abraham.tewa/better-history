import Encyclopedia from './Encyclopedia';
import Mailbox from './Mailbox';
import SearchEngine from './SearchEngine';
import VideoLibrary from './VideoLibrary';

type Traits = {
    encyclopedia: Encyclopedia | undefined,
    mailbox: Mailbox | undefined,
    searchEngine: SearchEngine | undefined,
    videoLibrary: VideoLibrary | undefined,
};

function getDisplayTitle(trait: Traits) : string | undefined {
    if (trait.videoLibrary) {
        const title = VideoLibrary.getDisplayTitle(trait.videoLibrary);

        if (title) {
            return title;
        }
    }

    if (trait.mailbox) {
        const title = Mailbox.getDisplayTitle(trait.mailbox);

        if (title) {
            return title;
        }
    }

    if (trait.encyclopedia) {
        const title = Encyclopedia.getDisplayTitle(trait.encyclopedia);

        if (title) {
            return title;
        }
    }

    if (trait.searchEngine) {
        const title = SearchEngine.getDisplayTitle(trait.searchEngine);

        if (title) {
            return title;
        }
    }

    return undefined;
}

export {
    getDisplayTitle,
};

export type {
    Traits,
};

export { default as Mailbox } from './Mailbox';
export { default as Encyclopedia } from './Encyclopedia';
export { default as SearchEngine } from './SearchEngine';
export { default as VideoLibrary } from './VideoLibrary';

export type {
    SearchEngineResultPageInfo,
} from './SearchEngine';
