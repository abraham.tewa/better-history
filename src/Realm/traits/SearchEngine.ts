// ============================================================
// Imports modules
import { SearchEnginePageType } from '../enum';
import Trait from './Trait';

// ============================================================
// Types
abstract class SearchEngine extends Trait<SearchEnginePageType> {
    /**
     * ============================================================
     * Property: searchTerms
     */
    #searchTerms: string | undefined | null = null;

    // Search terms.
    // Will be defined only on result list page.
    get searchTerms(): string | undefined {
        if (this.#searchTerms === null) {
            this.#searchTerms = this.calcSearchTerms();
        }

        return this.#searchTerms;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcSearchTerms(): string | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Property: searchResultInfo
     */
    #searchResultsInfo: SearchEngineResultPageInfo | undefined | null = null;

    // Search page info
    get searchResultsInfo(): SearchEngineResultPageInfo | undefined {
        if (this.#searchResultsInfo === null) {
            this.#searchResultsInfo = this.calcSearchResultsInfo();
        }

        return this.#searchResultsInfo;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcSearchResultsInfo(): SearchEngineResultPageInfo | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Property: openedResults
     */
    #openedResults: VisitEntry[] | undefined;

    get openedResults(): VisitEntry[] | undefined {
        if (!this.#openedResults) {
            this.#openedResults = this.searchTerms === undefined
                ? undefined
                : openedResults(this.urlParser, this.searchTerms);
        }

        return this.#openedResults;
    }

    /**
     * ============================================================
     * Statics
     */
    static getDisplayTitle(trait: SearchEngine): string | undefined {
        switch (trait.pageType) {
        case SearchEnginePageType.resultList:
            return trait.searchTerms;
        default:
            return undefined;
        }
    }
}

// ============================================================
// Helpers

function openedResults(
    urlParser: IUrlParser,
    searchTerms: string,
): VisitEntry[] {
    const results = urlParser.entry
        .children
        .map((child) => {
            if (!child.parseInfo || child.product !== urlParser.product) {
                return [];
            }

            const {
                searchEngine,
            } = child.parseInfo.traits as { searchEngine: SearchEngine };

            if (searchEngine.searchTerms === searchTerms) {
                return searchEngine.openedResults || [];
            }

            if (searchEngine.pageType === SearchEnginePageType.resultRedirection) {
                return child.directChildren;
            }

            return [];
        });

    return results.flat();
}

// ============================================================
// Types

type SearchEngineResultPageInfo = SearchResultInfoWithoutPagination | SearchResultInfoWithPagination;

type SearchResultInfoWithoutPagination = {
    pagination: false,
};

type SearchResultInfoWithPagination = BasicPagination & OffsetPagination & { pagination: true };

type BasicPagination = {
    page: number,
    pageSize: number,
};

type OffsetPagination = {
    offset: number,
    limit: number,
};

// ============================================================
// Exports

export default SearchEngine;

export type {
    SearchEngineResultPageInfo,
};
