// ============================================================
// Types
abstract class Trait<T> {
    protected urlParser: IUrlParser;

    constructor(urlParser: IUrlParser) {
        this.urlParser = urlParser;
    }

    /**
     * ============================================================
     * Property: pageType
     */
    #pageType: T | undefined | null = null;

    // eslint-disable-next-line class-methods-use-this
    get pageType(): T | undefined {
        if (this.#pageType === null) {
            this.#pageType = this.calcPageType();
        }

        return this.#pageType;
    }

    // eslint-disable-next-line class-methods-use-this
    protected abstract calcPageType(): T | undefined;
}

export default Trait;
