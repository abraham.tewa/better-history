// ============================================================
// Imports modules
import { MailboxPageType } from '../enum';
import Trait from './Trait';

// ============================================================
// Class
abstract class Mailbox extends Trait<MailboxPageType> {
    /**
     * ============================================================
     * Property: Email account
     */
    #emailAccount: GetOrCalc<string> = null;

    get emailAccount(): string | undefined {
        if (this.#emailAccount === null) {
            this.#emailAccount = this.calcEmailAccount();
        }

        return this.#emailAccount;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcEmailAccount(): string | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Property: EmailTitle
     */
    #emailTitle: GetOrCalc<string> = null;

    get emailTitle(): string | undefined {
        if (this.#emailTitle === null) {
            this.#emailTitle = this.calcEmailTitle();
        }

        return this.#emailTitle;
    }

    // eslint-disable-next-line class-methods-use-this
    protected calcEmailTitle(): string | undefined {
        return undefined;
    }

    /**
     * ============================================================
     * Static
     */
    static getDisplayTitle(trait: Mailbox) : string | undefined {
        if (trait.pageType === MailboxPageType.email) {
            return trait.emailTitle;
        }

        return undefined;
    }
}

// ============================================================
// Exports
export default Mailbox;
